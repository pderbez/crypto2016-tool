# Generic Makefile for compiling a simple executable.

#Polynomial used to define the finite field GF(2^n) - 
# AES Field
GF_POLY := 0x11b 
# F_2
#GF_POLY := 0x02
# Piccolo
#GF_POLY := 0x13

CC := g++
#CC := icpc
SRCDIR := src
BUILDDIR := build
USERDEFINES := -DGF_POLY=$(GF_POLY)
#CFLAGS := -fast -s -Wall -Wno-unused-variable -std=c++11 -DNDEBUG -opt-multi-version-aggressive -xHost
#CFLAGS := -fast -s -Wall -Wno-unused-variable -std=c++11 -DNDEBUG -opt-multi-version-aggressive -xHost -prof-use -prof-dir=profile/
#CFLAGS := -fast -Wall -Wno-unused-variable -std=c++11 -DNDEBUG -opt-multi-version-aggressive -xHost -prof-gen -prof-dir=profile/
CFLAGS := -O3 -s -mtune=native -march=native -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O3 -s -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O3 -march=native -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -g -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O2 -g -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O2 -pg -g -Wall -Wno-unused-variable -std=c++11 -DNDEBUG

LIBS := -lboost_program_options

TARGET_GDS := GDSSearch_$(GF_POLY)
TARGET_MitM := MitMSearch_$(GF_POLY)
TARGET_ImpDiff := IDSearch_$(GF_POLY)

SOURCES := $(shell find $(SRCDIR) -type f -name *.cpp)
HEADERS := $(shell find $(SRCDIR) -type f -name *.h)

SOURCES_GDS := $(shell find $(SRCDIR) -type f -name *.cpp -not -name main*.* -or -name mainGDS.cpp)
SOURCES_MitM := $(shell find $(SRCDIR) -type f -name *.cpp -not -name main*.* -or -name mainMitM.cpp)
SOURCES_ImpDiff := $(shell find $(SRCDIR) -type f -name *.cpp -not -name main*.* -or -name mainImpDiff.cpp)


#OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
OBJECTS_GDS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES_GDS:.cpp=.o))
OBJECTS_MitM := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES_MitM:.cpp=.o))
OBJECTS_ImpDiff := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES_ImpDiff:.cpp=.o))

#DEPS := $(OBJECTS:.o=.deps)
DEPS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.cpp=.deps))


all: $(TARGET_GDS) $(TARGET_MitM) $(TARGET_ImpDiff)

$(TARGET_GDS): $(OBJECTS_GDS)
	@echo " Linking GDSSearch..."; $(CC) $(USERDEFINES) $(CFLAGS) $^ $(LIBS) -o $(TARGET_GDS) 
	
$(TARGET_MitM): $(OBJECTS_MitM)
	@echo " Linking MitMSearch..."; $(CC) $(USERDEFINES) $(CFLAGS) $^ $(LIBS) -o $(TARGET_MitM) 

$(TARGET_ImpDiff): $(OBJECTS_ImpDiff)
	@echo " Linking IDSearch..."; $(CC) $(USERDEFINES) $(CFLAGS) $^ $(LIBS) -o $(TARGET_ImpDiff) 


$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp  $(HEADERS)
	@mkdir -p $(BUILDDIR)
	@echo " CC $<"; $(CC) $(USERDEFINES) $(CFLAGS) -MD -MF $(@:.o=.deps) -c -o $@ $<

clean:
	@echo " Cleaning..."; $(RM) -r $(BUILDDIR) $(TARGET_GDS) $(TARGET_MitM) $(TARGET_ImpDiff) *~

-include $(DEPS)

$(BUILDDIR)/GFElement.o:   .FORCE

.FORCE:

.PHONY: clean .FORCE

