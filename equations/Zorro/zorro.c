


#include <stdio.h>

#define Test(l,c) ((c==0 && l==0) || (c==1 && l==0) || (c==2 && l==3) || (c==3 && l==1)) // where the sbox is applied

#define R 15 // number of rounds

int main(int argc, char **argv)
{
	int i;
	
	printf("--system:\n");
	
	for (i = 0; i < 16; i++) printf("P[%d,%d] + K[%d,%d] + X_0[%d,%d]\n",i/4,i%4,i/4,i%4,i/4,i%4);
	
	int nb_step = R/4;
	int final_round = R - 4*(R/4);
	
	for (i = 0; i < nb_step; i++)
	{
		int j;
		for (j = 0; j < 4; j++)
		{
			int c,l;
			for (l = 0; l < 4; l++)
			{
				for (c = 0; c < 4; c++)
				{
					if (Test(l,c)) printf("S(X_%d[%d,%d]) + Y_%d[%d,%d]\n",4*i + j, l,c,4*i + j, l,(c - l + 4)%4);
					else printf("X_%d[%d,%d] + Y_%d[%d,%d]\n",4*i + j, l,c,4*i + j, l,(c - l + 4)%4);
				}
			}
			for (c = 0; c < 4; c++)
			{
				printf("W_%d[0,%d] + 02*Y_%d[0,%d] + 03*Y_%d[1,%d] + Y_%d[2,%d] + Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
				printf("W_%d[1,%d] + Y_%d[0,%d] + 02*Y_%d[1,%d] + 03*Y_%d[2,%d] + Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
				printf("W_%d[2,%d] + Y_%d[0,%d] + Y_%d[1,%d] + 02*Y_%d[2,%d] + 03*Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
				printf("W_%d[3,%d] + 03*Y_%d[0,%d] + Y_%d[1,%d] + Y_%d[2,%d] + 02*Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
			}
			if (j < 3)
			{
				unsigned char k = 4*i + j;
				for (l = 0; l < 4; l++)
				{
					for (c = 0; c < 4; c++)
					{
						if (!Test(l,c)) printf("W_%d[%d,%d] + X_%d[%d,%d]\n",4*i + j, l,c,4*i + j+1, l,c);
						else
						{
							if (c == 0) printf("W_%d[%d,%d] + X_%d[%d,%d] + %02x\n",4*i + j, l,c,4*i + j+1, l,c,k<<3);
							else printf("W_%d[%d,%d] + X_%d[%d,%d] + %02x\n",4*i + j, l,c,4*i + j+1, l,c,k);
						}
					}
				}
			}
			else
			{
				int k;
				if (i == nb_step - 1 && final_round == 0) for (k = 0; k < 16; k++) printf("W_%d[%d,%d] + K[%d,%d] + C[%d,%d]\n",4*i+j,k/4,k%4,k/4,k%4,k/4,k%4);
				else for (k = 0; k < 16; k++) printf("W_%d[%d,%d] + K[%d,%d] + X_%d[%d,%d]\n",4*i+j,k/4,k%4,k/4,k%4,4*i+j+1,k/4,k%4);
			}
			
		}
	}
	
	{
		int j;
		for (j = 0; j < final_round; j++)
		{
			int c,l;
			for (l = 0; l < 4; l++)
			{
				for (c = 0; c < 4; c++)
				{
					if (Test(l,c)) printf("S(X_%d[%d,%d]) + Y_%d[%d,%d]\n",4*i + j, l,c,4*i + j, l,(c - l + 4)%4);
					else printf("X_%d[%d,%d] + Y_%d[%d,%d]\n",4*i + j, l,c,4*i + j, l,(c - l + 4)%4);
				}
			}
			for (c = 0; c < 4; c++)
			{
				printf("W_%d[0,%d] + 02*Y_%d[0,%d] + 03*Y_%d[1,%d] + Y_%d[2,%d] + Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
				printf("W_%d[1,%d] + Y_%d[0,%d] + 02*Y_%d[1,%d] + 03*Y_%d[2,%d] + Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
				printf("W_%d[2,%d] + Y_%d[0,%d] + Y_%d[1,%d] + 02*Y_%d[2,%d] + 03*Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
				printf("W_%d[3,%d] + 03*Y_%d[0,%d] + Y_%d[1,%d] + Y_%d[2,%d] + 02*Y_%d[3,%d]\n",4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c,4*i+j,c);
			}
			if (j < final_round-1)
			{
				unsigned char k = 4*i + j;
				for (l = 0; l < 4; l++)
				{
					for (c = 0; c < 4; c++)
					{
						if (!Test(l,c)) printf("W_%d[%d,%d] + X_%d[%d,%d]\n",4*i + j, l,c,4*i + j+1, l,c);
						else
						{
							if (c == 0) printf("W_%d[%d,%d] + X_%d[%d,%d] + %02x\n",4*i + j, l,c,4*i + j+1, l,c,k<<3);
							else printf("W_%d[%d,%d] + X_%d[%d,%d] + %02x\n",4*i + j, l,c,4*i + j+1, l,c,k);
						}
					}
				}
			}
			else
			{
				int k;
				//for (k = 0; k < 16; k++) printf("W_%d[%d,%d] + K[%d,%d] + X_%d[%d,%d]\n",4*i+j,k/4,k%4,k/4,k%4,4*i+j+1,k/4,k%4);
				for (k = 0; k < 16; k++) printf("W_%d[%d,%d] + K[%d,%d] + C[%d,%d]\n",4*i+j,k/4,k%4,k/4,k%4,k/4,k%4);
			}
			
		}
		
		
	}
	
	
	
	return 0;
}

