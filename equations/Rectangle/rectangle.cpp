
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	cout << "--system:" << endl;
	
	const unsigned int nb_rounds = 12;
	
	unsigned int shifts[4] = {0,1,12,13};
	unsigned int shifts_ks[4] = {7,9,11,13};
	unsigned int RC[25] = {0x01,0x02,0x04,0x09,0x12,0x05,0x0b,0x16,0x0c,0x19,0x13,0x07,0x0f,0x1f,0x1e,0x1c,0x18,0x11,0x03,0x06,0x0d,0x1b,0x17,0x0e,0x1d};
	
	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 0; j < 16; j++)
		{
			cout << "X_" << 0 << "[" << i << ',' << j << "]";
			cout << " + ";
			cout << "P" << "[" << i << ',' << j << "]";
			cout << endl;
		}
	}
	
	for (unsigned int r = 0; r < nb_rounds; r++)
	{
		for (unsigned int i = 0; i < 4; i++) // AddRoundKey
		{
			for (unsigned int j = 0; j < 16; j++)
			{
				cout << "X_" << r << "[" << i << ',' << j << "]";
				cout << " + ";
				cout << "Y_" << r << "[" << i << ',' << j << "]";
				cout << " + ";
				cout << "K_" << r << "[" << i << ',' << j << "]";
				cout << endl;
			}
		}
		
		for (unsigned int i = 0; i < 4; i++) //SubColumns
		{
			for (unsigned int j = 0; j < 16; j++)
			{
				cout << "Z_" << r << "[" << i << ',' << j << "]";
				cout << " + ";
				cout << "S_" << i << "(";
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Y_" << r << "[" << k << ',' << j << "]";
					if (k == 3) cout << ")";
					else cout << ',';
				}
				cout << endl;
			}
		}
		
		for (unsigned int i = 0; i < 4; i++) //ShiftRows
		{
			for (unsigned int j = 0; j < 16; j++)
			{
				cout << "X_" << r+1 << "[" << i << ',' << j << "]";
				cout << " + ";
				cout << "Z_" << r << "[" << i << ',' << (j-shifts[i])%16 << "]";
				cout << endl;
			}
		}
	}
	
	for (unsigned int i = 0; i < 4; i++) // AddRoundKey
	{
		for (unsigned int j = 0; j < 16; j++)
		{
			cout << "X_" << nb_rounds << "[" << i << ',' << j << "]";
			cout << " + ";
			cout << "C" << "[" << i << ',' << j << "]";
			cout << " + ";
			cout << "K_" << nb_rounds << "[" << i << ',' << j << "]";
			cout << endl;
		}
	}
	
	for (unsigned int r = 0; r < nb_rounds; r++) //Keyschedule
	{
		for (unsigned int i = 0; i < 4; i++) //step 0
		{
			for (unsigned int j = 0; j < 16; j++)
			{
				cout << "Kv_" << r << "[" << i << ',' << j << "]";
				cout << " + ";
				cout << "K_" << r << "[" << i << ',' << j << "]";
				cout << endl;
			}
		}
		for (unsigned int i = 0; i < 4; i++) //step 1a
		{
			cout << "Kvv_" << r << "[" << i << ',' << 0 << "]";
			cout << " + ";
			cout << "S_" << i << '(';
			for (unsigned int j = 0; j < 4; j++)
			{
				cout << "Kv_" << r << "[" << j << ',' << 0 << "]";
				if (j == 3) cout << ')';
				else cout << ',';
			}			
			cout << endl;
		}
		for (unsigned int i = 0; i < 4; i++) //step 1b
		{
			for (unsigned int j = 1; j < 20; j++)
			{
				cout << "Kv_" << r << "[" << i << ',' << j << "]";
				cout << " + ";
				cout << "Kvv_" << r << "[" << i << ',' << j << "]";
				cout << endl;
			}
		}
		for (unsigned int i = 0; i < 4; i++) //step 1b
		{
			for (unsigned int j = 0; j < 20; j++)
			{
				cout << "Kv_" << r+1 << "[" << i << ',' << j << "]";
				cout << " + ";
				cout << "Kvv_" << r << "[" << i << ',' << (j-shifts_ks[i])%20 << "]";
				if (i == 0 && j < 5)
				{
					auto tmp = RC[r];
					for (unsigned int k = 0; k < j; k++) tmp = tmp >> 1;
					if (tmp%2 == 1) cout << " + 1";
				}
				cout << endl;
			}
		}
	}
	
	for (unsigned int i = 0; i < 4; i++) //step 0
	{
		for (unsigned int j = 0; j < 16; j++)
		{
			cout << "Kv_" << nb_rounds << "[" << i << ',' << j << "]";
			cout << " + ";
			cout << "K_" << nb_rounds << "[" << i << ',' << j << "]";
			cout << endl;
		}
	}
	
	
	return 0;
}

