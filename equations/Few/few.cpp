

#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	const unsigned int nb_rounds = 16;
	
	cout << "--system:" << endl;
	
	for (unsigned int i = 0; i < 32; i++) cout << "L_0[" << i << "] + " << "P[" << i << "]" << endl;
	for (unsigned int i = 0; i < 32; i++) cout << "L_1[" << i << "] + " << "P[" << i + 32 << "]" << endl;
	
	for (unsigned int r = 0; r < nb_rounds; ++r)
	{
		for (unsigned int i = 0; i < 32; ++i) cout << "L_" << r + 2 << "[" << i << "] + " << "L_" << r << "[" << i << "] + " << "Y_" << r << "[" << i << "]" << endl;
		
		for (unsigned int i = 0; i < 8; i++) cout << "A_" << r << "[" << i << "] + " << "L_" << r+1 << "[" << i << "] + " << "K_" << r << "[" << i << "]" << endl;
		for (unsigned int i = 0; i < 8; i++) cout << "A_" << r << "[" << i+8 << "] + " << "L_" << r+1 << "[" << i+24 << "] + " << "K_" << r << "[" << i+24 << "]" << endl;
		for (unsigned int i = 0; i < 8; i++) cout << "B_" << r << "[" << i << "] + " << "L_" << r+1 << "[" << i+16 << "] + " << "K_" << r << "[" << i+16 << "]" << endl;
		for (unsigned int i = 0; i < 8; i++) cout << "B_" << r << "[" << i+8 << "] + " << "L_" << r+1 << "[" << i+8 << "] + " << "K_" << r << "[" << i+8 << "]" << endl;
		
		for (unsigned int i = 0; i < 4; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				cout << "U_" << r << "[" << 4*i+j << "]" << " + " << "S_" << j << "(";
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "A_" << r << "[" << 4*i + k << "]";
					if (k == 3) cout << ")";
					else cout << ",";
				}
				cout << endl;
			}
		}
		for (unsigned int i = 0; i < 16; i++)
		{
			cout << "Y_" << r << "[" << i << "]";
			cout << " + " << "U_" << r << "[" << i << "]";
			cout << " + " << "U_" << r << "[" << (i+1)%16 << "]";
			cout << " + " << "U_" << r << "[" << (i+5)%16 << "]";
			cout << " + " << "U_" << r << "[" << (i+9)%16 << "]";
			cout << " + " << "U_" << r << "[" << (i+12)%16 << "]";
			cout << endl;
		}
		
		for (unsigned int i = 0; i < 4; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				cout << "V_" << r << "[" << 4*i+j << "]" << " + " << "S_" << j << "(";
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "B_" << r << "[" << 4*i + k << "]";
					if (k == 3) cout << ")";
					else cout << ",";
				}
				cout << endl;
			}
		}
		for (unsigned int i = 0; i < 16; i++)
		{
			cout << "Y_" << r << "[" << i+16 << "]";
			cout << " + " << "V_" << r << "[" << i << "]";
			cout << " + " << "V_" << r << "[" << (i+4)%16 << "]";
			cout << " + " << "V_" << r << "[" << (i+7)%16 << "]";
			cout << " + " << "V_" << r << "[" << (i+11)%16 << "]";
			cout << " + " << "V_" << r << "[" << (i+15)%16 << "]";
			cout << endl;
		}
	}
	
	for (unsigned int i = 0; i < 32; i++) cout << "L_" << nb_rounds+1 << "[" << i << "] + " << "C[" << i << "]" << endl;
	for (unsigned int i = 0; i < 32; i++) cout << "L_" << nb_rounds << "[" << i << "] + " << "C[" << i + 32 << "]" << endl;
	
	
	for (unsigned int r = 0; r < 2*nb_rounds; r++)
	{
		unsigned int cst[8];
		auto x = r;
		for (unsigned int i = 0; i < 8; i++)
		{
			cst[i] = x%2;
			x = x >> 1;
		}
		for (unsigned i = 0; i < 80; i++) cout << "Kmm_" << r << "[" << i << "] + " << "Km_" << r << "[" << (i+13)%80 << "]" << endl;
		cout << "Km_" << r+1 << "[" << 0 << "] + S_0(" << "Kmm_" << r << "[" << 0 << "]," << "Kmm_" << r << "[" << 1 << "]," << "Kmm_" << r << "[" << 2 << "]," << "Kmm_" << r << "[" << 3 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 1 << "] + S_1(" << "Kmm_" << r << "[" << 0 << "]," << "Kmm_" << r << "[" << 1 << "]," << "Kmm_" << r << "[" << 2 << "]," << "Kmm_" << r << "[" << 3 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 2 << "] + S_2(" << "Kmm_" << r << "[" << 0 << "]," << "Kmm_" << r << "[" << 1 << "]," << "Kmm_" << r << "[" << 2 << "]," << "Kmm_" << r << "[" << 3 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 3 << "] + S_3(" << "Kmm_" << r << "[" << 0 << "]," << "Kmm_" << r << "[" << 1 << "]," << "Kmm_" << r << "[" << 2 << "]," << "Kmm_" << r << "[" << 3 << "])" << endl;
		for (unsigned i = 4; i < 64; i++) cout << "Kmm_" << r << "[" << i << "] + " << "Km_" << r+1 << "[" << i << "]" << endl;
		cout << "Km_" << r+1 << "[" << 64 << "] + S_0(" << "Kmm_" << r << "[" << 64 << "]," << "Kmm_" << r << "[" << 65 << "]," << "Kmm_" << r << "[" << 66 << "]," << "Kmm_" << r << "[" << 67 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 65 << "] + S_1(" << "Kmm_" << r << "[" << 64 << "]," << "Kmm_" << r << "[" << 65 << "]," << "Kmm_" << r << "[" << 66 << "]," << "Kmm_" << r << "[" << 67 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 66 << "] + S_2(" << "Kmm_" << r << "[" << 64 << "]," << "Kmm_" << r << "[" << 65 << "]," << "Kmm_" << r << "[" << 66 << "]," << "Kmm_" << r << "[" << 67 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 67 << "] + S_3(" << "Kmm_" << r << "[" << 64 << "]," << "Kmm_" << r << "[" << 65 << "]," << "Kmm_" << r << "[" << 66 << "]," << "Kmm_" << r << "[" << 67 << "])" << endl;
		for (unsigned i = 68; i < 76; i++)
		{
			cout << "Kmm_" << r << "[" << i << "] + " << "Km_" << r+1 << "[" << i << "]";
			if (cst[75 - i] == 1) cout << " + 1";
			cout << endl;
		}
		cout << "Km_" << r+1 << "[" << 76 << "] + S_0(" << "Kmm_" << r << "[" << 76 << "]," << "Kmm_" << r << "[" << 77 << "]," << "Kmm_" << r << "[" << 78 << "]," << "Kmm_" << r << "[" << 79 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 77 << "] + S_1(" << "Kmm_" << r << "[" << 76 << "]," << "Kmm_" << r << "[" << 77 << "]," << "Kmm_" << r << "[" << 78 << "]," << "Kmm_" << r << "[" << 79 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 78 << "] + S_2(" << "Kmm_" << r << "[" << 76 << "]," << "Kmm_" << r << "[" << 77 << "]," << "Kmm_" << r << "[" << 78 << "]," << "Kmm_" << r << "[" << 79 << "])" << endl;
		cout << "Km_" << r+1 << "[" << 79 << "] + S_3(" << "Kmm_" << r << "[" << 76 << "]," << "Kmm_" << r << "[" << 77 << "]," << "Kmm_" << r << "[" << 78 << "]," << "Kmm_" << r << "[" << 79 << "])" << endl;
		
		if (r%2 == 0)
		{
			for (unsigned int i = 0; i < 16; i++) cout << "Km_" << r << "[" << i << "] + K_" << r/2 << "[" << i << "]" << endl;
		}
		else
		{
			for (unsigned int i = 0; i < 16; i++) cout << "Km_" << r << "[" << i << "] + K_" << r/2 << "[" << i+16 << "]" << endl;
		}
	}
	
	
	
	
	return 0;
}

