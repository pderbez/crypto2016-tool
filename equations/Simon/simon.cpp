
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	const unsigned int nb_rounds = 23;
	const unsigned int m = 4; // key word
	const unsigned int n = 24; // block size / 2
	
	cout << "--system:" << endl;
	
	for (unsigned int r = 0; r < nb_rounds; r++)
	{
		for (unsigned int i = 0; i < n; i++)
		{
			cout << "R_" << r+1 << "[" << i << "]" << " + " <<  "L_" << r << "[" << i << "]" << endl;
		}
		
		for (unsigned int i = 0; i < n; i++)
		{
			cout << "L_" << r+1 << "[" << i << "]" << " + " <<  "R_" << r << "[" << i << "]" << " + " <<  "K_" << r << "[" << i << "]";
			cout << " + " << "L_" << r << "[" << (i+2)%n << "]";
			cout << " + " << "AND(" << "L_" << r << "[" << (i+8)%n << "]" << "," << "L_" << r << "[" << (i+1)%n << "]" << ")";
			cout << endl;
		}
	}
	
	for (unsigned int i = 0; i < n; i++)
	{
		cout << "P" << "[" << i << "]" << " + " <<  "L_0" << "[" << i << "]" << endl;
		cout << "P" << "[" << i+n << "]" << " + " <<  "R_0" << "[" << i << "]" << endl;
		cout << "C" << "[" << i << "]" << " + " <<  "L_" << nb_rounds << "[" << i << "]" << endl;
		cout << "C" << "[" << i+n << "]" << " + " <<  "R_" << nb_rounds << "[" << i << "]" << endl;
	}
	
	if (m == 2)
	{
		for (unsigned int i = 0; i+m < nb_rounds; i++)
		{
			for (unsigned int j = 0; j < n; j++)
			{
				cout << "K_" << i+m << "[" << j << "]";
				cout << " + " << "K_" << i << "[" << j << "]";
				cout << " + " << "K_" << i+1 << "[" << (j-3)%n << "]";
				cout << " + " << "K_" << i+1 << "[" << (j-4)%n << "]";
				cout << endl;
			}
		}
	}
	if (m == 3)
	{
		for (unsigned int i = 0; i+m < nb_rounds; i++)
		{
			for (unsigned int j = 0; j < n; j++)
			{
				cout << "K_" << i+m << "[" << j << "]";
				cout << " + " << "K_" << i << "[" << j << "]";
				cout << " + " << "K_" << i+2 << "[" << (j-3)%n << "]";
				cout << " + " << "K_" << i+2 << "[" << (j-4)%n << "]";
				cout << endl;
			}
		}
	}
	if (m == 4)
	{
		for (unsigned int i = 0; i+m < nb_rounds; i++)
		{
			for (unsigned int j = 0; j < n; j++)
			{
				cout << "K_" << i+m << "[" << j << "]";
				cout << " + " << "K_" << i << "[" << j << "]";
				cout << " + " << "K_" << i+3 << "[" << (j-3)%n << "]";
				cout << " + " << "K_" << i+3 << "[" << (j-4)%n << "]";
				cout << " + " << "K_" << i+1 << "[" << j << "]";
				cout << " + " << "K_" << i+1 << "[" << (j-1)%n << "]";
				cout << endl;
			}
		}
	}
	
	
	
	
	
	return 0;
}

