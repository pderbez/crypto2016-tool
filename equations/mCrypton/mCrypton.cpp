

#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	const unsigned int nb_rounds = 9;
	//const unsigned int key_size = 64;
	const unsigned int key_size = 96;
	//const unsigned int key_size = 128;
	
	unsigned int tab_mask[4][4] = {{1,1,1,0},{1,1,0,1},{1,0,1,1},{0,1,1,1}}; 
	unsigned int tab_transposition[16] = {0,4,8,12,1,5,9,13,2,6,10,14,3,7,11,15};
	
	cout << "--system:" << endl << endl;
	
	// initialization
	{
		for (unsigned int i = 0; i < 16; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				cout << "P[" << i << "][" << j << "]";
				cout << " + ";
				cout << "Kr[0][" << i << "][" << j << "]";
				cout << " + ";
				cout << "X[0][" << i << "][" << j << "]";
				cout << endl;
			}
		}
	}
	
	for (unsigned int r = 0; r < nb_rounds; r++)
	{
		// substitution
		for (unsigned int i = 0; i < 4; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				unsigned int sbox = (i + j)%4;
				if (sbox < 2)
				{
					for (unsigned int k = 0; k < 4; k++)
					{
						cout << "Y[" << r << "][" << 4*i + j << "][" << k << "]";
						cout << " + ";
						cout << "S" << sbox << "_" << k << "(";
						cout << "X[" << r << "][" << 4*i + j << "][" << 0 << "],";
						cout << "X[" << r << "][" << 4*i + j << "][" << 1 << "],";
						cout << "X[" << r << "][" << 4*i + j << "][" << 2 << "],";
						cout << "X[" << r << "][" << 4*i + j << "][" << 3 << "])";
						cout << endl;
					}
				}
				else
				{
					sbox -= 2;
					for (unsigned int k = 0; k < 4; k++)
					{
						cout << "X[" << r << "][" << 4*i + j << "][" << k << "]";
						cout << " + ";
						cout << "S" << sbox << "_" << k << "(";
						cout << "Y[" << r << "][" << 4*i + j << "][" << 0 << "],";
						cout << "Y[" << r << "][" << 4*i + j << "][" << 1 << "],";
						cout << "Y[" << r << "][" << 4*i + j << "][" << 2 << "],";
						cout << "Y[" << r << "][" << 4*i + j << "][" << 3 << "])";
						cout << endl;
					}
				}
			}
		}
		
		// mixcolumn		
		for (unsigned int i = 0; i < 4; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Z[" << r << "][" << 4*j + i << "][" << k << "]";
					for (unsigned int l = 0; l < 4; l++)
					{
						if (tab_mask[(i+j+l)%4][k] == 1)
						{
							cout << " + ";
							cout << "Y[" << r << "][" << 4*l + i << "][" << k << "]";
						}
					}
					cout << endl;
				}
			}
		}
		
		// transposition + add round key
		for (unsigned int i = 0; i < 16; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				cout << "X[" << r+1 << "][" << i << "][" << j << "]";
				cout << " + ";
				cout << "Kr[" << r+1 << "][" << i << "][" << j << "]";
				cout << " + ";
				cout << "Z[" << r << "][" << tab_transposition[i] << "][" << j << "]";
				cout << endl;
			}
		}
	}
	
	// finalization
	
	{
		// transposition
		for (unsigned int i = 0; i < 16; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				cout << "X[" << nb_rounds << "][" << i << "][" << j << "]";
				cout << " + ";
				cout << "Y[" << nb_rounds << "][" << tab_transposition[i] << "][" << j << "]";
				cout << endl;
			}
		}
		
		// mixcolumn		
		for (unsigned int i = 0; i < 4; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Z[" << nb_rounds << "][" << 4*j + i << "][" << k << "]";
					for (unsigned int l = 0; l < 4; l++)
					{
						if (tab_mask[(i+j+l)%4][k] == 1)
						{
							cout << " + ";
							cout << "Y[" << nb_rounds << "][" << 4*l + i << "][" << k << "]";
						}
					}
					cout << endl;
				}
			}
		}
		
		// transposition
		for (unsigned int i = 0; i < 16; i++)
		{
			for (unsigned int j = 0; j < 4; j++)
			{
				cout << "C[" << i << "][" << j << "]";
				cout << " + ";
				cout << "Z[" << nb_rounds << "][" << tab_transposition[i] << "][" << j << "]";
				cout << endl;
			}
		}
	}
	
	if (key_size == 64)
	{
		for (unsigned int r = 0; r <= nb_rounds; r++)
		{
			for (unsigned int i = 0; i < 4; i++)
			{
				for (unsigned int j = 0; j < 4; j++)
				{
					for (unsigned int k = 0; k < 4; k++)
					{
						cout << "Kr[" << r << "][" << 4*i + j << "][" << k << "]";
						cout << " + ";
						cout << "Ku[" << r << "][" << (4*(i+1) + j)%4 << "][" << k << "]";
						if (i == j)
						{
							cout << " + ";
							cout << "S0_" << k << "(";
							cout << "Ku[" << r << "][" << j << "][" << 0 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 1 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 2 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 3 << "])";
						}
						cout << endl;
					}
				}
			}
			
			for (unsigned int i = 0; i < 12; i++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Ku[" << r+1 << "][" << i << "][" << k << "]";
					cout << " + ";
					cout << "Ku[" << r << "][" << i+4 << "][" << k << "]";
					cout << endl;
				}
			}
			for (unsigned int i = 0; i < 16; i++)
			{
				unsigned int j = (13 + i)%16;
				cout << "Ku[" << r+1 << "][" << 12 + j/4 << "][" << j%4 << "]";
				cout << " + ";
				cout << "Ku[" << r << "][" << i/4 << "][" << i%4 << "]";
				cout << endl;
			}
		}
	}
	
	
	if (key_size == 96)
	{
		for (unsigned int r = 0; r <= nb_rounds; r++)
		{
			for (unsigned int i = 0; i < 4; i++)
			{
				for (unsigned int j = 0; j < 4; j++)
				{
					for (unsigned int k = 0; k < 4; k++)
					{
						cout << "Kr[" << r << "][" << 4*i + j << "][" << k << "]";
						cout << " + ";
						cout << "Ku[" << r << "][" << 4*(i+1) + j << "][" << k << "]";
						if (i == j)
						{
							cout << " + ";
							cout << "S0_" << k << "(";
							cout << "Ku[" << r << "][" << j << "][" << 0 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 1 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 2 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 3 << "])";
						}
						cout << endl;
					}
				}
			}
			
			for (unsigned int i = 0; i < 4; i++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Ku[" << r+1 << "][" << i << "][" << k << "]";
					cout << " + ";
					cout << "Ku[" << r << "][" << i+ 5*4 << "][" << k << "]";
					cout << endl;
				}
			}
			for (unsigned int i = 8; i < 16; i++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Ku[" << r+1 << "][" << i << "][" << k << "]";
					cout << " + ";
					cout << "Ku[" << r << "][" << i - 4 << "][" << k << "]";
					cout << endl;
				}
			}
			for (unsigned int i = 20; i < 24; i++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Ku[" << r+1 << "][" << i << "][" << k << "]";
					cout << " + ";
					cout << "Ku[" << r << "][" << i - 4 << "][" << k << "]";
					cout << endl;
				}
			}
			for (unsigned int i = 0; i < 16; i++)
			{
				unsigned int j = (13 + i)%16;
				cout << "Ku[" << r+1 << "][" << 4 + j/4 << "][" << j%4 << "]";
				cout << " + ";
				cout << "Ku[" << r << "][" << i/4 << "][" << i%4 << "]";
				cout << endl;
			}
			for (unsigned int i = 0; i < 16; i++)
			{
				unsigned int j = (8 + i)%16;
				cout << "Ku[" << r+1 << "][" << 16 + j/4 << "][" << j%4 << "]";
				cout << " + ";
				cout << "Ku[" << r << "][" << 12 + i/4 << "][" << i%4 << "]";
				cout << endl;
			}
		}
	}
	
	if (key_size == 128)
	{
		for (unsigned int r = 0; r <= nb_rounds; r++)
		{
			for (unsigned int i = 0; i < 4; i++)
			{
				for (unsigned int j = 0; j < 4; j++)
				{
					for (unsigned int k = 0; k < 4; k++)
					{
						cout << "Kr[" << r << "][" << 4*i + j << "][" << k << "]";
						cout << " + ";
						cout << "Ku[" << r << "][" << 4*(i+1) + j << "][" << k << "]";
						if (i == j)
						{
							cout << " + ";
							cout << "S0_" << k << "(";
							cout << "Ku[" << r << "][" << j << "][" << 0 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 1 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 2 << "],";
							cout << "Ku[" << r << "][" << j << "][" << 3 << "])";
						}
						cout << endl;
					}
				}
			}
			
			for (unsigned int i = 0; i < 12; i++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Ku[" << r+1 << "][" << i << "][" << k << "]";
					cout << " + ";
					cout << "Ku[" << r << "][" << i+ 5*4 << "][" << k << "]";
					cout << endl;
				}
			}
			for (unsigned int i = 16; i < 28; i++)
			{
				for (unsigned int k = 0; k < 4; k++)
				{
					cout << "Ku[" << r+1 << "][" << i << "][" << k << "]";
					cout << " + ";
					cout << "Ku[" << r << "][" << i - 12 << "][" << k << "]";
					cout << endl;
				}
			}
			for (unsigned int i = 0; i < 16; i++)
			{
				unsigned int j = (13 + i)%16;
				cout << "Ku[" << r+1 << "][" << 12 + j/4 << "][" << j%4 << "]";
				cout << " + ";
				cout << "Ku[" << r << "][" << i/4 << "][" << i%4 << "]";
				cout << endl;
			}
			for (unsigned int i = 0; i < 16; i++)
			{
				unsigned int j = (8 + i)%16;
				cout << "Ku[" << r+1 << "][" << 28 + j/4 << "][" << j%4 << "]";
				cout << " + ";
				cout << "Ku[" << r << "][" << 16 + i/4 << "][" << i%4 << "]";
				cout << endl;
			}
		}
	}
	
	return 0;
}

