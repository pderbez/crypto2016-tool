/*
 * SetOfVars.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_SET_OF_VARS
#define DEF_SET_OF_VARS

#include "SetOf.h"
#include "Variable.h"

typedef SetOf<Variable> SetOfVars;
typedef SubsetOf<Variable> SubsetOfVars;

//typedef SubsetOfVars SubsetOfVars_s;

/*
class SubsetOfVars_s : public SubsetOfVars
{
	public:
		SubsetOfVars_s();
		SubsetOfVars_s(SubsetOfVars_s const &);
		SubsetOfVars_s(SubsetOfVars_s &&);
		
		SubsetOfVars_s & operator=(SubsetOfVars_s const &);
		SubsetOfVars_s & operator=(SubsetOfVars_s &&);
		
		SubsetOfVars_s(SubsetOfVars const &);
		SubsetOfVars_s(SubsetOfVars &&);
		SubsetOfVars_s & operator=(SubsetOfVars const &);
		SubsetOfVars_s & operator=(SubsetOfVars &&);
		
		~SubsetOfVars_s();
		
		unsigned int size() const override {return m_size;};
		
		SubsetOfVars_s & operator+=(SubsetOfVars const &); // union
		SubsetOfVars_s & operator-=(SubsetOfVars const &); // difference
		SubsetOfVars_s & operator&=(SubsetOfVars const &); // intersection
		SubsetOfVars_s & operator|=(SubsetOfVars const & s) {return *this += s;}; // union
		
		SubsetOfVars_s & operator+=(Variable const &);
		SubsetOfVars_s & operator-=(Variable const &);
		SubsetOfVars_s & operator&=(Variable const &);
		SubsetOfVars_s & operator|=(Variable const & v) {return *this += v;};
		
		void clear() override;
		bool is_empty() const override {return (m_size == 0);};
		
	private:
		unsigned int m_size;
};*/

#endif
