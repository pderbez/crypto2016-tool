/*
 * MitMAttack_eq.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_MITM_ATTACK_EQS
#define DEF_MITM_ATTACK_EQS

#include <set>
#include <map>
#include <list>

class MitMAttack_Core;
class BlockCipher;

#include "PackOfVars.h"

class MitMAttack_Eqs
{
	public:
		MitMAttack_Eqs(MitMAttack_Eqs const &);
		MitMAttack_Eqs(MitMAttack_Eqs &&);
		
		MitMAttack_Eqs & operator=(MitMAttack_Eqs const &);
		MitMAttack_Eqs & operator=(MitMAttack_Eqs &&);
		
		~MitMAttack_Eqs();
		
		MitMAttack_Eqs(MitMAttack_Core const &);
		
		
		friend bool operator==(MitMAttack_Eqs const &, MitMAttack_Eqs const &);
		friend bool operator!=(MitMAttack_Eqs const & A1, MitMAttack_Eqs const & A2) {return !(A1 == A2);};
		friend bool operator<(MitMAttack_Eqs const &, MitMAttack_Eqs const &);
		
		int nbEqs() const {return m_nb_eqs;};
		int timeOnline() const;
		SubsetOfPacks const & activeOn() const {return *m_active_on;};
		SubsetOfPacks const & activeOff() const {return *m_active_off;};
		SubsetOfPacks const & guessOn() const {return *m_guess_on;};
		SubsetOfPacks const & guessOff() const {return *m_guess_off;};
		SubsetOfPacks const & null() const {return m_null;};
		
		BlockCipher const & getBlockCipher() const {return *m_B;};
		
	private:
		static std::set<SubsetOfPacks> st_all_online;
		static std::set<SubsetOfPacks> st_all_offline;
		static std::map<SubsetOfPacks const *,int> st_all_time;
		
		SubsetOfPacks const * m_active_on;
		SubsetOfPacks const * m_active_off;
		SubsetOfPacks const * m_guess_on;
		SubsetOfPacks const * m_guess_off;
		SubsetOfPacks m_null;
		int m_nb_eqs;
		BlockCipher const * m_B;
		
		
		void updateNbEqs();
		void setBlockCipher(BlockCipher const &);
		
		friend std::list<MitMAttack_Eqs> findMitMAttacks_Eqs_fromP(int, int, int, int, BlockCipher const &);
};

bool operator==(MitMAttack_Eqs const &, MitMAttack_Eqs const &);
bool operator<(MitMAttack_Eqs const &, MitMAttack_Eqs const &);

std::list<MitMAttack_Eqs> findMitMAttacks_Eqs_fromC(int, int, int, int, BlockCipher const &);
std::list<MitMAttack_Eqs> findSimpleMitMAttacks_Eqs_fromC(int, int, BlockCipher const &);

#endif
