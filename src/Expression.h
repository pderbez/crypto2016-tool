/*
 * Expression.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#ifndef DEF_EXPRESSION
#define DEF_EXPRESSION

#include <iostream>
#include "Variable.h"
#include "GFElement.h"
#include <map>
#include <string>
#include <list>
#include <set>

class Sbox;
class SofExpression;

class Expression
{
	
	public:
		Expression(Expression &&);
		Expression(Expression const&);
		Expression(SofExpression const&);
		Expression(Variable const&);
		Expression(const GFElement = 0);
		Expression(const GFSymbol);
		Expression(std::string const&);
		Expression(Sbox const&, std::vector<Expression> const& Exprs);

		Expression& operator=(Expression const&);
		Expression& operator=(Expression &&);
		Expression& operator=(SofExpression const&);
		Expression& operator=(Variable const&);
		Expression& operator=(const GFElement);
		Expression& operator=(const GFSymbol);
		// Member functions
		
		Expression& operator+=(Expression const&);
		Expression& operator+=(SofExpression const&);
		Expression& operator+=(Variable const&);
		Expression& operator+=(const GFElement);
		Expression& operator+=(const GFSymbol);
		
		Expression& operator*=(const GFElement);
		Expression& operator/=(const GFElement);
		Expression& operator*=(const GFSymbol);
		Expression& operator/=(const GFSymbol);	
		
		GFElement getConstant()	const { return m_cst; };
		
		std::set<Variable> getVariables() const;
		void getVariables(std::set<Variable> &) const;
		
		int_fast8_t getTypeSbox() const;
		
		bool isConstant() const { return (m_lin.size() == 0 && m_nonlin.size() == 0); };
		bool isVariable() const;
		bool isNormalized() const; // each SofExpression looks like S(x_1,...,x_n) <-> no nested Sboxes
		
		typedef std::map<Variable,GFElement>::iterator iterator_lin;
		typedef std::map<SofExpression,GFElement>::iterator iterator_nonlin;
		
		typedef std::map<Variable,GFElement>::const_iterator const_iterator_lin;
		typedef std::map<SofExpression,GFElement>::const_iterator const_iterator_nonlin;
		
		iterator_lin lin_begin() { return m_lin.begin(); }; 
		iterator_lin lin_end() { return m_lin.end(); }; 
		
		const_iterator_lin lin_begin() const { return m_lin.begin(); }; 
		const_iterator_lin lin_end() const { return m_lin.end(); };
		
		const_iterator_lin lin_cbegin() const { return m_lin.cbegin(); }; 
		const_iterator_lin lin_cend() const { return m_lin.cend(); };
		
		iterator_nonlin nonlin_begin() { return m_nonlin.begin(); }; 
		iterator_nonlin nonlin_end() { return m_nonlin.end(); }; 
		
		const_iterator_nonlin nonlin_begin() const { return m_nonlin.begin(); }; 
		const_iterator_nonlin nonlin_end() const { return m_nonlin.end(); };
		
		const_iterator_nonlin nonlin_cbegin() const { return m_nonlin.cbegin(); }; 
		const_iterator_nonlin nonlin_cend() const { return m_nonlin.cend(); };
		
		bool replace(Variable old, Variable x);
		
		friend bool operator==(Expression const &, Expression const &);
		friend bool operator==(Expression const &, Variable const &);
		friend bool operator==(Expression const &, SofExpression const &);
		friend bool operator==(Variable const & v, Expression const & expr) {return (expr == v);};
		friend bool operator==(SofExpression const & sexpr, Expression const & expr) {return (expr == sexpr);};
		
		friend bool operator!=(Expression const & expr1, Expression const & expr2) {return !(expr1 == expr2);};
		friend bool operator!=(Expression const & expr, Variable const &v) {return !(expr == v);};
		friend bool operator!=(Variable const & v, Expression const & expr) {return (expr != v);};
		friend bool operator!=(Expression const & expr, SofExpression const & sexpr) {return !(expr == sexpr);};
		friend bool operator!=(SofExpression const & sexpr, Expression const & expr) {return (expr != sexpr);};
		
		friend std::ostream& operator<<( std::ostream &flux, Expression const& var);
		friend bool areColinear(Expression const& expr1, Expression const& expr2);
		
		template <typename Iter> friend typename std::enable_if<std::is_same<Expression, typename Iter::value_type>::value,std::list<Expression>>::type normalize(Iter begin, const Iter end);
		
	private:
	
		// Member variables
		GFElement m_cst; // the constant
		std::map<Variable,GFElement> m_lin; // the linear part of the expression
		std::map<SofExpression,GFElement> m_nonlin; // the non-linear part
		
		void print(std::ostream &flux) const;	
		
	
		friend bool operator<(Expression const& expr1, Expression const& expr2); //used for std::map	
	
	
	
	
		
};

Expression operator+(Expression const& expr1, Expression const& expr2);
Expression operator+(Expression const& expr1, SofExpression const& expr2);
Expression operator+(Expression const& expr1, Variable const& expr2);
Expression operator+(Expression const& expr1, const GFElement expr2);
Expression operator+(Expression const& expr1, const GFSymbol expr2);
	
inline Expression operator+(SofExpression const& expr1, Expression const& expr2) { return expr2 + expr1; };
Expression operator+(SofExpression const& expr1, SofExpression const& expr2);
Expression operator+(SofExpression const& expr1, Variable const& expr2);
Expression operator+(SofExpression const& expr1, const GFElement expr2);
Expression operator+(SofExpression const& expr1, const GFSymbol expr2);
	
inline Expression operator+(Variable const& expr1, Expression const& expr2) { return expr2 + expr1; };
inline Expression operator+(Variable const& expr1, SofExpression const& expr2) { return expr2 + expr1; };
Expression operator+(Variable const& expr1, Variable const& expr2);
Expression operator+(Variable const& expr1, const GFElement expr2);
Expression operator+(Variable const& expr1, const GFSymbol expr2);
	
inline Expression operator+(const GFElement expr1, Expression const& expr2) { return expr2 + expr1; };
inline Expression operator+(const GFElement expr1, SofExpression const& expr2) { return expr2 + expr1; };
inline Expression operator+(const GFElement expr1, Variable const& expr2) { return expr2 + expr1; };
	
inline Expression operator+(const GFSymbol expr1, Expression const& expr2) { return expr2 + expr1; };
inline Expression operator+(const GFSymbol expr1, SofExpression const& expr2) { return expr2 + expr1; };
inline Expression operator+(const GFSymbol expr1, Variable const& expr2) { return expr2 + expr1; };


Expression operator*(const GFElement val, Expression const& expr);
Expression operator*(const GFSymbol val, Expression const& expr);
Expression operator*(const GFElement val, SofExpression const& expr);
Expression operator*(const GFSymbol val, SofExpression const& expr);
Expression operator*(const GFElement val, Variable const& expr);
Expression operator*(const GFSymbol val, Variable const& expr);

inline Expression operator*(Expression const& expr, const GFElement val) { return val*expr; };
inline Expression operator*(Expression const& expr, const GFSymbol val) { return val*expr; };
inline Expression operator*(SofExpression const& expr, const GFElement val) { return val*expr; };
inline Expression operator*(SofExpression const& expr, const GFSymbol val) { return val*expr; };
inline Expression operator*(Variable const& expr, const GFElement val) { return val*expr; };
inline Expression operator*(Variable const& expr, const GFSymbol val) { return val*expr; };

#include "Expression.tpp"

#endif
