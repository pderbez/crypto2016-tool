/*
 * main.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 

#include <boost/program_options.hpp>

#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <string>

#include "SystemOfEquations.h"
#include "BlockCipher.h"
#include "MitMAttack.h"
#include "SimpleMitMAttack.h"
#include "ImpDiffAttack.h"


namespace po = boost::program_options;
using namespace std;

int main(int argc, char **argv)
{
	int time,data,memory;
	int in,out;
	
	po::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "produce help message")
		("time,t", po::value<int>(&time), "time bound (default: keysize - 1)")
		("data,d", po::value<int>(&data), "data bound (default: statesize - 1)")
		("memory,m", po::value<int>(&memory), "memory bound (default: keysize + statesize - 1)")
		("in,i", po::value<int>(&in), "size delta-set min (default: 1)")
		("out,o", po::value<int>(&out), "number of eqs min (default: 1)")
	;
	
	po::options_description hidden("Hidden options");
	hidden.add_options()
		("equation", po::value<string>(), "File of the system of equations to solve")
	;
	
	po::positional_options_description p;
    p.add("equation", -1);
    
    po::options_description all_options;
    all_options.add(desc).add(hidden);
	
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(all_options).positional(p).run(), vm);
	po::notify(vm); 
	
	if (vm.count("help"))
	{
		cout << desc << "\n";
		return 1;
	}
	
	string eqs_file;
	if (vm.count("equation"))
	{
		eqs_file = vm["equation"].as<string>();
		cout << "System of Equations: " << eqs_file << endl;
	}
	else
	{
		cout << "System of equations missing...";
		exit(EXIT_FAILURE);
	}
	
	cout << "  -- GDS Attacks Search Tool --  " << endl << endl;
	
    {
		SystemOfEquations sys(eqs_file);
		BlockCipher B(sys);
				
		if (!vm.count("time") || time < 0 || time >= B.getKeysize()) time = B.getKeysize()-1;
		if (!vm.count("memory") || memory < 0 || memory >= B.getKeysize()+B.getStatesize()) memory = time + B.getStatesize();
		if (!vm.count("data") || data < 0 || data > B.getStatesize()) data = B.getStatesize() - 1;
		
		if (!vm.count("in")) in = 1;
		if (!vm.count("out")) out = 1;
		
		cout << endl;
		
		cout << "Search Parameters: " << endl;
		cout << " - size delta-set min: " << in << endl;
		cout << " - nb of eqs: " << out << endl;
		cout << endl;
		findMitMAttacks_fromP(data+1,time+1,memory+1,B,B.getStatesize()-in,out);
		
	}
    
    
	return 0;
}

