/*
 * SystemOfEquations.tpp
 * 
 * Copyright 2014 pderbez <pderbez@pderbez-VirtualBox>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "GFElement.h"
#include "Expression.h"

 
template <typename Iter> 
SystemOfEquations::SystemOfEquations(Iter begin, Iter end, bool simplify) : m_known_vars()
{
	std::list<Expression> L = normalize(begin,end);
	
	std::set<Variable> all_vars;
	for (auto itL = L.begin(); itL != L.end() ; ++itL) itL->getVariables(all_vars);
	m_set_of_vars = SetOfVars(all_vars.begin(),all_vars.end());
	
	std::map<Variable,unsigned int> var_col;
	std::map<SofExpression,unsigned int> sexpr_col;
	unsigned int col_cst = 0;
	unsigned int c = 1;
	
	for (auto itL = L.begin(); itL != L.end() ; ++itL)
	{
		for (auto it = itL->lin_begin(); it != itL->lin_end() ; ++it)
		{
			Variable const &v = it->first;
			if (var_col.find(v) == var_col.end()) var_col[v] = c++;
		}
		for (auto it = itL->nonlin_begin(); it != itL->nonlin_end() ; ++it)
		{
			SofExpression const &v = it->first;
			if (sexpr_col.find(v) == sexpr_col.end()) sexpr_col[v] = c++;
		}
	}
	
	const unsigned int bound_c = c;
	
	Matrix<GFElement> mat(L.size(),bound_c,0);
	
	unsigned int l = 0;
	for (auto itL = L.begin(); itL != L.end() ; ++itL, ++l)
	{
		mat(l,0) = itL->getConstant();
		for (auto it = itL->lin_begin(); it != itL->lin_end() ; ++it) mat(l,var_col[it->first]) = it->second;
		for (auto it = itL->nonlin_begin(); it != itL->nonlin_end() ; ++it) mat(l,sexpr_col[it->first]) = it->second;
	}
	const unsigned int bound_l = l;
	
	const unsigned int rank = mat.Gauss();
	mat.resize(rank,bound_c);
	
	m_original_equations = move(L);
	
	m_matrix = std::move(mat);
	
	m_expr_by_col.reserve(bound_c);
	for (unsigned int i = 0; i < bound_c; ++i) m_expr_by_col.emplace_back(0);
	
	m_expr_by_col[0] += 1;
	
	for (auto it = var_col.begin(); it != var_col.end(); ++it) m_expr_by_col[it->second] += it->first;

	for (auto it = sexpr_col.begin(); it != sexpr_col.end(); ++it) m_expr_by_col[it->second] += it->first;
	
	this->initialize_SubSystem();
	
	if (simplify)
	{
		std::cout << "Looking for fake equations: " << std::flush;
		this->simplify();
		std::cout << "done" << std::endl;
	}
}



template <typename Iter>
void SystemOfEquations::setKnownVars(Iter it, const Iter end)
{
	m_known_vars.insert(it,end);
	m_E.setKnownVars(m_known_vars.begin(),m_known_vars.end());
}
