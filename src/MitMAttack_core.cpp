/*
 * MitMAttack_core.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "MitMAttack_core.h"
#include "MitMAttack_diff.h"
#include "MitMAttack_eqs.h"

const int MitMAttack_Core::tt[7][7] = {{0,-1,-1,0,0,-1,0},{-1,1,-1,1,-1,1,1},{-1,-1,2,-1,2,2,2},{0,1,-1,3,0,1,3},{0,-1,2,0,4,2,4},{-1,1,2,1,2,5,5},{0,1,2,3,4,5,6}};	

using namespace std;

MitMAttack_Core origin_MitM_Diff_fromP(BlockCipher const & B, const int bound_data, const int bound_time, const int bound_memory);
MitMAttack_Core origin_MitM_Eqs_fromC(BlockCipher const & B, const int bound_data, const int bound_time, const int bound_memory);

MitMAttack_Core::MitMAttack_Core(BlockCipher const & B) : m_B(addressof(B)), m_valid(true)
{
	t_vars = new SubsetOfPacks [9];
	SetOf<PackOfVars> const & sop = m_B->getSetOfPacks();
	SubsetOfPacks empty_set = sop.empty_set();
	for (unsigned int i = 0; i < 9; ++i) t_vars[i] = empty_set;
	t_vars[6] = m_B->getStateVars();
}
		
MitMAttack_Core::MitMAttack_Core(MitMAttack_Core const & A) : m_B(A.m_B), m_valid(A.m_valid)
{
	t_vars = new SubsetOfPacks [9];
	for (int i = 0; i < 9; ++i) t_vars[i] = A.t_vars[i];
}

MitMAttack_Core::MitMAttack_Core(MitMAttack_Core && A) : t_vars(A.t_vars), m_B(A.m_B), m_valid(A.m_valid)
{
	A.t_vars = new SubsetOfPacks [9];
}
		
MitMAttack_Core & MitMAttack_Core::operator=(MitMAttack_Core const & A)
{
	for (int i = 0; i < 9; ++i) t_vars[i] = A.t_vars[i];
	m_B = A.m_B;
	m_valid = A.m_valid;
	return *this;
}

MitMAttack_Core & MitMAttack_Core::operator=(MitMAttack_Core && A)
{
	swap(t_vars,A.t_vars);
	m_B = A.m_B;
	m_valid = A.m_valid;
	return *this;
}
		
MitMAttack_Core::~MitMAttack_Core()
{
	delete[] t_vars;
}

bool MitMAttack_Core::time_online_Diff_P_smaller_than(const int time) const
{
	SubsetOfPacks const wanted = t_vars[7] + m_B->getPlaintextVars();
	BlockCipher const & B = *m_B;
	if (B.isSPN()) return B.getEqsNonLinKey().nb_sols_smaller_than(time + B.getStatesize(),wanted,B.getNonLinKeyVars() + t_vars[1]);
	else
	{
		SubsetOfPacks maybe = B.getNonLinKeyVars();
		auto tmp = (t_vars[1]+t_vars[7]).get();
		for (auto const & v : tmp) maybe += B.varsFromPtoV(v);
		return B.getEqsNonLinKey().nb_sols_smaller_than(time + B.getStatesize(),wanted,maybe);
	}
}

bool MitMAttack_Core::time_online_Eqs_C_smaller_than(const int time) const
{
	SubsetOfPacks const wanted = t_vars[7] + m_B->getCiphertextVars();
	return m_B->getEqsNonLinKey().nb_sols_smaller_than(time + m_B->getStatesize(),wanted,m_B->getNonLinKeyVars() + t_vars[1]);
}

int MitMAttack_Core::data_fromP() const
{
	SubSystem const & E = m_B->getEqsKnownKey();
	SubsetOfPacks tmp = t_vars[0] + t_vars[3] + t_vars[4] + t_vars[6];
	const int nb_sols_wo_p = E.getNbSols(tmp);
	tmp += m_B->getPlaintextVars();
	const int nb_sols_w_p = E.getNbSols(tmp);
	return nb_sols_w_p - nb_sols_wo_p;
}

template <typename T>
vector<PackOfVars> find_related_packs(T const & p, SubsetOfPacks packs, SubSystem const & E)
{
	if (!E.testRelatedPacks(p,packs)) return vector<PackOfVars>();
	
	vector<PackOfVars> vec = packs.get();
	unsigned int i = 0;
	unsigned int vec_size = vec.size();
	while (i < vec_size)
	{
		SubsetOfPacks tmp = packs - vec[i];
		if (E.testRelatedPacks(p,tmp))
		{
			packs = move(tmp);
			vec[i] = move(vec[--vec_size]);
		}
		else ++i;
	}
	vec.resize(vec_size);
	return vec;	
}


template <typename T>
vector<PackOfVars> find_related_packs_nonlinear(T const & p, SubsetOfPacks & packs, SubSystem const & E)
{
	if (!E.testRelatedPacksNonLinear(p,packs)) return vector<PackOfVars>();
	
	vector<PackOfVars> vec = packs.get();
	unsigned int i = 0;
	unsigned int vec_size = vec.size();
	while (i < vec_size)
	{
		SubsetOfPacks tmp = packs - vec[i];
		if (E.testRelatedPacksNonLinear(p,tmp))
		{
			packs = move(tmp);
			vec[i] = move(vec[--vec_size]);
		}
		else ++i;
	}
	vec.resize(vec_size);
	return vec;	
}

void MitMAttack_Core::propagateZeros(bool check)
{
	SubSystem const & E = m_B->getEqsKnownKey();
	int dim_diff = E.getNbSols(t_vars[0]);
	if (dim_diff >= m_B->getStatesize() && check)
	{
		m_valid = false;
		return;
	}
	const unsigned int index_right[3] = {3,4,6};
	const unsigned int index_wrong[3] = {1,2,5};
	
	vector<PackOfVars> my_vec_of_vec[3];
	for (unsigned int i = 0; i < 3; ++i) my_vec_of_vec[i] =  t_vars[index_right[i]].get();
	
	
start_propagate:
	{
		for (unsigned int i = 0; i < 3; ++i)
		{
			const unsigned int loop_bound = my_vec_of_vec[i].size();
			for (unsigned int j = 0; j < loop_bound; ++j)
			{
				PackOfVars & p = my_vec_of_vec[i][j];
				auto tmp = t_vars[0] + p;
				const int nb_sols = E.getNbSols(tmp);
				if (nb_sols <= dim_diff)
				{
					dim_diff = nb_sols;
					t_vars[0] = move(tmp);
					t_vars[index_right[i]] -= p;
					p = move(my_vec_of_vec[i][loop_bound - 1]);
					my_vec_of_vec[i].pop_back();
					goto start_propagate;
				}
			}
		}
	}
	
	for (unsigned int i = 0; i < 3; ++i)
	{
		vector<PackOfVars> my_vec = t_vars[index_wrong[i]].get();
		const unsigned int loop_bound = my_vec.size();
		for (unsigned int j = 0; j < loop_bound; ++j)
		{
			PackOfVars const & p = my_vec[j];
			const int nb_sols = E.getNbSols(t_vars[0] + p);
			if (nb_sols <= dim_diff)
			{
				m_valid = false;
				return;
			}
		}
	}
}

bool MitMAttack_Core::updateAsDiff_tmp(SubsetOfPacks const & s, const int type)
{
	bool res = false;
	for (int i = 0; i <= 6; ++i)
	{
		const int new_type = tt[type][i];
		if (new_type >= 0)
		{
			if (new_type != i)
			{
				SubsetOfPacks const new_s = s & t_vars[i];
				if (new_type == 1) res = (res || !new_s.is_empty()); // update of data and time complexities required
				t_vars[i] -= new_s;
				t_vars[new_type] += new_s;
			}
		}
		else
		{
			if (s.shareElements(t_vars[i])) // s and t_vars[i] have some elements in common
			{
				m_valid = false;
				return res;
			}
		}
	}
	return res;
}
		
void MitMAttack_Core::updateAsDiff_fromP(PackOfVars const & p, const int type, const int bound_data, const int bound_time)
{
	assert(type >= 0 && type <= 6);
	
	bool m_time_need_up = false;	
	switch (type)
	{
		case 0:
			{
				auto const tmp1 = this->updateAsDiff_tmp(m_B->varsFromPtoV(p),3);
				if (!this->isValid()) return;
				auto const tmp2 = this->updateAsDiff_tmp(m_B->varsFromCtoV(p),4);
				if (!this->isValid()) return;
				m_time_need_up = tmp1 || tmp2;
			}
			break;
		case 1:
			{
				auto const tmp1 = this->updateAsDiff_tmp(m_B->varsFromVtoP(p),1);
				if (!this->isValid()) return;
				auto const tmp2 = this->updateAsDiff_tmp(m_B->varsFromPtoV(p) + m_B->guessToP(p),3);
				if (!this->isValid()) return;
				t_vars[7] += m_B->guessToP(p);
				m_time_need_up = tmp1 || tmp2;
			}
			break;
		case 2:
			{
				auto const tmp1 = this->updateAsDiff_tmp(m_B->varsFromVtoC(p),2);
				if (!this->isValid()) return;
				auto const tmp2 = this->updateAsDiff_tmp(m_B->varsFromCtoV(p) + m_B->guessToC(p),4);
				if (!this->isValid()) return;
				t_vars[8] += m_B->guessToC(p);
			}
			break;
		case 3:
			{
				m_time_need_up = this->updateAsDiff_tmp(m_B->varsFromPtoV(p),3);
				if (!this->isValid()) return;
			}
			break;
		case 4:
			{
				this->updateAsDiff_tmp(m_B->varsFromCtoV(p),4);
				if (!this->isValid()) return;
			}
			break;
		case 5:
			{
				for (int i = 0; i <= 6; ++i)
				{
					if (t_vars[i].includes(p))
					{
						const int new_type = tt[5][i];
						if (new_type == -1)
						{
							m_valid = false;
							return;
						}
						if (new_type != 5)
						{
							return this->updateAsDiff_fromP(p,new_type, bound_data, bound_time);
						}
						else
						{
							t_vars[i] -= p;
							t_vars[5] += p;
							return;
						}
					}
				}
			}
			break;
		default:
			break;
	}
	
	if (m_time_need_up && (this->data_fromP() >= bound_data || !this->time_online_Diff_P_smaller_than(bound_time))) m_valid = false;
}

bool MitMAttack_Core::allVarsRequired() const
{
	if (t_vars[5].is_empty())
	{
		SubsetOfPacks tmp = t_vars[1] + t_vars[2];
		SubSystem F = m_B->getEqsKnownKey().Extract(t_vars[1] + m_B->getCiphertextVars(), t_vars[2] + m_B->getPlaintextVars());
		tmp &= m_B->getStateVars();
		vector<PackOfVars> my_vec = tmp.get();
		for (auto const & p : my_vec)
		{
			if (F.isLinear(p)) return false;
		}
		return true;
	}
	else return false;
}

int MitMAttack_Core::nbEqs_fromC() const
{
	SubSystem const & E = m_B->getEqsKnownKey();
	const int s = E.getNbSolsNotBoth(t_vars[1] + m_B->getCiphertextVars(),t_vars[2]);
	//const int s_P = E.getNbSols(t_vars[2]);
	const int s_P = static_cast<int>((t_vars[2].getVars() & m_B->getPlaintextVars().getVars()).size());
	return s_P + m_B->getStatesize() - s;
}

int MitMAttack_Core::nbEqs() const
{
	SubSystem const & E = m_B->getEqsKnownKey();
	const int s_P = E.getNbSols(t_vars[2]);
	const int s = E.getNbSols(t_vars[1] + t_vars[2] + m_B->getPlaintextVars() + m_B->getCiphertextVars());
	return s_P + m_B->getStatesize() - s;
}

int MitMAttack_Core::nbEqsMax() const
{
	SubSystem const & E = m_B->getEqsKnownKey();
	auto const tmp = t_vars[2] + t_vars[4] + t_vars[5] + t_vars[6];
	const int s_P = E.getNbSols(tmp);
	const int s = E.getNbSols(t_vars[1] + t_vars[3] + tmp + m_B->getPlaintextVars() + m_B->getCiphertextVars());
	return s_P + m_B->getStatesize() - s;
}

bool MitMAttack_Core::updateAsEqs_tmp(SubsetOfPacks const & s, const int type)
{
	bool res = false;
	for (int i = 0; i <= 6; ++i)
	{
		const int new_type = tt[type][i];
		if (new_type >= 0)
		{
			if (new_type != i)
			{
				SubsetOfPacks const new_s = s & t_vars[i];
				if (new_type == 1) res = res || !new_s.is_empty(); // update of time complexity required
				t_vars[i] -= new_s;
				t_vars[new_type] += new_s;
			}
		}
		else
		{
			if (s.shareElements(t_vars[i])) // s and t_vars[i] have some elements in common
			{
				m_valid = false;
				return res;
			}
		}
	}
	return res;
}

void MitMAttack_Core::updateAsEqs_fromC(PackOfVars const & p, const int type, const int bound_time)
{
	assert(type >= 0 && type <= 6);
	
	bool m_time_need_up = false;
	
	switch (type)
	{
		case 0:
			{
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromVtoC(p),3);
				if (!this->isValid()) return;
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromVtoP(p),4);
				if (!this->isValid()) return;
			}
			break;
		case 1:
			{
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromCtoV(p),1);
				if (!this->isValid()) return;
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromVtoC(p) + m_B->guessToC(p),3);
				if (!this->isValid()) return;
				t_vars[7] += m_B->guessFromC(p);
			}
			break;
		case 2:
			{
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromPtoV(p),2);
				if (!this->isValid()) return;
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromVtoP(p) + m_B->guessToP(p),4);
				if (!this->isValid()) return;
				t_vars[8] += m_B->guessFromP(p);
			}
			break;
		case 3:
			{
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromVtoC(p),3);
				if (!this->isValid()) return;
			}
			break;
		case 4:
			{
				m_time_need_up = m_time_need_up || this->updateAsEqs_tmp(m_B->varsFromVtoP(p),4);
				if (!this->isValid()) return;
			}
			break;
		case 5:
			{
				for (int i = 0; i <= 6; ++i)
				{
					if (t_vars[i].includes(p))
					{
						const int new_type = tt[5][i];
						if (new_type == -1)
						{
							m_valid = false;
							return;
						}
						if (new_type != 5) return this->updateAsEqs_fromC(p,new_type,bound_time);
						else
						{
							t_vars[i] -= p;
							t_vars[5] += p;
							return;
						}
					}
				}
			}
			break;
		default:
			break;
	}
	
	if (m_time_need_up) m_valid = this->time_online_Eqs_C_smaller_than(bound_time);
}

void findMitMAttacks_Diff_fromP_lead_tmp_fast(MitMAttack_Core & A, PackOfVars const & lead, map<pair<unsigned int, unsigned int>,set<SubsetOfPacks>> & my_map,
 const int bound_data, const int bound_time, const int bound_dim)
{
	SubsetOfPacks const maybe_non_zero = A.t_vars[3] + A.t_vars[4] + A.t_vars[6];
	SubSystem E = A.m_B->getEqsKnownKey().Extract(A.t_vars[0] + maybe_non_zero + lead);
	E.setKnownVars(A.t_vars[0]);
	SubSystem F = E.extractRelated(lead);
	auto sub = maybe_non_zero.empty_set();
	auto vec = maybe_non_zero.get();
	for (auto const & p : vec)
	{
		if (F.involves(p)) sub += p;
	}
	findMitMAttacks_Diff_fromP_lead_tmp_fast_tmp(A,lead,my_map,F,sub,bound_data,bound_time,bound_dim);
}

void findMitMAttacks_Diff_fromP_lead_tmp_fast_tmp(MitMAttack_Core & A, PackOfVars const & lead, map<pair<unsigned int, unsigned int>,set<SubsetOfPacks>> & my_map,
 SubSystem & E, SubsetOfPacks const & sub, const int bound_data, const int bound_time, const int bound_dim)
{
	stack<MitMAttack_Core> waitingA;
	stack<SubSystem> waitingE;
	waitingA.emplace(move(A));
	waitingE.emplace(move(E));
	
	map<SubsetOfPacks, set<SubsetOfPacks>> waitingNoLead;
	
start:
	while (!waitingA.empty())
	{
		A = move(waitingA.top());
		waitingA.pop();
		E = move(waitingE.top());
		waitingE.pop();
		
		const unsigned int size_in = A.t_vars[1].getVars().size();
		const unsigned int size_out = A.t_vars[2].getVars().size();
		for (auto const & m : my_map)
		{
			if ((m.first.first <= size_in && m.first.second < size_out) || (m.first.first < size_in && m.first.second <= size_out)) goto start;
		}
		
		SubsetOfPacks const maybe_non_zero = A.t_vars[3] + A.t_vars[4] + A.t_vars[6];
		SubsetOfPacks const maybe_zero = A.t_vars[0] + maybe_non_zero;
		SubSystem F = E.Extract(maybe_zero + lead);
		F.setKnownVars(A.t_vars[0]);
		vector<PackOfVars> related = find_related_packs(lead,(maybe_non_zero & sub),F);
		if (related.empty())
		{
			auto A_saved = A;
			
			A.t_vars[0] = maybe_zero;
			A.t_vars[3].clear();
			A.t_vars[4].clear();
			A.t_vars[6].clear();
			A.propagateZeros();
			
			if (A.isValid())
			{
				SubSystem const & G = A.m_B->getEqsKnownKey();
				const int dim_diff = G.getNbSols(A.t_vars[0]);
				
				if (dim_diff <= bound_dim)
				{
					auto it = my_map.begin();
					while ( it != my_map.end() )
					{
						if ((size_in <= it->first.first && size_out < it->first.second) || (size_in < it->first.first && size_out <= it->first.second)) it = my_map.erase(it);
						else ++it;
					}
					my_map[make_pair(size_in,size_out)].emplace(A.t_vars[0]);
				}
				else
				{
					related = (A_saved.t_vars[4] + A_saved.t_vars[6]).get();
					A.m_B->sortVectorFromPtoV(related);
					for (auto const & p : related)
					{
						MitMAttack_Core A_tmp = A_saved;
						A_tmp.updateAsDiff_fromP(p,2,bound_data,bound_time);
						if (A_tmp.isValid()) findMitMAttacks_Diff_fromP_lead_tmp_fast(A_tmp,p,my_map,bound_data,bound_time,bound_dim);
						A_saved.updateAsDiff_fromP(p,3,bound_data,bound_time);
						if (!A_saved.isValid()) break;
					}
					if (A_saved.isValid())
					{
						A_saved.t_vars[0] += A_saved.t_vars[4];
						A_saved.t_vars[3] += A_saved.t_vars[6];
						A_saved.t_vars[4].clear();
						A_saved.t_vars[6].clear();
						A_saved.propagateZeros();
						related = A_saved.t_vars[3].get();
						A.m_B->sortVectorFromCtoV(related);
						for (auto const & p : related)
						{
							MitMAttack_Core A_tmp = A_saved;
							A_tmp.updateAsDiff_fromP(p,1,bound_data,bound_time);
							if (A_tmp.isValid()) findMitMAttacks_Diff_fromP_lead_tmp_fast(A_tmp,p,my_map,bound_data,bound_time,bound_dim);
							A_saved.updateAsDiff_fromP(p,0,bound_data,bound_time);
							if (!A_saved.isValid()) break;
						}
					}
				}
			}	
		}
		else
		{
			for (auto const & p : related)
			{
				{
					MitMAttack_Core A_tmp = A;
					A_tmp.updateAsDiff_fromP(p,1,bound_data,bound_time);
					if (A_tmp.isValid())
					{
						waitingA.emplace(move(A_tmp));
						waitingE.emplace(F);
					}
				}
				{
					MitMAttack_Core A_tmp = A;
					A_tmp.updateAsDiff_fromP(p,2,bound_data,bound_time);
					if (A_tmp.isValid())
					{
						waitingA.emplace(move(A_tmp));
						waitingE.emplace(F);
					}
				}
				A.updateAsDiff_fromP(p,0,bound_data,bound_time);
				if (!A.isValid()) break;
			}
		}
	}
}


list<MitMAttack_Core> findMitMAttacks_Diff_fromP_lead(MitMAttack_Core A, const int bound_data, const int bound_time, const int bound_dim)
{
	bool full = false;
	MitMAttack_Core A_copy = A;
	set<SubsetOfPacks> zeros;
	map<pair<unsigned int, unsigned int>, set<SubsetOfPacks>> zeros_map;
	SubsetOfPacks tmp = A.m_B->getStateVars() + A.m_B->getCiphertextVars();
	vector<PackOfVars> vec = tmp.get();
	A.m_B->sortVectorFromPtoV(vec);
	auto const end = vec.end();
	
	for (auto it = vec.begin(); it != end; ++it)
	{
		PackOfVars const & lead = *it;
		cout << "\rPhase Diff: " << lead << " " << flush;
		{
			MitMAttack_Core A_tmp = A;
			A_tmp.updateAsDiff_fromP(lead,2,bound_data,bound_time);
			if (A_tmp.isValid())
			{
				findMitMAttacks_Diff_fromP_lead_tmp_fast(A_tmp,lead,zeros_map,bound_data,bound_time,bound_dim);
				
				for (auto & m : zeros_map)
				{
					for (auto const & p : m.second)	zeros.emplace(p);
					m.second.clear();
				}
			}
		}
		A.updateAsDiff_fromP(lead,3,bound_data,bound_time);
		{
			vector<PackOfVars> vec_tmp = A.t_vars[3].get();
			for (auto const & p : vec_tmp)
			{
				MitMAttack_Core A_tmp = A;
				A_tmp.updateAsDiff_fromP(p,0,bound_data,bound_time);
				if (A.m_B->getEqsKnownKey().getNbSols(A_tmp.t_vars[0]) >= A.m_B->getStatesize()) A.updateAsDiff_fromP(p,1,bound_data,bound_time);
			}
		}
		if (!A.isValid()) break;
	}
	
	list<MitMAttack_Core> L;

	for (auto const & s : zeros)
	{
		MitMAttack_Core A_tmp = A_copy;
		for (auto const & p : vec)
		{
			if (s.includes(p)) A_tmp.updateAsDiff_fromP(p,0,bound_data,bound_time);
			else A_tmp.updateAsDiff_fromP(p,5,bound_data,bound_time);
		}
		if (A_tmp.isValid())
		{
			{
				auto vec_tmp = A_tmp.t_vars[1].get();
				for (auto const & p : vec_tmp) A_tmp.updateAsDiff_fromP(p,1,bound_data,bound_time);
			}
			{
				auto vec_tmp = A_tmp.t_vars[2].get();
				for (auto const & p : vec_tmp) A_tmp.updateAsDiff_fromP(p,2,bound_data,bound_time);
			}
			L.emplace_back(move(A_tmp));
		}
	}
	
	return L;
}

list<MitMAttack_Diff> findMitMAttacks_Diff_fromP(const int bound_data, const int bound_time, const int bound_memory, const int bound_dim, BlockCipher const & B)
{
	MitMAttack_Core A = origin_MitM_Diff_fromP(B,bound_data,bound_time, bound_memory);
	
	list<MitMAttack_Core> L;
	
	if (A.isValid()) L = findMitMAttacks_Diff_fromP_lead(A,bound_data,bound_time,bound_dim);
	
	list<MitMAttack_Diff> L_res;
	unsigned int cpt = 0;
	for (auto const & A : L)
	{
		A.t_vars[1] += B.getPlaintextVars();
		A.t_vars[7] += B.getPlaintextVars();
		L_res.emplace_back(A);
	}
	cout << " -- done (" << L_res.size() << ")" << endl;
	return L_res;
}


void findMitMAttacks_Eqs_fromC_lead_tmp_fast_tmp(MitMAttack_Core & A, map<pair<unsigned int, unsigned int>, list<MitMAttack_Core>> & my_map, const int bound_time, const int nb_eqs, const int bound_eqs)
{
	static unsigned int cpt = 0;
	
	const int new_nb_eqs = A.nbEqs_fromC();
	if (new_nb_eqs > nb_eqs && A.allVarsRequired()) 
	{
		if (new_nb_eqs >= bound_eqs)
		{
			//if ()
			{
				const unsigned int size_in = A.t_vars[1].getVars().size();
				const unsigned int size_out = A.t_vars[2].getVars().size();
				auto it = my_map.begin();
				while ( it != my_map.end() )
				{
					if ((size_in <= it->first.first && size_out < it->first.second) || (size_in < it->first.first && size_out <= it->first.second)) it = my_map.erase(it);
					else ++it;
				}
				my_map[make_pair(size_in,size_out)].emplace_back(move(A));
				cout << "cpt: " << ++cpt << flush;
			}
		}
		else
		{
			{
				auto vec = (A.t_vars[4] + A.t_vars[6]).get();
				A.m_B->sortVectorFromVtoC(vec);
				for (auto const & p : vec)
				{
					MitMAttack_Core A_tmp = A;
					A_tmp.updateAsEqs_fromC(p,2,bound_time);
					if (A_tmp.isValid())
					{
						bool size_ok = true;
						const unsigned int size_in = A_tmp.t_vars[1].getVars().size();
						const unsigned int size_out = A_tmp.t_vars[2].getVars().size();
						for (auto const & m : my_map)
						{
							if ((m.first.first <= size_in && m.first.second < size_out) || (m.first.first < size_in && m.first.second <= size_out))
							{
								size_ok = false;
								break;
							}
						}
						if (size_ok == true) findMitMAttacks_Eqs_fromC_lead_tmp_fast(A_tmp,p,my_map,bound_time,new_nb_eqs,bound_eqs);
					}
					A.updateAsEqs_fromC(p,3,bound_time);
					if (!A.isValid()) break;
				}
			}
			{
				auto vec = A.t_vars[3].get();
				A.m_B->sortVectorFromVtoP(vec);
				for (auto const & p : vec)
				{
					if (!A.isValid()) break;
					MitMAttack_Core A_tmp = A;
					A_tmp.updateAsEqs_fromC(p,1,bound_time);
					if (A_tmp.isValid())
					{
						bool size_ok = true;
						const unsigned int size_in = A_tmp.t_vars[1].getVars().size();
						const unsigned int size_out = A_tmp.t_vars[2].getVars().size();
						for (auto const & m : my_map)
						{
							if ((m.first.first <= size_in && m.first.second < size_out) || (m.first.first < size_in && m.first.second <= size_out))
							{
								size_ok = false;
								break;
							}
						}
						if (size_ok == true) findMitMAttacks_Eqs_fromC_lead_tmp_fast(A_tmp,p,my_map,bound_time,new_nb_eqs,bound_eqs);
					}
					A.updateAsEqs_fromC(p,0,bound_time);
				}
			}
		}
	}
}

#include <queue>

void findMitMAttacks_Eqs_fromC_lead_tmp_fast_init(MitMAttack_Core & A , PackOfVars const & lead, map<pair<unsigned int, unsigned int>, list<MitMAttack_Core>> & my_map,
	const int bound_time, const int nb_eqs, const int bound_eqs, SubSystem const & E, SubsetOfPacks const & sub)
{	
	{
		bool size_ok = true;
		const unsigned int size_in = A.t_vars[1].getVars().size();
		const unsigned int size_out = A.t_vars[2].getVars().size();
		for (auto const & m : my_map)
		{
			if ((m.first.first <= size_in && m.first.second < size_out) || (m.first.first < size_in && m.first.second <= size_out))
			{
				size_ok = false;
				break;
			}
		}
		if (size_ok == false) return;
	}
	
	queue<MitMAttack_Core> waiting;
	
	SubsetOfPacks const non_zero = (A.t_vars[1] + A.t_vars[2] + A.m_B->getCiphertextVars());
	SubsetOfPacks maybe_non_zero = A.t_vars[3] + A.t_vars[4] + A.t_vars[6];
	SubSystem F = E.Extract(A.t_vars[1] + A.t_vars[3] + A.t_vars[6] + A.m_B->getCiphertextVars(), A.t_vars[2] + A.t_vars[4] + A.t_vars[6]);
	F.setKnownVars(non_zero - lead);
	maybe_non_zero &= sub;
	vector<PackOfVars> related = find_related_packs_nonlinear(lead,maybe_non_zero,F);
	
	list<MitMAttack_Core> L;
	L.emplace_back(move(A));
	for (auto const & p : related)
	{
		list<MitMAttack_Core> L_tmp;
		for (auto & A_L : L)
		{
			{
				MitMAttack_Core A_tmp = A_L;
				A_tmp.updateAsEqs_fromC(p,1,bound_time);
				if (A_tmp.isValid())
				{
					bool size_ok = true;
					const unsigned int size_in = A_tmp.t_vars[1].getVars().size();
					const unsigned int size_out = A_tmp.t_vars[2].getVars().size();
					for (auto const & m : my_map)
					{
						if ((m.first.first <= size_in && m.first.second < size_out) || (m.first.first < size_in && m.first.second <= size_out))
						{
							size_ok = false;
							break;
						}
					}
					if (size_ok == true) L_tmp.emplace_back(move(A_tmp));
				}
			}
			
			{
				MitMAttack_Core A_tmp = A_L;
				A_tmp.updateAsEqs_fromC(p,2,bound_time);
				if (A_tmp.isValid())
				{
					bool size_ok = true;
					const unsigned int size_in = A_tmp.t_vars[1].getVars().size();
					const unsigned int size_out = A_tmp.t_vars[2].getVars().size();
					for (auto const & m : my_map)
					{
						if ((m.first.first <= size_in && m.first.second < size_out) || (m.first.first < size_in && m.first.second <= size_out))
						{
							size_ok = false;
							break;
						}
					}
					if (size_ok == true) L_tmp.emplace_back(move(A_tmp));
				}
			}
			
			{
				A_L.updateAsEqs_fromC(p,0,bound_time);
				if (A_L.isValid()) waiting.emplace(move(A_L));
			}
		}
		swap(L,L_tmp);
	}
	for (auto & A_L : L) findMitMAttacks_Eqs_fromC_lead_tmp_fast_tmp(A_L,my_map,bound_time,nb_eqs,bound_eqs);
	while (!waiting.empty())
	{
		findMitMAttacks_Eqs_fromC_lead_tmp_fast_init(waiting.front(),lead,my_map,bound_time,nb_eqs,bound_eqs,F,sub);
		waiting.pop();
	}
}



void findMitMAttacks_Eqs_fromC_lead_tmp_fast(MitMAttack_Core & A , PackOfVars const & lead, map<pair<unsigned int, unsigned int>, list<MitMAttack_Core>> & my_map,
	const int bound_time, const int nb_eqs, const int bound_eqs)
{
	SubsetOfPacks const non_zero = (A.t_vars[1] + A.t_vars[2] + A.m_B->getCiphertextVars());
	SubsetOfPacks maybe_non_zero = A.t_vars[3] + A.t_vars[4] + A.t_vars[6];
	SubSystem E = A.m_B->getEqsKnownKey().Extract(maybe_non_zero + non_zero);
	E.setKnownVars(non_zero - lead);
	SubSystem F = E.extractRelated(lead);
	auto sub = maybe_non_zero.empty_set();
	auto vec = maybe_non_zero.get();
	for (auto const & p : vec)
	{
		if (F.involves(p)) sub += p;
	}
	findMitMAttacks_Eqs_fromC_lead_tmp_fast_init(A,lead,my_map,bound_time,nb_eqs,bound_eqs,F,sub);
}




list<MitMAttack_Core> findMitMAttacks_Eqs_fromC_lead(MitMAttack_Core A, const int bound_time, const int bound_eqs)
{
	bool full = false;
	list<MitMAttack_Core> L;
	map<pair<unsigned int, unsigned int>, list<MitMAttack_Core>> L_map;
	SubsetOfPacks tmp = A.m_B->getStateVars() + A.m_B->getPlaintextVars();
	vector<PackOfVars> vec = tmp.get();
	A.m_B->sortVectorFromVtoC(vec);	
	const auto end = vec.end();
	for (auto it = vec.begin(); it != end; ++it)
	{
		PackOfVars const & lead = *it;
		if (A.m_B->varsFromCtoV(lead).getVars().size() == 1 && A.m_B->varsFromPtoV(lead).getVars().size() == 1) continue;
		cout << "\rPhase Eqs: " << lead << " " << flush;
		cout << endl;
		MitMAttack_Core A_tmp = A;
		A_tmp.updateAsEqs_fromC(lead,2,bound_time);
		if (A_tmp.isValid())
		{
			findMitMAttacks_Eqs_fromC_lead_tmp_fast(A_tmp,lead,L_map,bound_time,0,bound_eqs);
			for (auto & m : L_map)
			{
				for (auto & l : m.second) L.emplace_back(move(l));
				m.second.clear();
			}
		}
		A.updateAsEqs_fromC(lead,3,bound_time);
		{
			vector<PackOfVars> vec_tmp = A.t_vars[3].get();
			for (auto const & p : vec_tmp)
			{
				MitMAttack_Core A_tmp = A;
				A_tmp.updateAsEqs_fromC(p,0,bound_time);
				if (!A_tmp.isValid()) A.updateAsEqs_fromC(p,1,bound_time);
			}
		}
		if (!A.isValid()) break;
	}
	return L;
}

list<MitMAttack_Eqs> findMitMAttacks_Eqs_fromC(const int bound_data, const int bound_time, const int bound_memory, const int bound_eqs, BlockCipher const & B)
{
	MitMAttack_Core A = origin_MitM_Eqs_fromC(B,bound_data,bound_time,bound_memory);
	
	
	list<MitMAttack_Core> L;
	
	if (A.isValid()) L = findMitMAttacks_Eqs_fromC_lead(A,bound_time,bound_eqs);
	
	list<MitMAttack_Eqs> L_res;
	for (auto const & A : L)
	{
		A.t_vars[1] += B.getCiphertextVars();
		A.t_vars[0] += A.t_vars[3] + A.t_vars[4] + A.t_vars[6];
		A.t_vars[3].clear();
		A.t_vars[4].clear();
		A.t_vars[6].clear();
		A.t_vars[7] += B.getCiphertextVars();
		L_res.emplace_back(A);
	}
	
	cout << " -- done (" << L_res.size() << ")" << endl;
	
	return L_res;
}

list<MitMAttack_Diff> findMitMAttacks_Diff_fromC(const int bound_data, const int bound_time, const int bound_memory, const int bound_dim, BlockCipher const & B)
{
	BlockCipher B_reverse = B.reverse();
	list<MitMAttack_Diff> L = findMitMAttacks_Diff_fromP(bound_data,bound_time,bound_memory,bound_dim,B_reverse);
	for (auto & A : L) A.setBlockCipher(B);
	return L;
}

list<MitMAttack_Eqs> findMitMAttacks_Eqs_fromP(const int bound_data, const int bound_time, const int bound_memory, const int bound_eqs, BlockCipher const & B)
{
	BlockCipher B_reverse = B.reverse();
	list<MitMAttack_Eqs> L = findMitMAttacks_Eqs_fromC(bound_data,bound_time,bound_memory,bound_eqs,B_reverse);
	for (auto & A : L) A.setBlockCipher(B);
	return L;
}


MitMAttack_Core origin_MitM_Diff_fromP_tmp(BlockCipher const & B, const int bound_data, const int bound_time)
{	
	MitMAttack_Core A(B);
	
	A.t_vars[4] = B.getCiphertextVars();
	
	{
		auto known = B.getPlaintextVars().empty_set();
		auto vec = B.getStateVars().get();
		SubSystem const & E = B.getEqsKnownKey();
		int sol = 0;
		auto it = vec.begin();
		while (it != vec.end())
		{
			if (B.varsFromPtoV(*it).size() == 1 && B.varsFromCtoV(*it).size() == 1)
			{
				auto tmp = known + *it;
				auto sol_tmp = E.getNbSols(tmp);
				if (sol_tmp <= sol)
				{
					sol = sol_tmp;
					known = move(tmp);
				}
			}
			++it;
		}
		if (((A.t_vars[1] + A.t_vars[2] + A.t_vars[5]) & known).is_empty())
		{
			A.t_vars[0] += known;
			A.t_vars[3] -= known;
			A.t_vars[4] -= known;
			A.t_vars[6] -= known;
		}
		else
		{
			cout << "something strange with the system of equations ...";
			exit(EXIT_FAILURE);
		}
	}
	
	{
		auto known = B.getPlaintextVars();
		auto vec = (B.getStateVars() - A.t_vars[0]).get();
		B.sortVectorFromPtoV(vec);
		SubSystem const & E = B.getEqsNonLinKey();
		auto it = vec.begin();
		while (it != vec.end())
		{
			auto tmp = known + *it;
			if (E.getNbSols(tmp) == B.getStatesize()) known = move(tmp);
			++it;
		}
		known -= B.getPlaintextVars();
		A.t_vars[3] += (A.t_vars[6] & known);
		A.t_vars[1] += (A.t_vars[5] & known);
		A.t_vars[0] += (A.t_vars[4] & known);
		A.t_vars[4] -= known;
		A.t_vars[5] -= known;
		A.t_vars[6] -= known;
	}
	
	{
		unsigned int cpt = 0;
		vector<PackOfVars> my_vec = A.t_vars[6].get();
		B.sortVectorFromVtoP(my_vec);
		for (auto const & p : my_vec)
		{
			if (bound_time > 1)
			{
				MitMAttack_Core A_tmp = A;
				A_tmp.updateAsDiff_fromP(p,1,bound_data,bound_time);
				if (!A_tmp.isValid()) A.updateAsDiff_fromP(p,4,bound_data,bound_time);
			}
			else A.updateAsDiff_fromP(p,4,bound_data,bound_time);
		}
		SubSystem const & E = B.getEqsKnownKey();
		my_vec = A.t_vars[4].get();
		for (auto const & p : my_vec)
		{
			MitMAttack_Core A_tmp = A;
			A_tmp.updateAsDiff_fromP(p,0,bound_data,bound_time);
			if (!A_tmp.isValid() || E.getNbSols(A_tmp.t_vars[0]) >= B.getStatesize()) A.updateAsDiff_fromP(p,2,bound_data,bound_time);
		}
	}

	return A;
}

MitMAttack_Core origin_MitM_Eqs_fromC_tmp(BlockCipher const & B, const int bound_time)
{
	MitMAttack_Core A(B);
	A.t_vars[2] = B.getPlaintextVars();
	
	{
		auto known = B.getCiphertextVars();
		auto vec = B.getStateVars().get();
		B.sortVectorFromCtoV(vec);
		SubSystem const & E = B.getEqs();
		auto it = vec.begin();
		while (it != vec.end())
		{
			auto tmp = known + *it;
			if (E.getNbSols(tmp) == B.getStatesize()) known = move(tmp);
			++it;
		}
		known -= B.getCiphertextVars();
		A.t_vars[3] += (A.t_vars[6] & known);
		A.t_vars[1] += (A.t_vars[5] & known);
		A.t_vars[0] += (A.t_vars[4] & known);
		A.t_vars[4] -= known;
		A.t_vars[5] -= known;
		A.t_vars[6] -= known;
	}
	
	{
		vector<PackOfVars> my_vec = A.t_vars[6].get();
		B.sortVectorFromCtoV(my_vec);
		for (auto const & p : my_vec)
		{
			if (bound_time > 1)
			{
				MitMAttack_Core A_tmp = A;
				A_tmp.updateAsEqs_fromC(p,1,bound_time);
				if (!A_tmp.isValid()) A.updateAsEqs_fromC(p,4,bound_time);
			}
			else A.updateAsEqs_fromC(p,4,bound_time);
		}
		my_vec = A.t_vars[4].get();
		for (auto const & p : my_vec)
		{
			MitMAttack_Core A_tmp = A;
			A_tmp.updateAsEqs_fromC(p,0,bound_time);
			if (!A_tmp.isValid() || A_tmp.nbEqsMax() <= 0) A.updateAsEqs_fromC(p,2,bound_time);
		}
	}

	return A;
}

MitMAttack_Core origin_MitM_Diff_fromP(BlockCipher const & B, const int bound_data, const int bound_time, const int bound_memory)
{
	MitMAttack_Core A = origin_MitM_Diff_fromP_tmp(B,bound_data,bound_time);
	
	return A;
}

MitMAttack_Core origin_MitM_Eqs_fromC(BlockCipher const & B, const int bound_data, const int bound_time, const int bound_memory)
{
	MitMAttack_Core A_end = origin_MitM_Eqs_fromC_tmp(B,bound_time);
	
	return A_end;
	
}

MitMAttack_Core origin_SimpleMitM_Eqs_fromC(BlockCipher const & B, const int bound_time)
{
	BlockCipher Br = B.reverse();
	MitMAttack_Core A = origin_MitM_Eqs_fromC_tmp(Br,bound_time);
	MitMAttack_Core A_end = origin_MitM_Eqs_fromC_tmp(B,bound_time);
	
	vector<PackOfVars> my_vec = (A_end.t_vars[0] + A_end.t_vars[2] + A_end.t_vars[4]).get();
	for (auto const & p : my_vec)
	{
		A.updateAsEqs_fromC(p,3,bound_time);
		if (!A.isValid()) break;
	}
	
	my_vec = (A.t_vars[0] + A.t_vars[2] + A.t_vars[4]).get();
	for (auto const & p : my_vec)
	{
		A_end.updateAsEqs_fromC(p,3,bound_time);
		if (!A_end.isValid()) break;
	}
	
	return A_end;	
}

list<MitMAttack_Eqs> findSimpleMitMAttacks_Eqs_fromC(const int bound_time, const int bound_eqs, BlockCipher const & B)
{
	MitMAttack_Core A = origin_SimpleMitM_Eqs_fromC(B,bound_time);
	
	list<MitMAttack_Core> L;
	
	if (A.isValid()) L = findMitMAttacks_Eqs_fromC_lead(A,bound_time,bound_eqs);
	
	list<MitMAttack_Eqs> L_res;
	for (auto const & A : L)
	{
		A.t_vars[1] += B.getCiphertextVars();
		A.t_vars[0] += A.t_vars[3] + A.t_vars[4] + A.t_vars[6];
		A.t_vars[3].clear();
		A.t_vars[4].clear();
		A.t_vars[6].clear();
		A.t_vars[7] += B.getCiphertextVars();
		L_res.emplace_back(A);
	}
	cout << " - done (" << L_res.size() << ")" << endl;
	return L_res;
}

