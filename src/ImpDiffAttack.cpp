/*
 * ImpDiffAttack.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include "ImpDiffAttack.h"
#include "BlockCipher.h"
#include "MitMAttack_diff.h"

using namespace std;

SubsetOfPacks getRealOnlineP(MitMAttack_Diff const & A)
{
	BlockCipher const & B = A.getBlockCipher();
	SubsetOfPacks res = A.guessOn();
	auto my_vec = res.get();
	for (auto const & p : my_vec) res += B.varsFromPtoV(p);
	return res;
}

SubsetOfPacks getRealOnlineC(MitMAttack_Diff const & A)
{
	BlockCipher const & B = A.getBlockCipher();
	SubsetOfPacks res = A.guessOn();
	auto my_vec = res.get();
	for (auto const & p : my_vec) res += B.varsFromCtoV(p);
	return res;
}

ImpDiffAttack::ImpDiffAttack(ImpDiffAttack const & A) : m_online(A.m_online), m_null(A.m_null), m_time(A.m_time), m_timekey(A.m_timekey), 
m_data_in(A.m_data_in), m_data_out(A.m_data_out), m_cin(A.m_cin), m_cout(A.m_cout),
m_B(A.m_B), m_guess(A.m_guess)
{
}


ImpDiffAttack & ImpDiffAttack::operator=(ImpDiffAttack const & A)
{
	m_online = A.m_online;
	m_null = A.m_null;
	m_time = A.m_time;
	m_timekey = A.m_timekey;
	m_cin = A.m_cin;
	m_cout = A.m_cout;
	m_data_in = A.m_data_in;
	m_data_out = A.m_data_out;
	m_B = A.m_B;
	m_guess = A.m_guess;
	return *this;
}

ImpDiffAttack::~ImpDiffAttack()
{
}
		
ImpDiffAttack::ImpDiffAttack(ImpDiffAttack && A) : m_online(move(A.m_online)), m_null(move(A.m_null)), m_time(A.m_time), m_timekey(A.m_timekey), 
m_data_in(A.m_data_in), m_data_out(A.m_data_out), m_cin(A.m_cin), m_cout(A.m_cout), m_B(A.m_B), m_guess(move(A.m_guess))
{
}

ImpDiffAttack & ImpDiffAttack::operator=(ImpDiffAttack && A)
{
	m_online = move(A.m_online);
	m_null = move(A.m_null);
	m_time = A.m_time;
	m_timekey = A.m_timekey;
	m_cin = A.m_cin;
	m_cout = A.m_cout;
	m_data_in = A.m_data_in;
	m_data_out = A.m_data_out;
	m_B = A.m_B;
	m_guess = move(A.m_guess);
	return *this;
}
		
ImpDiffAttack::ImpDiffAttack(MitMAttack_Diff const & A_P, MitMAttack_Diff const & A_C)
{
	m_B = addressof(A_P.getBlockCipher());
	
	assert( m_B == addressof(A_C.getBlockCipher()));
	
	m_data_in = A_P.data();
	m_data_out = A_C.data();
	m_null = A_P.null() + A_C.null();
	m_online = A_P.activeOn() + A_C.activeOn();
	
	m_cin = A_P.data() + A_P.dimDiff() - m_B->getStatesize();
	m_cout = A_C.data() + A_C.dimDiff() - m_B->getStatesize();
	
	SubsetOfPacks real_online = getRealOnlineP(A_P) + getRealOnlineC(A_C);
	
	m_guess = A_P.guessOn() + A_C.guessOn();
			
	m_time = m_B->getEqsNonLinKey().getNbSols(m_guess, real_online + m_B->getNonLinKeyVars()) - 2*m_B->getStatesize();
	m_timekey = m_B->getEqsNonLinKey().getNbSols(real_online, m_B->getNonLinKeyVars()) - 2*m_B->getStatesize();
}

ImpDiffAttack::ImpDiffAttack(MitMAttack_Diff const & A_P, MitMAttack_Diff const & A_C, const int time_key)
{
	m_B = addressof(A_P.getBlockCipher());
	
	assert( m_B == addressof(A_C.getBlockCipher()));
	
	m_data_in = A_P.data();
	m_data_out = A_C.data();
	m_null = A_P.null() + A_C.null();
	m_online = A_P.activeOn() + A_C.activeOn();
	
	m_cin = A_P.data() + A_P.dimDiff() - m_B->getStatesize();
	m_cout = A_C.data() + A_C.dimDiff() - m_B->getStatesize();
	
	SubsetOfPacks real_online = getRealOnlineP(A_P) + getRealOnlineC(A_C);
	
	m_guess = A_P.guessOn() + A_C.guessOn();
			
	m_time = m_B->getEqsNonLinKey().getNbSols(m_guess, real_online + m_B->getNonLinKeyVars()) - 2*m_B->getStatesize();
	m_timekey = time_key;
	
	if (m_time > m_timekey)
	{
		std::cout << "very strange: " << m_time << " > " << m_timekey << endl;
		std::cout << m_B->getEqsNonLinKey().getNbSols(real_online, m_B->getNonLinKeyVars()) - 2*m_B->getStatesize();
		if (real_online.includes(m_online)) std::cout << "issue in getNbSols ????" << endl;
		else std::cout << "issue in getRealOnline ????" << endl;
		if (m_B->getEqsNonLinKey().nb_sols_smaller_than(2*m_B->getStatesize() + m_timekey + 1, m_online, (real_online + m_B->getNonLinKeyVars()) - m_online))
		{
			std::cout << "same issue in ...smaller than..." << endl;
		}
		getchar();
	}
	
}
		

		
int validCombinationImp2(MitMAttack_Diff const & A_P, MitMAttack_Diff const & A_C)
{
	return 0;
	SubsetOfPacks null = A_P.null() + A_C.null();
	SubsetOfPacks non_null = A_P.activeOff() & A_C.activeOff();
	
	SubsetOfPacks all = null + non_null;
	BlockCipher const & B = A_P.getBlockCipher();
	SubSystem E = B.getEqsKnownKey().Extract(all);
	if (E.getNbSols(all) < B.getStatesize()) return 2;
	E.setKnownVars(null);
	null.clear();
	int s = 0;
	auto vec = non_null.get();
	auto it = vec.begin();
	while (it != vec.end())
	{
		auto & p = *it;
		SubsetOfPacks null_tmp = null + p;
		const int s_tmp = E.getNbSols(null_tmp);
		if (s_tmp <= s)
		{
			s = s_tmp;
			null = move(null_tmp);
			p = move(*(vec.rbegin()));
			vec.pop_back();
			it = vec.begin();
		}
		else ++it;
	}
	if (vec.empty()) return 1;
	else return 0;
}

int validCombinationImp(MitMAttack_Diff const & A_P, MitMAttack_Diff const & A_C)
{
	SubsetOfPacks null = A_P.null() + A_C.null();
	SubsetOfPacks non_null = A_P.activeOff() & A_C.activeOff();
	
	SubsetOfPacks all = null + non_null;
	BlockCipher const & B = A_P.getBlockCipher();
	SubSystem E = B.getEqsKnownKey().Extract(all);
	if (E.getNbSols(all) < B.getStatesize()) return 2;
	E.setKnownVars(null);
	/*auto vec = non_null.get();
	auto it = vec.begin();
	while (it != vec.end())
	{
		auto & p = *it;
		if (E.propagateIfKnown(p))
		{
			p = move(*(vec.rbegin()));
			vec.pop_back();
			it = vec.begin();
		}
		else ++it;
	}
	if (vec.empty()) return 1;
	else return 0;*/
	if (E.testAllKnown(non_null)) return 1;
	else return 0;
}


void testTMP(ImpDiffAttack A)
{
	SubsetOfPacks null = A.null();
	SubsetOfPacks non_null = A.getBlockCipher().getStateVars() - null;
	
	SubSystem E = A.getBlockCipher().getEqsKnownKey().Extract(null + non_null);
	E.setKnownVars(null);
	null.clear();
	int s = 0;
	auto vec = non_null.get();
	SubsetOfPacks lolol = null;
start:
	{
		cout << "  -- ";
		bool redo = false;
		auto it = vec.begin();
		while (it != vec.end())
		{
			auto & p = *it;
			SubsetOfPacks null_tmp = null + p;
			const int s_tmp = E.getNbSols(null_tmp);
			if (s_tmp <= s)
			{
				cout << p << " ";
				s = s_tmp;
				lolol += p;
				p = move(vec.back());
				vec.pop_back();
				redo = true;
			}
			else ++it;
		}
		if (redo == true)
		{
			null = lolol;
			cout << endl;
			goto start;
		}
	}
}

bool workForSure(ImpDiffAttack A)
{
	SubsetOfPacks null = A.null();
	SubsetOfPacks non_null = A.getBlockCipher().getStateVars() - null;
	
	SubSystem E = A.getBlockCipher().getEqsKnownKey().Extract(null + non_null);
	unsigned int cpt = 0;
	int s = E.getNbSols(null);
	auto vec = non_null.get();
	{
		auto it = vec.begin();
		while (it != vec.end())
		{
			auto & p = *it;
			const int s_tmp = E.getNbSolsNotBoth(null.getVars(),p);
			if (s_tmp <= s)
			{
				null += p;
				s = E.getNbSols(null);
				p = move(vec.back());
				vec.pop_back();
				it = vec.begin();
			}
			else ++it;
		}
	}
	return (vec.empty());
}

bool betterImp(ImpDiffAttack const & A1, ImpDiffAttack const & A2) //true iff A1 better than A2
{
	if (A2.dataIn() < A1.dataIn()) return false;
	if (2*A2.dataIn() + A2.dataOut() > 2*A1.dataIn() + A1.dataOut()) return false;
	if (A2.cin() + A2.cout() < A1.cin() + A1.cout()) return false;
	if (A2.timeKey() < A1.timeKey()) return false;
	return true;
}

void findImpDiffAttack(const int bound_data, const int bound_time, BlockCipher const & B, const int bound_dim_in, const int bound_dim_out)
{
	list<MitMAttack_Diff> L_P = findMitMAttacks_Diff_fromP(bound_data,bound_time,B.getKeysize() + B.getStatesize()+1,bound_dim_in,B);
	list<MitMAttack_Diff> L_C = findMitMAttacks_Diff_fromC(B.getStatesize() + 1,bound_time,B.getKeysize() + B.getStatesize()+1,bound_dim_out,B);
	
	map<MitMAttack_Diff const *, SubsetOfPacks> real_online;
	for (auto const & A : L_P) real_online[addressof(A)] = getRealOnlineP(A) + B.getPlaintextVars();
	for (auto const & A : L_C) real_online[addressof(A)] = getRealOnlineC(A) + B.getCiphertextVars();
	
	unsigned int cpt = 0;
	list<ImpDiffAttack> L_final;
	for (auto & A_P : L_P)
	{
		cout << "\r" << "Merging: " << ++cpt << "/" << L_P.size() << flush;	
		//if (cpt != 77) continue;
		SubsetOfPacks const & real_onlineP = real_online[addressof(A_P)];
		for (auto & A_C : L_C)
		{
			const int t_all = B.getEqsNonLinKey().getNbSols(real_onlineP + real_online[addressof(A_C)], B.getNonLinKeyVars()) - 2*B.getStatesize();
			if (t_all < bound_time && validCombinationImp(A_P,A_C) == 1) L_final.emplace_back(A_P,A_C,t_all);
		}
	}
	
	list<ImpDiffAttack> L_best;
	for (auto const & A : L_final)
	{
		if (!workForSure(A)) continue;
		bool best = true;
		auto it = L_best.begin();
		while (it != L_best.end())
		{
			if (betterImp(*it,A))
			{
				best = false;
				break;
			}
			if (betterImp(A,*it)) it = L_best.erase(it);
			else ++it;
		}
		if (best) L_best.emplace_back(A);
	}
	
	list<ImpDiffAttack> L_best_may_not_work;
	for (auto const & A : L_final)
	{
		bool best = true;
		{
			auto it = L_best_may_not_work.begin();
			while (it != L_best_may_not_work.end())
			{
				if (betterImp(*it,A))
				{
					best = false;
					break;
				}
				if (betterImp(A,*it)) it = L_best_may_not_work.erase(it);
				else ++it;
			}
		}
		if (best)
		{
			auto it = L_best.begin();
			while (it != L_best.end())
			{
				if (betterImp(*it,A))
				{
					best = false;
					break;
				}
				++it;
			}
			if (best) L_best_may_not_work.emplace_back(A);
		}
	}
	
	cout << endl << endl;
	for (auto const & A : L_best)
	{		
		cout << "Best Attack: " << endl;
		cout << " - time (state): " << A.time() << endl;
		cout << " - time (key): " << A.timeKey() << endl;
		cout << " - data (in): " << A.dataIn() << endl;
		cout << " - data (out): " << A.dataOut() << endl;
		cout << " - cin + cout: " << A.cin() << " + " << A.cout() << endl;
		cout << " - online (" << (A.guess().size() - 2*B.getStatesize()) << "/" << B.getStateVars().size() << "): " << A.online() << endl;
		cout << " - null: " << A.null() << endl;
		cout << " - order: " << endl;
		testTMP(A);
		cout << endl;
		cout << endl;
	}
	
	for (auto const & A : L_best_may_not_work)
	{		
		cout << "Best Attack (may not work): " << endl;
		cout << " - time (state): " << A.time() << endl;
		cout << " - time (key): " << A.timeKey() << endl;
		cout << " - data (in): " << A.dataIn() << endl;
		cout << " - data (out): " << A.dataOut() << endl;
		cout << " - cin + cout: " << A.cin() << " + " << A.cout() << endl;
		cout << " - online (" << (A.guess().size() - 2*B.getStatesize()) << "/" << B.getStateVars().size() << "): " << A.online() << endl;
		cout << " - null: " << A.null() << endl;
		cout << " - order: " << endl;
		testTMP(A);
		cout << endl;
		cout << endl;
	}
}
