/*
 * SofExpression.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include "Sbox.h"
#include "Expression.h"
#include "SofExpression.h"

using namespace std;

SofExpression::SofExpression(Sbox const& sb, const initializer_list<Expression const> Exprs) : m_sb(sb)
{
	const unsigned int nb_var = sb.getNbVariables();
	if (nb_var != Exprs.size())
	{
		std::cout << "Error: the Sbox " << sb << " takes exactly " << nb_var << " inputs" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_exprs = new Expression [nb_var];
	unsigned int pos = 0;
	for (initializer_list<Expression const>::const_iterator it=Exprs.begin(); it!=Exprs.end(); ++it) m_exprs[pos++] = *it;
}

SofExpression::SofExpression(Sbox const& sb, vector<Expression> const& Exprs) : m_sb(sb)
{
	const unsigned int nb_var = sb.getNbVariables();
	if (nb_var != Exprs.size())
	{
		std::cout << "Error: the Sbox " << sb << " takes exactly " << nb_var << " inputs" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_exprs = new Expression [nb_var];
	unsigned int pos = 0;
	for (vector<Expression>::const_iterator it=Exprs.cbegin(); it!=Exprs.cend(); ++it) m_exprs[pos++] = *it;
}


SofExpression::SofExpression(SofExpression const& expr) : m_sb(expr.m_sb)
{
	const unsigned int nb_var = m_sb.getNbVariables();
	m_exprs = new Expression [nb_var];
	for (unsigned int i = 0; i < nb_var; ++i) m_exprs[i] = expr.m_exprs[i];
}

SofExpression::~SofExpression()
{
	delete[] m_exprs;
}

SofExpression& SofExpression::operator=(SofExpression const& expr)
{
	if (this != &expr)
	{
		const unsigned int old_nb_var = getNbExpressions();
		const unsigned int new_nb_var = expr.getNbExpressions();
		m_sb = expr.m_sb;
		if (old_nb_var != new_nb_var)
		{
			delete[] m_exprs;
			m_exprs = new Expression [new_nb_var];
		}
		for (unsigned int i = 0; i < new_nb_var; ++i) m_exprs[i] = expr.m_exprs[i];
	}
	return *this;
}

bool operator<(SofExpression const& expr1, SofExpression const& expr2)
{
	if (expr1.m_sb == expr2.m_sb)
	{
		const unsigned int nb_var = expr1.getNbExpressions();
		unsigned int i = 0;
		while ((i < nb_var - 1) && (expr1.m_exprs[i] == expr2.m_exprs[i])) ++i;
		return (expr1.m_exprs[i] < expr2.m_exprs[i]);
	}
	else return (expr1.m_sb < expr2.m_sb);
}

bool operator!=(SofExpression const& expr1, SofExpression const& expr2)
{
	if (expr1.m_sb != expr2.m_sb) return true;
	const unsigned int nb_var = expr1.getNbExpressions();
	if (nb_var != expr2.getNbExpressions()) return true;
	for (unsigned int i = 0; i < nb_var; ++i)
	{
		if (expr1.m_exprs[i] != expr2.m_exprs[i]) return true;
	}
	return false;
}

void SofExpression::print(ostream &flux) const
{
	flux << m_sb;
	const unsigned nb_var = getNbExpressions();
	flux << '(';
	for (unsigned int i = 0; i < nb_var; ++i)
	{
		if (i != 0) flux << ", ";
		flux << m_exprs[i];
	}
	flux << ')';
}
		
		
ostream& operator<<(ostream &flux, SofExpression const& expr)
{
	expr.print(flux);
	return flux;
}

bool SofExpression::isMonomial() const
{
	for (const_iterator cit = cbegin(); cit != cend(); ++cit)
	{
		if (!cit->isVariable()) return false;
	}
	return true;
}


bool SofExpression::replace(const Variable old, const Variable x)
{
	bool replaced = false;
	for (auto it = this->begin(); it != this->end(); ++it) replaced |= it->replace(old,x);
	return replaced;
}

set<Variable> SofExpression::getVariables() const
{
	set<Variable> s;
	getVariables(s);
	return s;
}

void SofExpression::getVariables(set<Variable> & s) const
{
	for (auto it = this->begin(); it != this->end() ; ++it) it->getVariables(s);
}

