/*
 * MitMAttack_diff.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef DEF_MITM_ATTACK_DIFF
#define DEF_MITM_ATTACK_DIFF

#include <set>
#include <map>
#include <list>

class MitMAttack_Core;
class BlockCipher;

#include "PackOfVars.h"

class MitMAttack_Diff
{
	public:
		MitMAttack_Diff(MitMAttack_Diff const &);
		MitMAttack_Diff(MitMAttack_Diff &&);
		
		MitMAttack_Diff & operator=(MitMAttack_Diff const &);
		MitMAttack_Diff & operator=(MitMAttack_Diff &&);
		
		~MitMAttack_Diff();
		
		MitMAttack_Diff(MitMAttack_Core const &);
		
		int data() const {return m_data;};
		int dimDiff() const {return m_dim_diff;};
		int timeOnline() const;
		SubsetOfPacks const & activeOn() const {return *m_active_on;};
		SubsetOfPacks const & activeOff() const {return *m_active_off;};
		SubsetOfPacks const & activeBoth() const {return *m_both;};
		SubsetOfPacks const & null() const {return m_null;};
		SubsetOfPacks const & guessOn() const {return *m_guess_on;};
		SubsetOfPacks const & guessOff() const {return *m_guess_off;};
		
		BlockCipher const & getBlockCipher() const {return *m_B;};
		
		friend bool operator==(MitMAttack_Diff const &, MitMAttack_Diff const &);
		friend bool operator!=(MitMAttack_Diff const & A1, MitMAttack_Diff const & A2) {return !(A1 == A2);};
		friend bool operator<(MitMAttack_Diff const &, MitMAttack_Diff const &); //to use map, set, ...
		
	private:
		static std::set<SubsetOfPacks> st_all_both;
		static std::set<SubsetOfPacks> st_all_online;
		static std::set<SubsetOfPacks> st_all_offline;
		static std::map<SubsetOfPacks const *,int> st_all_time;
		
		SubsetOfPacks const * m_active_on;
		SubsetOfPacks const * m_active_off;
		SubsetOfPacks const * m_guess_on;
		SubsetOfPacks const * m_guess_off;
		SubsetOfPacks m_null;
		SubsetOfPacks const * m_both;
		int m_dim_diff;
		int m_data;
		BlockCipher const * m_B;
		
		void updateData();
		void setBlockCipher(BlockCipher const &);
		
		friend std::list<MitMAttack_Diff> findMitMAttacks_Diff_fromC(int, int, int, int, BlockCipher const &);
};


bool operator==(MitMAttack_Diff const &, MitMAttack_Diff const &);
bool operator<(MitMAttack_Diff const &, MitMAttack_Diff const &);

std::list<MitMAttack_Diff> findMitMAttacks_Diff_fromP(int, int, int, int, BlockCipher const &);
std::list<MitMAttack_Diff> findMitMAttacks_Diff_fromC(int, int, int, int, BlockCipher const &);

#endif
