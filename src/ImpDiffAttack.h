/*
 * ImpDiffAttack.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */



#ifndef DEF_IMP_DIFF_ATTACK
#define DEF_IMP_DIFF_ATTACK

class BlockCipher;
class SubsetOfPacks;
class MitMAttack_Diff;

#include "PackOfVars.h"

class ImpDiffAttack
{
	public:
		ImpDiffAttack(ImpDiffAttack const &);
		ImpDiffAttack & operator=(ImpDiffAttack const &);
		~ImpDiffAttack();
		
		ImpDiffAttack(ImpDiffAttack &&);
		ImpDiffAttack & operator=(ImpDiffAttack &&);
		
		ImpDiffAttack(MitMAttack_Diff const &, MitMAttack_Diff const &);
		ImpDiffAttack(MitMAttack_Diff const &, MitMAttack_Diff const &, int);
		
		int dataIn() const {return m_data_in;};
		int dataOut() const {return m_data_out;};
		int time() const {return m_time;};
		int timeKey() const {return m_timekey;};
		int cin() const {return m_cin;};
		int cout() const {return m_cout;};
		SubsetOfPacks const & online() const {return m_online;};
		SubsetOfPacks const & null() const {return m_null;};
		SubsetOfPacks const & guess() const {return m_guess;};
		
		friend int validCombinationImp(MitMAttack_Diff const &, MitMAttack_Diff const &);
		
		BlockCipher const & getBlockCipher() const {return *m_B;};
	
	private:
		SubsetOfPacks m_online;
		SubsetOfPacks m_null;
		int m_time;
		int m_timekey;
		int m_data_in;
		int m_data_out;
		int m_cin;
		int m_cout;
		BlockCipher const * m_B;
		SubsetOfPacks m_guess;
};

void findImpDiffAttack(int, int, BlockCipher const &, int, int);

#endif
