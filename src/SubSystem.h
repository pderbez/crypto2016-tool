/*
 * SubSystem.h
 * 
 * Copyright 2014 pderbez <pderbez@pderbez-VirtualBox>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_SUBSYSTEM
#define DEF_SUBSYSTEM

class SystemOfEquations;
class Expression;

#include <vector>
#include <list>

#include "GFElement.h"
#include "Matrix.h"
#include "Variable.h"
#include "SetOfVars.h"
#include "PackOfVars.h"

class SubSystem
{
	public:
		SubSystem();
		SubSystem(SubSystem const&);
		SubSystem(SubSystem &&);		
		
		SubSystem& operator=(SubSystem const&);
		SubSystem& operator=(SubSystem &&);
		
		SubSystem Extract(SubsetOfVars const &) const;
		SubSystem Extract(SubsetOfPacks const & s) const {return this->Extract(s.getVars());};
		SubSystem Extract(SubsetOfVars const &, SubsetOfVars const &) const;
		SubSystem Extract(SubsetOfPacks const & s1, SubsetOfPacks const & s2) const {return this->Extract(s1.getVars(),s2.getVars());};
		
		template <typename Iter> SubSystem Extract(Iter begin, Iter end) const;
		 		
		template <typename Iter> void setKnownVars(Iter begin, const Iter end);
		void setKnownVars(Variable const&);
		void setKnownVars(SubsetOfVars const&);
		void setKnownVars(SubsetOfPacks const & s) {return this->setKnownVars(s.getVars());};
		
		int getNbSols(Variable const &) const;
		int getNbSols(SubsetOfPacks const & s) const {return this->getNbSols(s.getVars());};
		int getNbSols(SubsetOfVars const &) const;
		template <typename Iter> int getNbSols(Iter begin, const Iter end) const;
		
		int getNbSolsNotBoth(SubsetOfVars const &, SubsetOfVars const &) const;
		int getNbSolsNotBoth(SubsetOfPacks const & s1, SubsetOfPacks const & s2) const {return this->getNbSolsNotBoth(s1.getVars(),s2.getVars());};
		
		void setSetOfVars(SetOfVars const &);
		
		std::vector<SubsetOfVars> getSubsetsOfVars() const;
		
		bool isLinear(Variable const &) const;
		bool isLinear(SubsetOfVars const &) const;
		
		~SubSystem();
		
		//bool nb_sols_smaller_than(const int bound_sols, SubsetOfVars wanted, SubsetOfVars maybe) const {return this->private_nb_sols_smaller_than(bound_sols,wanted,maybe);};
		bool nb_sols_smaller_than(const int bound_sols, SubsetOfPacks wanted, SubsetOfPacks maybe) const {return this->private_nb_sols_smaller_than(bound_sols,wanted,maybe);};
		
		//int getNbSols(SubsetOfVars wanted, SubsetOfVars maybe) const {return this->private_nb_sols(wanted,maybe);};
		int getNbSols(SubsetOfPacks wanted, SubsetOfPacks maybe) const {return this->private_nb_sols(wanted,maybe);};
		
		void KeepOnly(SubsetOfVars const & );
		void KeepOnly(SubsetOfPacks const & s) {return this->KeepOnly(s.getVars());};
		
		friend std::ostream& operator<<( std::ostream &flux, SubSystem const& sys);
		
		friend bool operator==(SubSystem const &, SubSystem const &);
		friend bool operator!=(SubSystem const & E1, SubSystem const & E2) {return !(E1==E2);};
		
		void onlyBoth(SubsetOfVars const &, SubsetOfVars const &) const;
		void onlyBoth(SubsetOfPacks const & s1, SubsetOfPacks const & s2) const {return this->onlyBoth(s1.getVars(), s2.getVars());};
		
		SubSystem extractRelated(PackOfVars const & lead);
		bool involves(PackOfVars const & );
		
		std::list<PackOfVars> testRelated();
		
		bool propagateIfKnown(SubsetOfVars const &);
		
		bool testAllKnown(SubsetOfPacks const & vars);
		bool testRelatedPacks(PackOfVars const & lead, SubsetOfPacks const & packs) const {return testRelatedPacks_tmp(lead,packs,lead.size());};
		bool testRelatedPacksNonLinear(PackOfVars const & lead, SubsetOfPacks const & packs) const {return testRelatedPacks_tmp(lead,packs,lead.size()+1);};
		
		SubsetOfVars involvedFull(SubsetOfPacks const & myset) const;
		SubsetOfVars getVarsANDOR() const;
	
	private:
	
		typedef std::pair<PackOfVars,int_fast8_t> MyPair;
		
		mutable Matrix<GFElement> m_matrix; //the matrix representing the system
		
		/* temporary stuff */
		unsigned int * m_index_col; // size >= number of columns
		unsigned int * m_index_line; // size >= number of lines 
		
		/* Variables per column: SubsetOfVars mode (fast union, intersection, ...) */
		SetOfVars m_set_of_vars;
		mutable std::vector<MyPair> m_line;
		mutable std::vector<MyPair> m_column;
		
		SubSystem(Matrix<GFElement> &&, SetOfVars const &, std::vector<MyPair> &&, std::vector<MyPair> &&);
		
		void swapLines(unsigned int, unsigned int) const;
		void swapColumns(unsigned int, unsigned int) const;
		void swapLineWithColumn(unsigned int, unsigned int) const;
		bool testRelatedPacks_tmp(PackOfVars const &, SubsetOfPacks const &, unsigned int) const;
		
		template <typename T, typename U> bool nb_sols_smaller_than_linear(int, T &, T &, std::vector<U> &, unsigned int, int) const;
		template <typename T, typename U> bool nb_sols_smaller_than_propagate(int, T &, T &, std::vector<U> &, unsigned int, int) const;
		template <typename T, typename U> bool nb_sols_smaller_than_tmp(int, T &, T &, std::vector<U> &, unsigned int, int) const;
		template <typename T> bool private_nb_sols_smaller_than(int, T &, T &) const;
		

		template <typename T, typename U> int nb_sols_linear(T &, T &, std::vector<U> &, unsigned int, int) const;
		template <typename T, typename U> int nb_sols_tmp(T &, T &, std::vector<U> &, unsigned int, int) const;

		template <typename T> int private_nb_sols(T &, T &) const;
		template <typename T> int private_nb_sols_original(T &, T &) const;
		
		friend class SystemOfEquations;
		
		void testRelated_tmp(unsigned int nb_lines, unsigned int nb_cols, std::list<PackOfVars> & my_list, PackOfVars start);		
	
};

#include "SubSystem.tpp"


#endif
