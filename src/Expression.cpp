/*
 * Expression.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "Expression.h"
#include "Sbox.h"
#include "SofExpression.h"

using namespace std;

Expression::Expression(Expression && expr) : m_cst(expr.m_cst), m_lin(move(expr.m_lin)), m_nonlin(move(expr.m_nonlin))
{
}

Expression::Expression(Expression const& expr) : m_cst(expr.m_cst), m_lin(expr.m_lin), m_nonlin(expr.m_nonlin)
{
}

Expression::Expression(SofExpression const& expr) : m_cst(0)
{
	m_nonlin.emplace(expr,1);
}

Expression::Expression(Variable const& v) : m_cst(0)
{
	m_lin.emplace(v,1);
}

Expression::Expression(const GFElement val) : m_cst(val)
{
}

Expression::Expression(const GFSymbol val) : m_cst(val)
{
}

Expression& Expression::operator=(Expression && expr)
{
	m_cst = expr.m_cst;
	m_lin = move(expr.m_lin);
	m_nonlin = move(expr.m_nonlin);
	return *this;
}

Expression& Expression::operator=(Expression const& expr)
{
	if (this != &expr)
	{
		m_cst = expr.m_cst;
		m_lin = expr.m_lin;
		m_nonlin = expr.m_nonlin;
	}
	return *this;
}


Expression& Expression::operator=(SofExpression const& expr)
{
	m_cst = 0;
	m_lin.clear();
	m_nonlin.clear();
	m_nonlin.emplace(expr,1); // = 1
	return *this;
}

Expression& Expression::operator=(Variable const& v)
{
	
	m_lin.clear();
	m_nonlin.clear();
	m_lin.emplace(v,1); // = 1
	return *this;
}
	
Expression& Expression::operator=(const GFElement val)
{
	m_lin.clear();
	m_nonlin.clear();
	m_cst = val;
	return *this;
}

Expression& Expression::operator=(const GFSymbol val)
{
	m_lin.clear();
	m_nonlin.clear();
	m_cst = val;
	return *this;
}

Expression& Expression::operator+=(Expression const& expr)
{
	m_cst += expr.m_cst;
	
	{
		pair<map<Variable,GFElement>::iterator,bool> ret;
		for (map<Variable,GFElement>::const_iterator it = expr.m_lin.cbegin(); it != expr.m_lin.cend() ; ++it)
		{
			ret = m_lin.insert(*it);
			if (ret.second == false)
			{
				if (m_lin.at(it->first) == it->second) m_lin.erase(it->first);  // Variable removed since new coefficient would be zero 
				else m_lin.at(it->first) += it->second;
			}
		}
	}
	
	{
		pair<map<SofExpression,GFElement>::iterator,bool> ret;
		for (map<SofExpression,GFElement>::const_iterator it = expr.m_nonlin.cbegin(); it != expr.m_nonlin.cend() ; ++it)
		{
			ret = m_nonlin.insert(*it);
			if (ret.second == false)
			{
				if (m_nonlin.at(it->first) == it->second) m_nonlin.erase(it->first);  // SofExpression removed since new coefficient would be zero 
				else m_nonlin.at(it->first) += it->second;
			}
		}
	}
	
	return *this;
}
	
	
Expression& Expression::operator+=(SofExpression const& expr)
{
	map<SofExpression,GFElement>::iterator it = m_nonlin.find(expr);
	if (it != m_nonlin.end()) // SofExpression is in the map
	{
		if (it->second == 1) m_nonlin.erase(it);
		else it->second += 1;
	}
	else
	{
		m_nonlin.emplace(expr,1);
	}
	return *this;
}

Expression& Expression::operator+=(Variable const& v)
{
	map<Variable,GFElement>::iterator it = m_lin.find(v);
	if (it != m_lin.end()) // SofExpression is in the map
	{
		if (it->second == 1) m_lin.erase(it);
		else it->second += 1;
	}
	else
	{
		m_lin.emplace(v,1);
	}
	return *this;
}

Expression& Expression::operator+=(const GFElement el)
{
	m_cst += el;
	return *this;
}

Expression& Expression::operator+=(const GFSymbol val)
{
	m_cst += val;
	return *this;
}
		
Expression& Expression::operator*=(const GFElement el)
{
	if (el != 1)
	{
		if (el != 0)
		{
			m_cst *= el;
			for (map<Variable,GFElement>::iterator it = m_lin.begin(); it != m_lin.end() ; ++it) it->second *= el;
			for (map<SofExpression,GFElement>::iterator it = m_nonlin.begin(); it != m_nonlin.end() ; ++it) it->second *= el;
		}
		else
		{
			m_cst = 0;
			m_lin.clear();
			m_nonlin.clear();
		}
	}
	return *this;
}

Expression& Expression::operator/=(const GFElement el)
{
	if (el != 1)
	{
		if (el != 0)
		{
			GFElement const inv_el = el.getInverse();
			m_cst *= inv_el;
			for (map<Variable,GFElement>::iterator it = m_lin.begin(); it != m_lin.end() ; ++it) it->second *= inv_el;
			for (map<SofExpression,GFElement>::iterator it = m_nonlin.begin(); it != m_nonlin.end() ; ++it) it->second *= inv_el;
		}
		else
		{
			cout << "Error: Division by zero of an Expression" << endl;
			exit(EXIT_FAILURE);
		}
	}
	return *this;
}

Expression& Expression::operator*=(const GFSymbol val)
{
	if (val != 1)
	{
		if (val != 0)
		{
			m_cst *= val;
			for (map<Variable,GFElement>::iterator it = m_lin.begin(); it != m_lin.end() ; ++it) it->second *= val;
			for (map<SofExpression,GFElement>::iterator it = m_nonlin.begin(); it != m_nonlin.end() ; ++it) it->second *= val;
		}
		else
		{
			m_cst = 0;
			m_lin.clear();
			m_nonlin.clear();
		}
	}
	return *this;
}

Expression& Expression::operator/=(const GFSymbol val)
{
	if (val != 1)
	{
		if (val != 0)
		{
			const GFSymbol inv_val = GFElement::inverse(val);
			m_cst *= inv_val;
			for (map<Variable,GFElement>::iterator it = m_lin.begin(); it != m_lin.end() ; ++it) it->second *= inv_val;
			for (map<SofExpression,GFElement>::iterator it = m_nonlin.begin(); it != m_nonlin.end() ; ++it) it->second *= inv_val;
		}
		else
		{
			cout << "Error: Division by zero of an Expression" << endl;
			exit(EXIT_FAILURE);
		}
	}
	return *this;
}

Expression operator+(Expression const& expr1, Expression const& expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(Expression const& expr1, SofExpression const& expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(Expression const& expr1, Variable const& expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(Expression const& expr1, const GFElement expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(Expression const& expr1, const GFSymbol expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(SofExpression const& expr1, SofExpression const& expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(SofExpression const& expr1, Variable const& expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(SofExpression const& expr1, const GFElement expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}

Expression operator+(SofExpression const& expr1, const GFSymbol expr2)
{
	Expression expr = expr1;
	expr += expr2;
	return expr;
}


Expression operator+(Variable const& expr1, const GFElement expr2)
{
	Expression expr (expr1);
	expr += expr2;
	return expr;
}

bool operator<(Expression const& expr1, Expression const& expr2)
{
	if (expr1.m_cst != expr2.m_cst) return (expr1.m_cst < expr2.m_cst);
	if (expr1.m_lin.size() != expr2.m_lin.size()) return (expr1.m_lin.size() < expr2.m_lin.size());
	if (expr1.m_nonlin.size() != expr2.m_nonlin.size()) return (expr1.m_nonlin.size() < expr2.m_nonlin.size());
	
	{
		map<Variable,GFElement>::const_iterator it2 = expr2.m_lin.cbegin();
		for (map<Variable,GFElement>::const_iterator it1 = expr1.m_lin.cbegin(); it1 != expr1.m_lin.cend() ; ++it1)
		{
			if (it1->second != it2->second) return (it1->second < it2->second);
			if (it1->first != it2->first) return (it1->first < it2->first);
		}
	}
	
	{
		map<SofExpression,GFElement>::const_iterator it2 = expr2.m_nonlin.cbegin();
		for (map<SofExpression,GFElement>::const_iterator it1 = expr1.m_nonlin.cbegin(); it1 != expr1.m_nonlin.cend() ; ++it1)
		{
			if (it1->second != it2->second) return (it1->second < it2->second);
			if (it1->first != it2->first) return (it1->first < it2->first);
		}
	}
	
	return false;
}

Expression operator*(const GFElement val, Expression const& expr)
{
	Expression res (expr);
	res*=val;
	return res;
}

Expression operator*(const GFSymbol val, Expression const& expr)
{
	Expression res (expr);
	res*=val;
	return res;
}

Expression operator*(const GFElement val, SofExpression const& expr)
{
	Expression res (expr);
	res*=val;
	return res;
}

Expression operator*(const GFSymbol val, SofExpression const& expr)
{
	Expression res (expr);
	res*=val;
	return res;
}

Expression operator*(const GFElement val, Variable const& expr)
{
	Expression res (expr);
	res*=val;
	return res;
}

Expression operator*(const GFSymbol val, Variable const& expr)
{
	Expression res (expr);
	res*=val;
	return res;
}

Expression operator*(Variable const expr, const GFSymbol val)
{
	Expression res (expr);
	res*=val;
	return res;
}

void Expression::print(ostream &flux) const
{
	unsigned int nb_terms = 0;
	for (map<Variable,GFElement>::const_iterator it = m_lin.cbegin(); it != m_lin.cend() ; ++it)
	{
		if (nb_terms > 0) flux << " + ";
		if (it->second != 1) flux << it->second << ".";
		flux << it->first;
		++nb_terms;
	}
	for (map<SofExpression,GFElement>::const_iterator it = m_nonlin.cbegin(); it != m_nonlin.cend() ; ++it)
	{
		if (nb_terms > 0) flux << " + ";
		if (it->second != 1) flux << it->second << ".";
		flux << it->first;
		++nb_terms;
	}
	if (nb_terms > 0) 
	{
		if (m_cst != 0)	flux << " + " << m_cst;
	}
	else flux << m_cst;
}

ostream& operator<<( ostream &flux, Expression const& expr)
{
	expr.print(flux);
	return flux;
}

Expression::Expression(std::string const& expr) : m_cst(0)
{
	string delims = "+*(."; //the delimiters
	string delims2 = " +*(."; 
	
	string::size_type start_index, end_index;
	start_index = expr.find_first_not_of(delims2);
	
	const unsigned int length_max_for_cst = (GFElement::getDim() + 3)/4;
	
	GFElement last_coeff_found = 1;
	
	while (start_index != string::npos)
	{
		end_index = expr.find_first_of(delims, start_index);
		if (end_index == string::npos) // case: Variable
		{
			end_index = expr.length();
			goto case_variable;
		}
		else
		{
			switch (expr[end_index])
			{
				case '+': // Variable
case_variable:
				{
					string::size_type tmp_end_index = end_index - 1;
					while (start_index <= tmp_end_index && expr[tmp_end_index] == ' ') --tmp_end_index;
					if (start_index > tmp_end_index)
					{
						cout << endl << "Error in Expression parser: consecutive operators found" << endl;
						exit(EXIT_FAILURE);
					}
					const string tmp = expr.substr(start_index, tmp_end_index + 1 - start_index);
					GFSymbol cst = 0;
					unsigned int pos = 0;
					while (pos < tmp.length() && tmp[pos] == '0') ++pos;
					bool is_cst = (tmp.length() <= length_max_for_cst + pos);
					if (is_cst) // it may be a cst
					{
						while (is_cst && pos < tmp.length())
						{
							if (tmp[pos] >= '0' && tmp[pos] <= '9')
							{
								cst = cst << 4;
								int hexa_val = tmp[pos] - '0';
								cst = cst | hexa_val;
							}
							else
							{
								if (tmp[pos] >= 'a' && tmp[pos] <= 'f')
								{
									cst = cst << 4;
									int hexa_val = (tmp[pos] - 'a') + 10;
									cst = cst | hexa_val;
								}
								else
								{
									is_cst = false;
								}
							}
							++pos;
						}
					}
					
					if (is_cst) // case: constant
					{
						m_cst += last_coeff_found * cst;
					}
					else // case: variable
					{
						const Variable v = tmp;
						map<Variable,GFElement>::iterator it = m_lin.find(v);
						if (it == m_lin.end()) m_lin.emplace(v,last_coeff_found);
						else it->second += last_coeff_found;
					}
					last_coeff_found = 1;
					break;
				}
				case '.':
				case '*': // Coefficient
				{
					string::size_type tmp_end_index = end_index - 1;
					while (start_index <= tmp_end_index && expr[tmp_end_index] == ' ') --tmp_end_index;
					if (start_index > tmp_end_index)
					{
						cout << endl << "Error in Expression parser: consecutive operators found" << endl;
						exit(EXIT_FAILURE);
					}
					const string tmp = expr.substr(start_index, tmp_end_index + 1 - start_index);
					GFSymbol cst = 0;
					unsigned int pos = 0;
					while (pos < tmp.length() && tmp[pos] == '0') ++pos;
					bool is_cst = (tmp.length() <= length_max_for_cst + pos);
					if (is_cst) // it may be a cst
					{
						while (is_cst && pos < tmp.length())
						{
							if (tmp[pos] >= '0' && tmp[pos] <= '9')
							{
								cst = cst << 4;
								int hexa_val = tmp[pos] - '0';
								cst = cst | hexa_val;
							}
							else
							{
								if (tmp[pos] >= 'a' && tmp[pos] <= 'f')
								{
									cst = cst << 4;
									int hexa_val = (tmp[pos] - 'a') + 10;
									cst = cst | hexa_val;
								}
								else is_cst = false;
							}
							++pos;
						}
					}
					if (is_cst) last_coeff_found *= cst;
					else
					{
						cout << endl << "Error in Expression parser: " << tmp << "is not a valid coefficient" << endl;
						exit(EXIT_FAILURE);
					}	
				}		
					break;
				default: // Sbox
				{
					const string sb_name = 	expr.substr(start_index, end_index - start_index);		
					vector<Expression> exprs;
					unsigned int nb_open = 1; // (
					unsigned int nb_open_4 = 0; // {
					unsigned int nb_open_5 = 0; // [
					++end_index;
					string::size_type start_index_expr = end_index;
					while (nb_open != 0 && end_index < expr.length())
					{
						switch (expr[end_index])
						{
							case '(': 
								nb_open += 1;
								break;
							case ')': 
								nb_open -= 1;
								break;
							case '{': 
								nb_open_4 += 1;
								break;
							case '}': 
								nb_open_4 -= 1;
								break;
							case '[': 
								nb_open_5 += 1;
								break;
							case ']': 
								nb_open_5 -= 1;
								break;
							case ',':
								if (nb_open == 1 && nb_open_4 == 0 && nb_open_5 == 0)
								{
									exprs.push_back(expr.substr(start_index_expr, end_index - start_index_expr));
									start_index_expr = end_index+1;
								}
								break;
							default:
								break;
						}
						++end_index;
					}
					if (nb_open == 0)
					{
						exprs.push_back(expr.substr(start_index_expr, end_index-1 - start_index_expr));
						const Sbox sb (sb_name,exprs.size());
						SofExpression sexpr (sb,exprs);
						map<SofExpression,GFElement>::iterator it = m_nonlin.find(sexpr);
						if (it == m_nonlin.end()) m_nonlin.emplace(sexpr,last_coeff_found);
						else it->second += last_coeff_found;
						last_coeff_found = 1;
					}
					else
					{
						cout << endl << "Error in Expression parser: ')' missing" << endl;
						exit(EXIT_FAILURE);
					}
				}
			}
		}
		
		start_index = expr.find_first_not_of(delims2, end_index);
	}
	
	
}
	
bool Expression::isVariable() const
{
	if (m_cst == 0 && m_lin.size() == 1 && m_nonlin.size() == 0)
	{
		map<Variable,GFElement>::const_iterator it = m_lin.cbegin();
		return (it->second == 1);
	}
	else return false;
}

bool Expression::isNormalized() const
{
	for (map<SofExpression,GFElement>::const_iterator it = m_nonlin.cbegin(); it != m_nonlin.cend() ; ++it)
	{
		SofExpression const& sexpr = it->first;
		if (!sexpr.isMonomial()) return false;
	}
	return true;
}

bool areColinear(Expression const& expr1, Expression const& expr2)
{
	if (expr1.m_lin.size() == expr2.m_lin.size() && expr1.m_nonlin.size() == expr2.m_nonlin.size())
	{
		if (expr1.m_lin.size() != 0)
		{
			map<Variable,GFElement>::const_iterator it1 = expr1.m_lin.cbegin();
			map<Variable,GFElement>::const_iterator it2 = expr2.m_lin.cbegin();
			if (it1->first == it2->first)
			{
				const GFElement coeff1 = it1->second;
				const GFElement coeff2 = it2->second;
				++it1;
				++it2;
				for (; it1 != expr1.m_lin.cend() ; ++it1)
				{
					if (it1->first == it2->first && coeff2*it1->second == coeff1*it2->second) continue;
					else return false;
					++it2;
				}
				map<SofExpression,GFElement>::const_iterator cit1 = expr1.m_nonlin.cbegin();
				map<SofExpression,GFElement>::const_iterator cit2 = expr2.m_nonlin.cbegin();
				for (; cit1 != expr1.m_nonlin.cend() ; ++cit1)
				{
					if (cit1->first == cit2->first && coeff2*cit1->second == coeff1*cit2->second) continue;
					else return false;
					++cit2;
				}
				return true;
			}
			else return false;
		}
		else
		{
			if (expr1.m_nonlin.size() != 0)
			{
				map<SofExpression,GFElement>::const_iterator cit1 = expr1.m_nonlin.cbegin();
				map<SofExpression,GFElement>::const_iterator cit2 = expr2.m_nonlin.cbegin();
				if (cit1->first == cit2->first)
				{
					const GFElement coeff1 = cit1->second;
					const GFElement coeff2 = cit2->second;
					
					for (; cit1 != expr1.m_nonlin.cend() ; ++cit1)
					{
						if (cit1->first == cit2->first && coeff2*cit1->second == coeff1*cit2->second) continue;
						else return false;
						++cit2;
					}
					return true;
				}
				else return false;
			}
			else return true;
		}
	}
	else return false;
}


bool Expression::replace(const Variable old, const Variable x)
{
	bool replaced = false;
	
	{
		map<SofExpression,GFElement> new_map;
		for (auto it = m_nonlin.cbegin(); it != m_nonlin.cend(); ++it)
		{
			SofExpression sexpr = it->first;
			replaced |= sexpr.replace(old,x);
			new_map[sexpr] = it->second;
		}
		m_nonlin = move(new_map);
	}
	
	auto it = m_lin.find(old);
	if (it != m_lin.end())
	{
		const GFElement coef = it->second;
		m_lin.erase(it);
		auto it_x = m_lin.find(x);
		if (it_x == m_lin.end()) m_lin[x] = coef;
		else
		{
			if (it_x->second == coef) m_lin.erase(it_x);
			else it_x->second += coef;
		}
		return true;
	}
	else return replaced;
	
}

void Expression::getVariables(set<Variable> & s) const
{
	for (auto it = lin_begin(); it != lin_end() ; ++it) s.insert(it->first);
	for (auto it = nonlin_begin(); it != nonlin_end() ; ++it) it->first.getVariables(s);
}

set<Variable> Expression::getVariables() const
{
	set<Variable> s;
	getVariables(s);
	return s;
}

bool operator==(Expression const & expr1, Expression const & expr2)
{
	if (expr1.m_cst != expr2.m_cst || expr1.m_lin.size() != expr2.m_lin.size() || expr1.m_nonlin.size() != expr2.m_nonlin.size()) return false;
	
	{
		auto it1 = expr1.m_lin.cbegin();
		auto end = expr1.m_lin.cend();
		auto it2 = expr2.m_lin.cbegin();
		while (it1 != end)
		{
			if (it1->second != it2->second || it1->first != it2->first) return false;
			++it1;
			++it2;
		}
	}
	
	{
		auto it1 = expr1.m_nonlin.cbegin();
		auto end = expr1.m_nonlin.cend();
		auto it2 = expr2.m_nonlin.cbegin();
		while (it1 != end)
		{
			if (it1->second != it2->second || it1->first != it2->first) return false;
			++it1;
			++it2;
		}
	}
	return true;
}

bool operator==(Expression const & expr, Variable const & v)
{
	if (!expr.isVariable()) return false;
	return (expr.m_lin.cbegin()->first == v);
}


bool operator==(Expression const & expr, SofExpression const & sexpr)
{
	if (expr.m_cst != 0 || expr.m_lin.size() != 0 || expr.m_nonlin.size() != 1) return false;
	auto it = expr.m_nonlin.cbegin();
	return (it->second == 1 && it-> first == sexpr);
}


Expression::Expression(Sbox const & S, std::vector<Expression> const& Exprs) : m_cst(0)
{
	SofExpression sexpr ( S, Exprs);
	m_nonlin.emplace(sexpr,1);
}

int_fast8_t Expression::getTypeSbox() const
{
	if (m_nonlin.size() + m_lin.size() > 1) return 2;
	if (m_nonlin.empty()) return 0;
	
	auto name = m_nonlin.begin()->first.m_sb.getName();
	if (name ==  "AND" || name == "OR") return -1;
	else return 1;
}
