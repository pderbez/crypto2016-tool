/*
 * BlockCipher.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "BlockCipher.h"

#include <iostream>
#include <utility> 
#include <vector> 

using namespace std;

BlockCipher::~BlockCipher()
{
}

BlockCipher::BlockCipher(SystemOfEquations const & sys) : m_E_eq_key_unknown_pc_unknown(sys.getSubSystem())
{
	//cout << "sys: " << endl << sys;
	//getchar();
	
	vector<Variable> all_vars = sys.getSetOfVars().get();
	
	/* Finding known variables */
	{
		auto it = all_vars.begin();
		while (it != all_vars.end())
		{
			if (sys.isKnown(*it))
			{
				*it = *all_vars.rbegin();
				all_vars.pop_back();
			}
			else ++it;
		}
		
		unsigned int cpt_known = 0;
		bool new_known = true;
		while (new_known == true)
		{
			new_known = false;
			vector<Variable> vec_known;
			it = all_vars.begin();
			while (it != all_vars.end())
			{
				if (m_E_eq_key_unknown_pc_unknown.getNbSols(*it) <= 0)
				{
					new_known = true;
					++cpt_known;
					if (cpt_known == 1) cout << "New known variables: ";
					cout << *it << ' ';
					vec_known.push_back(*it);
					*it = *all_vars.rbegin();
					all_vars.pop_back();
				}
				else ++it;
			}
			if (!vec_known.empty()) m_E_eq_key_unknown_pc_unknown.setKnownVars(vec_known.begin(),vec_known.end());
		}
		if (cpt_known != 0) cout << endl;
	}
	
	/* Removing linear variables */
	
removing_linear:
	{
		bool linear_found = false;
		unsigned int i = 0;
		while (i < all_vars.size())
		{
			Variable & v = all_vars[i];
			if (v.priority() <= 0 && m_E_eq_key_unknown_pc_unknown.isLinear(v) == true) // v is linear and is not a key var or a plaintext/ciphertext var
			{
				v = *all_vars.rbegin();
				all_vars.pop_back();
				linear_found = true;
			}
			else ++i;
		}
		if (linear_found)
		{
			m_E_eq_key_unknown_pc_unknown = m_E_eq_key_unknown_pc_unknown.Extract(all_vars.begin(),all_vars.end());
			goto removing_linear;
		}
	}
	
	/* Processing key variables */
	
	{
		unsigned int nb_key_vars = 0;
		const unsigned int bound = all_vars.size();
		for (unsigned int i = 0; i < bound; ++i)
		{
			if (all_vars[i].priority() == 1) swap(all_vars[i],all_vars[nb_key_vars++]);
		}
		auto end = all_vars.begin();
		for (unsigned int i = 0; i < nb_key_vars; ++i) ++end;
		SubsetOfVars key_vars = sys.getSetOfVars().getSubset(all_vars.begin(),end);
		m_keysize = m_E_eq_key_unknown_pc_unknown.getNbSols(key_vars);
		
		
simplify_keyschedule:
		{
			bool var_removed = false;
			unsigned int i = 0;
			while (i < nb_key_vars)
			{
				if (m_E_eq_key_unknown_pc_unknown.isLinear(all_vars[i]))
				{
					key_vars -= all_vars[i];
					if (m_E_eq_key_unknown_pc_unknown.getNbSols(key_vars) == m_keysize)
					{
						all_vars[i] = all_vars[--nb_key_vars];
						all_vars[nb_key_vars] = *(all_vars.rbegin());
						all_vars.pop_back();
						var_removed = true;
					}
					else
					{
						key_vars += all_vars[i];
						++i;
					}
				}
				else ++i;
			}
			if (var_removed)
			{
				m_E_eq_key_unknown_pc_unknown = m_E_eq_key_unknown_pc_unknown.Extract(all_vars.begin(),all_vars.end());
				goto simplify_keyschedule;
			}
		}
	}
	
	
	m_set_of_vars = SetOfVars(all_vars.begin(),all_vars.end()); //the set of all involved variables
	
	m_E_eq_key_unknown_pc_unknown.setSetOfVars(m_set_of_vars);
	
	this->initialize_PacksOfVars();
	
	m_E_keyschedule = m_E_eq_key_unknown_pc_unknown.Extract(m_key_vars);
	
	m_E_eq_key_known_pc_unknown = m_E_eq_key_unknown_pc_unknown;
	m_E_eq_key_known_pc_unknown.setKnownVars(m_key_vars);
	
	m_E_eq_key_unknown_pc_known = m_E_eq_key_unknown_pc_unknown;
	m_E_eq_key_unknown_pc_known.setKnownVars(m_plaintext_vars + m_ciphertext_vars);
	
	m_E_eq_nonlin_key_unknown_pc_unknown = m_E_eq_key_unknown_pc_unknown.Extract(m_plaintext_vars + m_ciphertext_vars + m_non_lin_key_vars + m_state_vars);
	
	
	m_statesize = m_E_eq_key_unknown_pc_unknown.getNbSols(m_plaintext_vars);
	if (m_E_eq_key_unknown_pc_unknown.getNbSols(m_ciphertext_vars) != m_statesize)
	{
		cout << "Error: the system cannot be handled as a blockcipher";
		exit(EXIT_FAILURE);
	}
	
	cout << "BlockCipher:" << endl;
	cout << " - key_vars (" << m_non_lin_key_vars.size() << '/' << m_key_vars.size() << "): " << m_key_vars << endl;
	cout << " - state_vars (" << m_state_vars.size() << "): " << m_state_vars << endl;
	cout << " - plaintext_vars: " << m_plaintext_vars << endl;
	cout << " - ciphertext_vars: " << m_ciphertext_vars << endl;
	cout << endl;
	cout << " - keysize: " << m_keysize << endl;
	cout << " - statesize: " << m_statesize << endl;
	cout << endl;
	

	cout << "Initialization: " << endl;
	this->initialize_m_from_p_to_v();
	this->initialize_m_from_c_to_v();
	cout << " - final step: " << flush;
	
	
	vector<PackOfVars> all_non_key_vars = (this->getPlaintextVars() + this->getCiphertextVars() + this->getStateVars()).get();
	
	for (auto & v : all_non_key_vars) 
	{
		this->initialize_m_from_v_to_p(v);
		this->initialize_m_from_v_to_c(v);
	}
	
	for (auto & v : all_non_key_vars) 
	{
		this->initialize_m_guess_to_p(v);
		this->initialize_m_guess_to_c(v);
	}
	
	for (auto & v : all_non_key_vars) 
	{
		this->initialize_m_guess_from_p(v);
		this->initialize_m_guess_from_c(v);
	}
	
	if (m_guess_to_c.size() != all_non_key_vars.size()) getchar();
	if (m_guess_to_p.size() != all_non_key_vars.size()) getchar();
	
	this->initialize_isSPN();
	
	if (this->isSPN())
	{
		for (auto & v : all_non_key_vars) 
		{
			this->initialize_m_min_related_from_p(v);
			this->initialize_m_min_related_from_c(v);
		}
	}
	
	cout << "done" << endl;	
	cout << "The blockcipher is ";
	if (!this->isSPN()) cout << "not ";
	cout << "an SPN" << endl;
}

void BlockCipher::initialize_isSPN()
{
	auto vec = (this->getStateVars() + this->getPlaintextVars() + this->getCiphertextVars()).get();
	for (auto const & v : vec)
	{
		if ((this->varsFromPtoV(v) != this->varsFromVtoP(v)) || (this->varsFromCtoV(v) != this->varsFromVtoC(v)))
		{
			m_spn = false;
			m_mds = false;
			return;
		}
	}
	m_spn = true;
	m_mds = true;
}

void BlockCipher::initialize_m_from_p_to_v(PackOfVars const & v, vector<SubsetOfPacks> const & way_fromC)
{	
	SubsetOfPacks all_state_vars = this->getStateVars() + this->getPlaintextVars() + this->getCiphertextVars();
	for (auto const & p : m_from_p_to_v)
	{
		if (p.second.includes(v)) all_state_vars &= p.second;
	}
	SubsetOfPacks removable = all_state_vars - (this->getPlaintextVars() + v);
	SubSystem E = this->getEqsKnownKey().Extract(all_state_vars);
	
	for (auto const & w : way_fromC)
	{
		auto my_vec = (w & removable).get();
		bool var_removed = false;
		for (unsigned int i = 0; i < my_vec.size(); i++)
		{
			if (E.isLinear(my_vec[i]))
			{
				all_state_vars -= my_vec[i];
				var_removed = true;
			}
		}
		if (var_removed) E.KeepOnly(all_state_vars);
	}
	
	int nb_sols = E.getNbSols(all_state_vars);
	auto my_vec = (this->getPlaintextVars() & all_state_vars).get();
	{
		unsigned int i = 0;
		while (i < my_vec.size())
		{
			PackOfVars & x = my_vec[i];
			all_state_vars -= x;
			const int new_nb_sols = E.getNbSols(all_state_vars);
			if (new_nb_sols < nb_sols)
			{
				nb_sols = new_nb_sols;
				x = *(my_vec.rbegin());
				my_vec.pop_back();
			}
			else
			{
				all_state_vars += x;
				++i;
			}
		}
	}
	
	//cout << "from plaintext to " << v << ": " << all_state_vars << endl;
	//getchar();
	m_from_p_to_v.emplace(v,all_state_vars);
}

void BlockCipher::initialize_m_from_p_to_v()
{
	unsigned int cpt = 0;
	const unsigned int cpt_max = (this->getStateVars() + this->getPlaintextVars() + this->getCiphertextVars()).size();
	vector<SubsetOfPacks> way_fromC;
	SubsetOfPacks now = this->getCiphertextVars();
	way_fromC.emplace_back(now);
	SubsetOfPacks all_state_vars = this->getStateVars();
	vector<PackOfVars> my_vec = all_state_vars.get();
	all_state_vars += this->getPlaintextVars();
	SubSystem E = this->getEqsKnownKey().Extract(all_state_vars);
start_removing:
	{
		now.clear();
		bool var_removed = false;
		unsigned int i = 0;
		while (i < my_vec.size())
		{
			PackOfVars & x = my_vec[i];
			if (E.isLinear(x))
			{
				now += x;
				all_state_vars -= x;
				x = move(*(my_vec.rbegin()));
				my_vec.pop_back();
				var_removed = true;
			}
			else ++i;
		}
		if (var_removed)
		{
			way_fromC.emplace_back(now);
			E.KeepOnly(all_state_vars);
			goto start_removing;
		}
		else
		{
			if (all_state_vars != this->getPlaintextVars())
			{
				cout << "The blockcipher is not compatible" << endl;
				exit(EXIT_FAILURE);
			}
		}
	}
	for (auto const & s : way_fromC)
	{
		auto my_vec = s.get();
		for (auto const & v : my_vec)
		{
			this->initialize_m_from_p_to_v(v,way_fromC);
			cout << "\r - step 1: " << ++cpt << "/" << cpt_max <<  flush;
		}
	}
	
	my_vec = this->getPlaintextVars().get();
	for (auto const & v : my_vec)
	{
		now.clear();
		now += v;
		m_from_p_to_v.emplace(v,now);
		cout << "\r - step 1: " << ++cpt << "/" << cpt_max <<  flush;
	}
	cout << endl;
}

void BlockCipher::initialize_m_from_c_to_v(PackOfVars const & v, vector<SubsetOfPacks> const & way_fromP)
{	
	SubsetOfPacks all_state_vars = this->getStateVars() + this->getPlaintextVars() + this->getCiphertextVars();
	for (auto const & p : m_from_c_to_v)
	{
		if (p.second.includes(v)) all_state_vars &= p.second;
	}
	SubsetOfPacks removable = all_state_vars - (this->getCiphertextVars() + v);
	SubSystem E = this->getEqsKnownKey().Extract(all_state_vars);
	
	for (auto const & w : way_fromP)
	{
		auto my_vec = (w & removable).get();
		bool var_removed = false;
		for (unsigned int i = 0; i < my_vec.size(); i++)
		{
			if (E.isLinear(my_vec[i]))
			{
				all_state_vars -= my_vec[i];
				var_removed = true;
			}
		}
		if (var_removed) E.KeepOnly(all_state_vars);
	}
	
	int nb_sols = E.getNbSols(all_state_vars);
	auto my_vec = (this->getCiphertextVars() & all_state_vars).get();
	{
		unsigned int i = 0;
		while (i < my_vec.size())
		{
			PackOfVars & x = my_vec[i];
			all_state_vars -= x;
			const int new_nb_sols = E.getNbSols(all_state_vars);
			if (new_nb_sols < nb_sols)
			{
				nb_sols = new_nb_sols;
				x = *(my_vec.rbegin());
				my_vec.pop_back();
			}
			else
			{
				all_state_vars += x;
				++i;
			}
		}
	}
	
	m_from_c_to_v.emplace(v,all_state_vars);
}

void BlockCipher::initialize_m_from_c_to_v()
{
	unsigned int cpt = 0;
	const unsigned int cpt_max = (this->getStateVars() + this->getPlaintextVars() + this->getCiphertextVars()).size();
	vector<SubsetOfPacks> way_fromP;
	SubsetOfPacks now = this->getPlaintextVars();
	way_fromP.emplace_back(now);
	SubsetOfPacks all_state_vars = this->getStateVars();
	vector<PackOfVars> my_vec = all_state_vars.get();
	all_state_vars += this->getCiphertextVars();
	SubSystem E = this->getEqsKnownKey().Extract(all_state_vars);
start_removing:
	{
		now.clear();
		bool var_removed = false;
		unsigned int i = 0;
		while (i < my_vec.size())
		{
			PackOfVars & x = my_vec[i];
			if (E.isLinear(x))
			{
				now += x;
				all_state_vars -= x;
				x = move(*(my_vec.rbegin()));
				my_vec.pop_back();
				var_removed = true;
			}
			else ++i;
		}
		if (var_removed)
		{
			way_fromP.emplace_back(now);
			E.KeepOnly(all_state_vars);
			goto start_removing;
		}
		else
		{
			if (all_state_vars != this->getCiphertextVars())
			{
				cout << "The blockcipher is not compatible" << endl;
				exit(EXIT_FAILURE);
			}
		}
	}
	for (auto const & s : way_fromP)
	{
		auto my_vec = s.get();
		for (auto const & v : my_vec)
		{
			this->initialize_m_from_c_to_v(v,way_fromP);
			cout << "\r - step 2: " << ++cpt << "/" << cpt_max <<  flush;
		}
	}
	
	my_vec = this->getCiphertextVars().get();
	for (auto const & v : my_vec)
	{
		now.clear();
		now += v;
		m_from_c_to_v.emplace(v,now);
		cout << "\r - step 2: " << ++cpt << "/" << cpt_max <<  flush;
	}
	cout << endl;
}


void BlockCipher::initialize_m_from_v_to_p(PackOfVars const & v)
{	
	SubsetOfPacks my_subset = m_set_of_pack.empty_set();
	for (auto const & x : m_from_c_to_v)
	{
		if (x.second.includes(v))
		{
			my_subset += x.first;
		}
	}
	
	m_from_v_to_p.emplace(v,move(my_subset));
	//cout << "from " << v << "to plaintext: " << m_from_v_to_p[v] << endl << endl;
	//getchar();
}

void BlockCipher::initialize_m_guess_from_p(PackOfVars const & v)
{
	auto res = getPlaintextVars();
	auto vec = res.get();
	for (auto const & x : vec) res += guessToC(x);
	m_guess_from_p.emplace(v,(res & varsFromPtoV(v)));
}

void BlockCipher::initialize_m_guess_to_p(PackOfVars const & v)
{
	auto tmp = varsFromCtoV(v);
	auto vec = varsFromVtoP(v).get();
	for (auto const & x : vec) tmp += varsFromCtoV(x);	
	SubSystem E = getEqsKnownKey().Extract(tmp);
	auto const lol = E.involvedFull(varsFromVtoP(v));
	auto sub = getPlaintextVars() + getCiphertextVars() + getStateVars();
	vec = sub.get();
	sub.clear();
	for (auto const & x : vec)
	{
		if (lol.shareElements(x)) sub += x;
	}
	m_guess_to_p.emplace(v,move(sub));
	//if (v.size() == 1 && varsFromCtoV(v).size() == varsFromPtoV(v).size()) cout << "guess " << v << "to plaintext: " << m_guess_to_p[v] << endl << endl;
}

void BlockCipher::initialize_m_guess_from_c(PackOfVars const & v)
{
	auto res = getCiphertextVars();
	auto vec = res.get();
	for (auto const & x : vec) res += guessToP(x);
	m_guess_from_c.emplace(v,(res & varsFromCtoV(v)));
	//if (v.size() == 1 && varsFromCtoV(v).size() == varsFromPtoV(v).size()) cout << "guess " << v << "from ciphertext: " << m_guess_from_c[v] << endl << endl;
}

void BlockCipher::initialize_m_guess_to_c(PackOfVars const & v)
{
	auto tmp = varsFromPtoV(v);
	auto vec = varsFromVtoC(v).get();
	for (auto const & x : vec) tmp += varsFromPtoV(x);	
	SubSystem E = getEqsKnownKey().Extract(tmp);
	auto const lol = E.involvedFull(varsFromVtoC(v));
	auto sub = getPlaintextVars() + getCiphertextVars() + getStateVars();
	vec = sub.get();
	sub.clear();
	for (auto const & x : vec)
	{
		if (lol.shareElements(x)) sub += x;
	}
	m_guess_to_c.emplace(v,move(sub));
}


void BlockCipher::initialize_m_from_v_to_c(PackOfVars const & v)
{
	SubsetOfPacks my_subset = m_set_of_pack.empty_set();
	for (auto const & x : m_from_p_to_v)
	{
		if (x.second.includes(v)) my_subset += x.first;
	}
	m_from_v_to_c.emplace(v,move(my_subset));
	//cout << "from " << v << "to ciphertext: " << m_from_v_to_c[v] << endl << endl;
}


SubsetOfPacks initialize_m_min_related(PackOfVars const & v, SubsetOfPacks my_subset, SubSystem const & E)
{
	my_subset -= v;
	vector<PackOfVars> my_vec = my_subset.get();
	unsigned int nb_pack_of_vars = my_vec.size();
	
	{
		unsigned int i = 0;
		while (i < nb_pack_of_vars)
		{
			vector<Variable> v_tmp = my_vec[i].get();
			if (v_tmp[0].priority() > 1) // i.e. *it is a plaintext/ciphertext var
			{
				--nb_pack_of_vars;
				swap(my_vec[i],my_vec[nb_pack_of_vars]);
			}
			else ++i;
		}
	}

	{
		unsigned int i = nb_pack_of_vars;
		while (i < my_vec.size())
		{
			my_subset -= my_vec[i];
			my_subset += v;
			const int nb_sols_w_v = E.getNbSols(my_subset);
			my_subset -= v;
			const int nb_sols_wo_v = E.getNbSols(my_subset);
			if (nb_sols_w_v == nb_sols_wo_v)
			{
				my_vec[i] = *(my_vec.rbegin());
				my_vec.pop_back();
				i = nb_pack_of_vars;
			}
			else
			{
				my_subset += my_vec[i];
				++i;
			}	
		}
	}
	
	{
		unsigned int i = 0;
		while (i < my_vec.size())
		{
			my_subset -= my_vec[i];
			my_subset += v;
			const int nb_sols_w_v = E.getNbSols(my_subset);
			my_subset -= v;
			const int nb_sols_wo_v = E.getNbSols(my_subset);
			if (nb_sols_w_v == nb_sols_wo_v)
			{
				my_vec[i] = *(my_vec.rbegin());
				my_vec.pop_back();
				i = 0;
			}
			else
			{
				my_subset += my_vec[i];
				++i;
			}	
		}
	}
	
	return my_subset;
}

void BlockCipher::initialize_m_min_related_from_p(PackOfVars const & v)
{	
	m_min_related_from_p.emplace(v,initialize_m_min_related(v,this->varsFromPtoV(v),this->getEqsKnownKey()));
	//cout << "min related from P (" << v << "): " << m_min_related_from_p.at(v) << endl;
}

void BlockCipher::initialize_m_min_related_from_c(PackOfVars const & v)
{	
	m_min_related_from_c.emplace(v,initialize_m_min_related(v,this->varsFromCtoV(v),this->getEqsKnownKey()));
	//cout << "min related from C (" << v << "): " << m_min_related_from_c.at(v) << endl;
}


BlockCipher::BlockCipher(BlockCipher const & B) : m_set_of_vars(B.m_set_of_vars), m_set_of_pack(B.m_set_of_pack), m_E_keyschedule(B.m_E_keyschedule), m_E_eq_key_known_pc_unknown(B.m_E_eq_key_known_pc_unknown),
m_E_eq_key_unknown_pc_known(B.m_E_eq_key_unknown_pc_known), m_E_eq_key_unknown_pc_unknown(B.m_E_eq_key_unknown_pc_unknown), m_E_eq_nonlin_key_unknown_pc_unknown(B.m_E_eq_nonlin_key_unknown_pc_unknown),
m_state_vars(B.m_state_vars), m_key_vars(B.m_key_vars), m_non_lin_key_vars(B.m_non_lin_key_vars), m_plaintext_vars(B.m_plaintext_vars), m_ciphertext_vars(B.m_ciphertext_vars),
m_keysize(B.m_keysize), m_statesize(B.m_statesize), m_from_p_to_v(B.m_from_p_to_v), m_from_c_to_v(B.m_from_c_to_v),
m_from_v_to_p(B.m_from_v_to_p), m_from_v_to_c(B.m_from_v_to_c), m_min_related_from_p(B.m_min_related_from_p), m_min_related_from_c(B.m_min_related_from_c),
m_guess_from_p(B.m_guess_from_p), m_guess_from_c(B.m_guess_from_c), m_guess_to_p(B.m_guess_to_p), m_guess_to_c(B.m_guess_to_c),
m_spn(B.m_spn), m_mds(B.m_mds), m_biggest_pack(B.m_biggest_pack)
{
}
	
	
BlockCipher::BlockCipher(BlockCipher && B) : m_set_of_vars(move(B.m_set_of_vars)), m_set_of_pack(move(B.m_set_of_pack)), m_E_keyschedule(move(B.m_E_keyschedule)), m_E_eq_key_known_pc_unknown(move(B.m_E_eq_key_known_pc_unknown)),
m_E_eq_key_unknown_pc_known(move(B.m_E_eq_key_unknown_pc_known)), m_E_eq_key_unknown_pc_unknown(move(B.m_E_eq_key_unknown_pc_unknown)), m_E_eq_nonlin_key_unknown_pc_unknown(move(B.m_E_eq_nonlin_key_unknown_pc_unknown)),
m_state_vars(move(B.m_state_vars)), m_key_vars(move(B.m_key_vars)), m_non_lin_key_vars(move(B.m_non_lin_key_vars)),
m_plaintext_vars(move(B.m_plaintext_vars)), m_ciphertext_vars(move(B.m_ciphertext_vars)),
m_keysize(B.m_keysize), m_statesize(B.m_statesize), m_from_p_to_v(move(B.m_from_p_to_v)), m_from_c_to_v(move(B.m_from_c_to_v)),
m_from_v_to_p(move(B.m_from_v_to_p)), m_from_v_to_c(move(B.m_from_v_to_c)), m_min_related_from_p(move(B.m_min_related_from_p)), m_min_related_from_c(move(B.m_min_related_from_c)),
m_guess_from_p(move(B.m_guess_from_p)), m_guess_from_c(move(B.m_guess_from_c)), m_guess_to_p(move(B.m_guess_to_p)), m_guess_to_c(move(B.m_guess_to_c)),
m_spn(B.m_spn), m_mds(B.m_mds), m_biggest_pack(B.m_biggest_pack)
{
}
		
BlockCipher & BlockCipher::operator=(BlockCipher const & B)
{
	if (this != &B)
	{
		m_set_of_vars = B.m_set_of_vars;
		m_set_of_pack = B.m_set_of_pack;
	
		m_E_keyschedule = B.m_E_keyschedule;
		m_E_eq_key_known_pc_unknown = B.m_E_eq_key_known_pc_unknown;
		m_E_eq_key_unknown_pc_known = B.m_E_eq_key_unknown_pc_known;
		m_E_eq_key_unknown_pc_unknown = B.m_E_eq_key_unknown_pc_unknown;
		m_E_eq_nonlin_key_unknown_pc_unknown = B.m_E_eq_nonlin_key_unknown_pc_unknown;
		
		m_state_vars = B.m_state_vars;
		m_key_vars = B.m_key_vars; 
		m_non_lin_key_vars = B.m_non_lin_key_vars;
		m_plaintext_vars = B.m_plaintext_vars;
		m_ciphertext_vars = B.m_ciphertext_vars;
		
		m_keysize = B.m_keysize; 
		m_statesize = B.m_statesize;
		
		m_from_p_to_v = B.m_from_p_to_v;
		m_from_c_to_v = B.m_from_c_to_v;
		
		m_from_v_to_p = B.m_from_v_to_p;
		m_from_v_to_c = B.m_from_v_to_c;
		
		m_min_related_from_p = B.m_min_related_from_p;
		m_min_related_from_c = B.m_min_related_from_c;
		
		m_guess_from_p = B.m_guess_from_p;
		m_guess_from_c = B.m_guess_from_c;
		m_guess_to_p = B.m_guess_to_p;
		m_guess_to_c = B.m_guess_to_c;
		
		m_spn = B.m_spn;
		m_mds = B.m_mds;
		
		m_biggest_pack = B.m_biggest_pack;
	}
	return *this;
}

BlockCipher & BlockCipher::operator=(BlockCipher && B)
{
	m_set_of_vars = move(B.m_set_of_vars);
	m_set_of_pack = move(B.m_set_of_pack);
	
	m_E_keyschedule = move(B.m_E_keyschedule);
	m_E_eq_key_known_pc_unknown = move(B.m_E_eq_key_known_pc_unknown);
	m_E_eq_key_unknown_pc_known = move(B.m_E_eq_key_unknown_pc_known);
	m_E_eq_key_unknown_pc_unknown = move(B.m_E_eq_key_unknown_pc_unknown);
	m_E_eq_nonlin_key_unknown_pc_unknown = move(B.m_E_eq_nonlin_key_unknown_pc_unknown);
		
	m_state_vars = move(B.m_state_vars);
	m_key_vars = move(B.m_key_vars); 
	m_non_lin_key_vars = move(B.m_non_lin_key_vars);
	m_plaintext_vars = move(B.m_plaintext_vars);
	m_ciphertext_vars = move(B.m_ciphertext_vars);
		
	m_keysize = B.m_keysize; 
	m_statesize = B.m_statesize;
		
	m_from_p_to_v = move(B.m_from_p_to_v);
	m_from_c_to_v = move(B.m_from_c_to_v);
		
	m_from_v_to_p = move(B.m_from_v_to_p);
	m_from_v_to_c = move(B.m_from_v_to_c);
	
	m_min_related_from_p = move(B.m_min_related_from_p);
	m_min_related_from_c = move(B.m_min_related_from_c);
	
	m_guess_from_p = move(B.m_guess_from_p);
	m_guess_from_c = move(B.m_guess_from_c);
	m_guess_to_p = move(B.m_guess_to_p);
	m_guess_to_c = move(B.m_guess_to_c);
	
	m_spn = B.m_spn;
	m_mds = B.m_mds;
	
	m_biggest_pack = B.m_biggest_pack;
	
	return *this;
}

void BlockCipher::initialize_PacksOfVars()
{
	vector<SubsetOfVars> my_vec = this->getEqs().getSubsetsOfVars();

	set<SubsetOfVars> my_set_tmp;
	set<SubsetOfVars> my_set_of_subsets;
	{
		unsigned int i = 0;
		while (i < my_vec.size())
		{
			auto size_i = my_vec[i].size();
			if (size_i == 1)
			{
				bool twice = false;
				unsigned int j = i+1;
				while (j < my_vec.size())
				{
					if (my_vec[i] == my_vec[j])
					{
						twice = true;
						my_vec[j] = move(*my_vec.rbegin());
						my_vec.pop_back();
					}
					else ++j;
				}
				if (!twice)
				{
					auto vec_v = my_vec[i].get();
					Variable const & v = vec_v[0];
					if (v.priority() >= 1)
					{
						if (v.priority() == 1) my_set_tmp.emplace(move(my_vec[i]));
						else twice = true;
					}
				}
				if (twice) my_set_of_subsets.emplace(move(my_vec[i]));
				my_vec[i] = move(*my_vec.rbegin());
				my_vec.pop_back();
			}
			else 
			{
				if (size_i > 1)
				{
					auto vec_v = my_vec[i].get();
					Variable const & v = vec_v[0];
					if (v.priority() == 1)
					{
						my_set_of_subsets.emplace(move(my_vec[i]));
						my_vec[i] = move(*my_vec.rbegin());
						my_vec.pop_back();
					}
					else ++i;
				}
				else
				{
					my_vec[i] = move(*my_vec.rbegin());
					my_vec.pop_back();
				}
			}		
		}
		for (auto const & k : my_set_tmp)
		{
			auto it = my_set_of_subsets.begin();
			while (it != my_set_of_subsets.end() && !it->includes(k)) ++it;
			if (it == my_set_of_subsets.end()) my_set_of_subsets.emplace(k);
		}
		auto sub_ANDOR = this->getEqs().getVarsANDOR();
		auto my_vec_ANDOR = sub_ANDOR.get();
		for (auto const & v : my_vec_ANDOR)
		{
			if (v.priority() != 1) my_vec.emplace_back((sub_ANDOR.empty_set() + v));
		}
		i = 0;
		while (i < my_vec.size())
		{
			set<SubsetOfVars> s_tmp;
			for (auto const & s : my_set_of_subsets)
			{
				auto tmp1 = s & my_vec[i];
				auto tmp2 = s - my_vec[i];
				my_vec[i] -= tmp1;
				s_tmp.emplace(move(tmp1));
				s_tmp.emplace(move(tmp2));
			}
			s_tmp.emplace(move(my_vec[i]));
			my_set_of_subsets = move(s_tmp);
			++i;
		}
		
		auto it = my_set_of_subsets.begin();
		while (it != my_set_of_subsets.end())
		{
			if (it->size() == 0) it = my_set_of_subsets.erase(it);
			else ++it;
		}
		m_biggest_pack = 0;
		for (auto const & sv : my_set_of_subsets)
		{
			if (sv.size() > m_biggest_pack) m_biggest_pack = sv.size();
		}
		
	}
	
	set<PackOfVars> final_set (my_set_of_subsets.begin(),my_set_of_subsets.end());
	
	m_set_of_pack = SetOf<PackOfVars>(final_set.begin(),final_set.end());
	
	m_state_vars = m_set_of_pack.empty_set();
	m_key_vars = m_set_of_pack.empty_set();
	m_plaintext_vars = m_set_of_pack.empty_set();
	m_ciphertext_vars = m_set_of_pack.empty_set();
	
	for (auto & s : final_set)
	{
		vector<Variable> v = s.get();
		const int priority = v[0].priority();
		for (auto & x : v) assert(x.priority() == priority);
		switch (priority)
		{
			case 1:
				m_key_vars += s;
				break;
			case 2:
				m_plaintext_vars += s;
				break;
			case 3:
				m_ciphertext_vars += s;
				break;
			default:
				m_state_vars += s;
		}
	}
	
	{
		m_non_lin_key_vars = m_key_vars;
		auto v_non_lin = m_non_lin_key_vars.get();
		SubSystem E = this->getEqs();
remove_lin:
		{
			auto const size_bak = v_non_lin.size();
			unsigned int i = 0;
			while (i != v_non_lin.size())
			{
				if (E.isLinear(v_non_lin[i]))
				{
					m_non_lin_key_vars -= v_non_lin[i];
					v_non_lin[i] = move(*v_non_lin.rbegin());
					v_non_lin.pop_back();
				}
				else ++i;
			}
			if (size_bak > v_non_lin.size())
			{
				E.KeepOnly(m_state_vars + m_ciphertext_vars + m_plaintext_vars + m_non_lin_key_vars);
				goto remove_lin;
			}
		}
	}
}

#if 0

void BlockCipher::sortVectorFromPtoV(vector<PackOfVars> & my_vec) const
{
	unsigned int i = 0;
	const unsigned int my_vec_size = my_vec.size();
	while (i < my_vec_size)
	{
		auto size_i = this->varsFromPtoV(my_vec[i]).size();
		auto size2_i = this->varsFromVtoP(my_vec[i]).size();
		unsigned int j = i + 1;
		while (j < my_vec_size)
		{
			auto size_j = this->varsFromPtoV(my_vec[j]).size();
			if (size_j < size_i || (size_i == size_j && size2_i > this->varsFromVtoP(my_vec[j]).size()))
			{
				swap(my_vec[i],my_vec[j]);
				break;
			}
			++j;
		}
		if (j == my_vec_size) ++i;
	}
}

void BlockCipher::sortVectorFromCtoV(vector<PackOfVars> & my_vec) const
{
	unsigned int i = 0;
	const unsigned int my_vec_size = my_vec.size();
	while (i < my_vec_size)
	{
		auto size_i = this->varsFromCtoV(my_vec[i]).size();
		auto size2_i = this->varsFromVtoC(my_vec[i]).size();
		unsigned int j = i + 1;
		while (j < my_vec_size)
		{
			auto size_j = this->varsFromCtoV(my_vec[j]).size();
			if (size_j < size_i || (size_j == size_i && size2_i > this->varsFromVtoC(my_vec[j]).size()))
			{
				swap(my_vec[i],my_vec[j]);
				break;
			}
			++j;
		}
		if (j == my_vec_size) ++i;
	}
}

void BlockCipher::sortVectorFromVtoC(vector<PackOfVars> & my_vec) const
{
	unsigned int i = 0;
	const unsigned int my_vec_size = my_vec.size();
	while (i < my_vec_size)
	{
		auto size_i = this->varsFromVtoC(my_vec[i]).size();
		unsigned int j = i + 1;
		while (j < my_vec_size)
		{
			if (this->varsFromVtoC(my_vec[j]).size() < size_i)
			{
				swap(my_vec[i],my_vec[j]);
				break;
			}
			++j;
		}
		if (j == my_vec_size) ++i;
	}
}

void BlockCipher::sortVectorFromVtoP(vector<PackOfVars> & my_vec) const
{
	unsigned int i = 0;
	const unsigned int my_vec_size = my_vec.size();
	while (i < my_vec_size)
	{
		auto size_i = this->varsFromVtoP(my_vec[i]).size();
		auto size2_i = this->varsFromPtoV(my_vec[i]).size();
		unsigned int j = my_vec_size - 1;
		while (j > i)
		{
			auto size_j = this->varsFromVtoP(my_vec[j]).size();
			auto size2_j = this->varsFromPtoV(my_vec[j]).size();
			if (size_j < size_i || (size_j == size_i && size2_j < size2_i))
			{
				swap(my_vec[i],my_vec[j]);
				size_i = size_j;
				size2_i = size2_j;
			}
			--j;
		}
		++i;
	}
}

#else

void BlockCipher::sortVectorFromPtoV(vector<PackOfVars> & my_vec) const
{
	map<unsigned int, list<PackOfVars>> my_map;
	for (auto & v : my_vec) my_map[this->varsFromPtoV(v).size()].emplace_back(move(v));
	unsigned int i = 0;
	for (auto & m : my_map)
	{
		auto & L = m.second;
		for (auto & v : L) my_vec[i++] = move(v);
	}
}

void BlockCipher::sortVectorFromCtoV(vector<PackOfVars> & my_vec) const
{
	map<unsigned int, list<PackOfVars>> my_map;
	for (auto & v : my_vec) my_map[this->varsFromCtoV(v).size()].emplace_back(move(v));
	unsigned int i = 0;
	for (auto & m : my_map)
	{
		auto & L = m.second;
		for (auto & v : L) my_vec[i++] = move(v);
	}
}

void BlockCipher::sortVectorFromVtoP(vector<PackOfVars> & my_vec) const
{
	map<unsigned int, list<PackOfVars>> my_map;
	for (auto & v : my_vec) my_map[this->varsFromVtoP(v).size()].emplace_back(move(v));
	unsigned int i = 0;
	for (auto & m : my_map)
	{
		auto & L = m.second;
		for (auto & v : L) my_vec[i++] = move(v);
	}
}

void BlockCipher::sortVectorFromVtoC(vector<PackOfVars> & my_vec) const
{
	map<unsigned int, list<PackOfVars>> my_map;
	for (auto & v : my_vec) my_map[this->varsFromVtoC(v).size()].emplace_back(move(v));
	unsigned int i = 0;
	for (auto & m : my_map)
	{
		auto & L = m.second;
		for (auto & v : L) my_vec[i++] = move(v);
	}
}

#endif



BlockCipher BlockCipher::reverse() const
{
	BlockCipher B = *this;
	swap(B.m_plaintext_vars,B.m_ciphertext_vars);
	swap(B.m_from_c_to_v,B.m_from_p_to_v);
	swap(B.m_from_v_to_c,B.m_from_v_to_p);
	swap(B.m_min_related_from_c,B.m_min_related_from_p);
	swap(B.m_guess_from_p,B.m_guess_from_c);
	swap(B.m_guess_to_c,B.m_guess_to_p);
	
	return B;
}

