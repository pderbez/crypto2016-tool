/*
 * Sbox.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_SBOX
#define DEF_SBOX

#include <string>
#include <iostream>
#include <initializer_list>
#include <vector>

class SofExpression;
class Expression;

class Sbox
{
	
	public:
		Sbox(const unsigned int nb_var);
		Sbox(std::string const& name, const unsigned int nb_var);
		Sbox(Sbox const&);
		Sbox& operator=(Sbox const&);
		
		Sbox(Sbox &&);
		Sbox& operator=(Sbox &&);
		
		// Member functions
		std::string getName() const {return m_name;};
		unsigned int getNbVariables() const {return m_nb_var;};
		
		template <typename ...ArgsT> SofExpression operator()(ArgsT ...Exprs) const;
		
	private:
		// Member variables
		std::string m_name;
		unsigned int m_nb_var;
		
		void print(std::ostream &flux) const {flux << m_name;};
		
		
	friend std::ostream& operator<<( std::ostream &flux, Sbox const& sb);
	
	//Relational Operators (based on both the name and the number of variables of the Sbox)
	friend bool operator<(Sbox const& sb1, Sbox const& sb2) {return (sb1.m_name < sb2.m_name || (sb1.m_name == sb2.m_name && sb1.m_nb_var < sb2.m_nb_var));};
	friend bool operator<=(Sbox const& sb1, Sbox const& sb2) {return (sb1.m_name < sb2.m_name || (sb1.m_name == sb2.m_name && sb1.m_nb_var <= sb2.m_nb_var));};
	friend bool operator>(Sbox const& sb1, Sbox const& sb2) {return (sb1.m_name > sb2.m_name || (sb1.m_name == sb2.m_name && sb1.m_nb_var > sb2.m_nb_var));};
	friend bool operator>=(Sbox const& sb1, Sbox const& sb2) {return (sb1.m_name > sb2.m_name || (sb1.m_name == sb2.m_name && sb1.m_nb_var >= sb2.m_nb_var));};
	friend bool operator==(Sbox const& sb1, Sbox const& sb2) {return (sb1.m_name == sb2.m_name && sb1.m_nb_var == sb2.m_nb_var);};
	friend bool operator!=(Sbox const& sb1, Sbox const& sb2) {return (sb1.m_name != sb2.m_name || sb1.m_nb_var != sb2.m_nb_var);};
		
};




#endif

