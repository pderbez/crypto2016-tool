/*
 * SubSystem.tpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <utility>

template <typename Iter> 
void SubSystem::setKnownVars(Iter const begin, Iter const end)
{
	this->setKnownVars(m_set_of_vars.getSubset(begin,end));
}

template <typename Iter>
SubSystem SubSystem::Extract(Iter const begin, Iter const end) const
{
	return this->Extract(m_set_of_vars.getSubset(begin,end));
}

template <typename Iter> 
int SubSystem::getNbSols(Iter const begin, Iter const end) const
{
	return this->getNbSols(m_set_of_vars.getSubset(begin,end));
}

template <typename T, typename U>
bool SubSystem::nb_sols_smaller_than_linear(const int bound_sols, T & wanted, T & maybe, std::vector<U> & my_vec, unsigned int my_vec_size, const int nb_sols) const
{
	SubSystem F = this->Extract(wanted + maybe);
	
	T remove = wanted;

start_removing:
	{
		const unsigned int my_vec_size_bak = my_vec_size;
		unsigned int i = my_vec_size;
		while (i > 0)
		{
			--i;
			if (F.isLinear(my_vec[i]))
			{
				remove += my_vec[i];
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
			}
		}
		if (my_vec_size < my_vec_size_bak)
		{
			maybe -= remove;
			F.KeepOnly(wanted + maybe);
			goto start_removing;
		}
	}
	
	return F.nb_sols_smaller_than_tmp(bound_sols, wanted, maybe, my_vec, my_vec_size, nb_sols);
}

template <typename T, typename U> 
bool SubSystem::nb_sols_smaller_than_tmp(const int bound_sols, T & wanted, T & maybe, std::vector<U> & my_vec, unsigned int my_vec_size, const int nb_sols) const
{
	if (my_vec_size == 0) return false;
	
	T wanted_copy = wanted;
	
	auto & x = my_vec[--my_vec_size];
	int nb_sols_w_x = this->getNbSols(wanted + x);
	
	{
		unsigned int i = 0;
		while (i < my_vec_size && nb_sols_w_x != nb_sols + 1)
		{
			auto tmp = this->getNbSols(wanted + my_vec[i]);
			if (tmp < nb_sols_w_x)
			{
				nb_sols_w_x = tmp;
				std::swap(x,my_vec[i]);
			}
			++i;
		}
	}
	
	wanted += x;
	

	{
		unsigned int i = my_vec_size;
		while (i > 0)
		{
			--i;
			auto tmp = wanted + my_vec[i];
			const int new_nb_sols = this->getNbSols(tmp);
			if (new_nb_sols <= nb_sols_w_x)
			{
				if (new_nb_sols < bound_sols) return true;
				nb_sols_w_x = new_nb_sols;
				wanted = std::move(tmp);
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
				i = my_vec_size;
			}
		}
	}
		
	maybe -= wanted;
	T maybe_copy = maybe;
		
	bool res = this->nb_sols_smaller_than_tmp(bound_sols,wanted, maybe, my_vec, my_vec_size, nb_sols_w_x);
	if (res == true || nb_sols_w_x <= nb_sols) return res;
	else return this->nb_sols_smaller_than_linear(bound_sols,wanted_copy, maybe_copy, my_vec, my_vec_size, nb_sols);
	
}



template <typename T> 
bool SubSystem::private_nb_sols_smaller_than(const int bound_sols, T & wanted, T & maybe) const
{
	int nb_sols_wanted = this->getNbSols(wanted);
	if (nb_sols_wanted < bound_sols) return true;
	
	maybe -= wanted;
	auto my_vec = maybe.get();
	unsigned int my_vec_size = my_vec.size();
	
	if (my_vec_size == 0) return false;
	
	SubSystem F = this->Extract(wanted + maybe);
	
	int new_bound_sols = bound_sols - nb_sols_wanted;
	
	int nb_sols = 0;
	F.setKnownVars(wanted);
	wanted.clear();
	
	{
		const unsigned int my_vec_size_bak = my_vec_size;
		unsigned int i = 0;
		while (i < my_vec_size)
		{
			auto tmp = wanted + my_vec[i];
			const int new_nb_sols = F.getNbSols(tmp);
			if (new_nb_sols <= nb_sols)
			{
				if (new_nb_sols < new_bound_sols) return true;
				nb_sols = new_nb_sols;
				wanted = std::move(tmp);
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
				i = 0;
			}
			else ++i;
		}
		if (my_vec_size_bak > my_vec_size)
		{
			F.setKnownVars(wanted);
			maybe -= wanted;
			wanted.clear();
			new_bound_sols -= nb_sols;
		}
	}
	
	T remove = wanted;
	
start_removing:
	{
		const unsigned int my_vec_size_bak = my_vec_size;
		unsigned int i = 0;
		while (i < my_vec_size)
		{
			if (F.isLinear(my_vec[i]))
			{
				remove += my_vec[i];
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
			}
			else ++i;
		}
		if (my_vec_size < my_vec_size_bak)
		{
			maybe -= remove;
			F.KeepOnly(maybe);
			goto start_removing;
		}
	}
	
	
	
	return F.nb_sols_smaller_than_tmp(new_bound_sols, wanted, maybe, my_vec, my_vec_size, 0);
}

template <typename T, typename U>
int SubSystem::nb_sols_linear(T & wanted, T & maybe, std::vector<U> & my_vec, unsigned int my_vec_size, const int nb_sols) const
{	
	SubSystem F = this->Extract(wanted + maybe);
	F.setKnownVars(wanted);
	wanted.clear();
	
	T remove = wanted;
	
start_removing:
	{
		const unsigned int my_vec_size_bak = my_vec_size;
		unsigned int i = my_vec_size;
		while (i > 0)
		{
			--i;
			if (F.isLinear(my_vec[i]))
			{
				remove += my_vec[i];
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
			}
		}
		if (my_vec_size < my_vec_size_bak)
		{
			maybe -= remove;
			F.KeepOnly(maybe);
			goto start_removing;
		}
	}
	
	return F.nb_sols_tmp(wanted, maybe, my_vec, my_vec_size, 0) + nb_sols;
}


template <typename T, typename U> 
int SubSystem::nb_sols_tmp(T & wanted, T & maybe, std::vector<U> & my_vec, unsigned int my_vec_size, const int nb_sols) const
{
	if (my_vec_size == 0) return nb_sols;
	
	T wanted_copy = wanted;
	
	auto & x = my_vec[--my_vec_size];
	int nb_sols_w_x = this->getNbSols(wanted + x);
	
	{
		unsigned int i = 0;
		while (i < my_vec_size && nb_sols_w_x != nb_sols + 1)
		{
			auto tmp = this->getNbSols(wanted + my_vec[i]);
			if (tmp < nb_sols_w_x)
			{
				nb_sols_w_x = tmp;
				std::swap(x,my_vec[i]);
			}
			++i;
		}
	}
	
	wanted += x;
	
	{
		unsigned int i = my_vec_size;
		while (i > 0)
		{
			--i;
			auto tmp = wanted + my_vec[i];
			const int new_nb_sols = this->getNbSols(tmp);
			if (new_nb_sols <= nb_sols_w_x)
			{
				nb_sols_w_x = new_nb_sols;
				wanted = std::move(tmp);
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
				i = my_vec_size;
			}
		}
	}
		
	maybe -= wanted;
	
	if (nb_sols_w_x > nb_sols)
	{
		T maybe_copy = maybe;
				
		const int nb_sols1 = this->nb_sols_tmp(wanted, maybe, my_vec, my_vec_size, nb_sols_w_x);	
		const int nb_sols2 = this->nb_sols_linear(wanted_copy, maybe_copy, my_vec, my_vec_size, nb_sols);
		
		return std::min(nb_sols1,nb_sols2);
	}
	else return this->nb_sols_tmp(wanted, maybe, my_vec, my_vec_size, nb_sols_w_x);	
}


template <typename T> 
int SubSystem::private_nb_sols(T & wanted, T & maybe) const
{	
	maybe -= wanted;
	auto my_vec = maybe.get();
	unsigned int my_vec_size = my_vec.size();
	
	if (my_vec_size == 0) return this->getNbSols(wanted);
	
	SubSystem F = this->Extract(wanted + maybe);
	
	int nb_sols_wanted = F.getNbSols(wanted);
	int nb_sols = 0;
	F.setKnownVars(wanted);
	wanted.clear();
	
	T remove = wanted;
	
start_removing:
	{
		const unsigned int my_vec_size_bak = my_vec_size;
		unsigned int i = 0;
		while (i < my_vec_size)
		{
			if (F.isLinear(my_vec[i]))
			{
				remove += my_vec[i];
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
			}
			else ++i;
		}
		if (my_vec_size < my_vec_size_bak)
		{
			maybe -= remove;
			F.KeepOnly(maybe);
			goto start_removing;
		}
	}
	
	const unsigned int my_vec_size_bak = my_vec_size;
	
	unsigned int i = 0;
	{
		while (i < my_vec_size)
		{
			auto tmp = wanted + my_vec[i];
			const int new_nb_sols = F.getNbSols(tmp);
			if (new_nb_sols <= nb_sols)
			{
				nb_sols = new_nb_sols;
				wanted = std::move(tmp);
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
				i = 0;
			}
			else
			{
				++i;
			}
		}
	}
	
	if (my_vec_size == 0) return nb_sols_wanted + nb_sols;
	
	if (my_vec_size_bak > my_vec_size)
	{
		F.setKnownVars(wanted);
		maybe -= wanted;
		wanted.clear();
	}
		
	return F.nb_sols_tmp(wanted, maybe, my_vec, my_vec_size, 0) + nb_sols_wanted + nb_sols;	
}

template <typename T> 
int SubSystem::private_nb_sols_original(T & wanted, T & maybe) const
{	
	maybe -= wanted;
	auto my_vec = maybe.get();
	unsigned int my_vec_size = my_vec.size();
	
	if (my_vec_size == 0) return this->getNbSols(wanted);
	
	SubSystem F = this->Extract(wanted + maybe);
	
	T remove = wanted;
	
start_removing:
	{
		const unsigned int my_vec_size_bak = my_vec_size;
		unsigned int i = 0;
		while (i < my_vec_size)
		{
			if (F.isLinear(my_vec[i]))
			{
				remove += my_vec[i];
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
			}
			else ++i;
		}
		if (my_vec_size < my_vec_size_bak)
		{
			maybe -= remove;
			F.KeepOnly(wanted + maybe);
			goto start_removing;
		}
	}
	
	//std::cout << "kept: " << maybe << std::endl;
	
	int nb_sols = F.getNbSols(wanted);
	
	unsigned int i = 0;
	{
		while (i < my_vec_size)
		{
			auto tmp = wanted + my_vec[i];
			const int new_nb_sols = F.getNbSols(tmp);
			//std::cout << " -- " << my_vec[i] << ": " << new_nb_sols << " <= " << nb_sols << "?" << std::endl;
			if (new_nb_sols <= nb_sols)
			{
				nb_sols = new_nb_sols;
				wanted = std::move(tmp);
				--my_vec_size;
				std::swap(my_vec[i],my_vec[my_vec_size]);
				i = 0;
			}
			else ++i;
		}
	}
	
	maybe -= wanted;

	return F.nb_sols_tmp(wanted, maybe, my_vec, my_vec_size, nb_sols);
	
}

