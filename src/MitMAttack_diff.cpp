/*
 * MitMAttack_diff.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "MitMAttack_diff.h"
#include "MitMAttack_core.h"
#include "BlockCipher.h"

std::set<SubsetOfPacks> MitMAttack_Diff::st_all_both;
std::set<SubsetOfPacks> MitMAttack_Diff::st_all_online;
std::set<SubsetOfPacks> MitMAttack_Diff::st_all_offline;
std::map<SubsetOfPacks const *,int> MitMAttack_Diff::st_all_time;

using namespace std;

MitMAttack_Diff::MitMAttack_Diff(MitMAttack_Diff const & A) : m_active_on(A.m_active_on), m_active_off(A.m_active_off), m_guess_on(A.m_guess_on), m_guess_off(A.m_guess_off),
m_null(A.m_null), m_both(A.m_both), m_dim_diff(A.m_dim_diff), m_data(A.m_data), m_B(A.m_B)
{
}


MitMAttack_Diff::MitMAttack_Diff(MitMAttack_Diff && A) : m_active_on(A.m_active_on), m_active_off(A.m_active_off), m_guess_on(A.m_guess_on), m_guess_off(A.m_guess_off),
m_null(move(A.m_null)), m_both(A.m_both), m_dim_diff(A.m_dim_diff), m_data(A.m_data), m_B(A.m_B)
{
}
		
MitMAttack_Diff & MitMAttack_Diff::operator=(MitMAttack_Diff const & A)
{
	m_active_on = A.m_active_on;
	m_active_off = A.m_active_off;
	m_guess_on = A.m_guess_on;
	m_guess_off = A.m_guess_off;
	m_null = A.m_null;
	m_both = A.m_both;
	m_dim_diff = A.m_dim_diff;
	m_data = A.m_data;
	m_B = A.m_B;
	return *this;
}


MitMAttack_Diff & MitMAttack_Diff::operator=(MitMAttack_Diff && A)
{
	m_active_on = A.m_active_on;
	m_active_off = A.m_active_off;
	m_guess_on = A.m_guess_on;
	m_guess_off = A.m_guess_off;
	m_null = move(A.m_null);
	m_both = A.m_both;
	m_dim_diff = A.m_dim_diff;
	m_data = A.m_data;
	m_B = A.m_B;
	return *this;
}
		
MitMAttack_Diff::~MitMAttack_Diff()
{
	
}

void MitMAttack_Diff::updateData()
{
	SubSystem const & E = m_B->getEqsKnownKey();
	const int nb_sols_wo_p = E.getNbSols(null());
	const int nb_sols_w_p = E.getNbSols(null() + (activeOn() - m_B->getStateVars()));
	m_data = nb_sols_w_p - nb_sols_wo_p;
}
		
MitMAttack_Diff::MitMAttack_Diff(MitMAttack_Core const & A) : m_null(A.t_vars[0]), m_B(A.m_B)
{	
	{
		auto pair7 = st_all_online.emplace(A.t_vars[7]);
		m_guess_on = addressof(*pair7.first);
		auto it = st_all_time.find(m_guess_on);
		if (it == st_all_time.end()) // online time is unknown
		{
			if (m_B->isSPN()) st_all_time[m_guess_on] = m_B->getEqsNonLinKey().getNbSols(A.t_vars[7], A.t_vars[1] + m_B->getNonLinKeyVars()) - m_B->getStatesize();
			else st_all_time[m_guess_on] = m_B->getEqsNonLinKey().getNbSols(A.t_vars[7], A.t_vars[0] + A.t_vars[1] + m_B->getNonLinKeyVars()) - m_B->getStatesize();
		}
	}
	
	{
		auto pair1 = st_all_online.emplace(A.t_vars[1]);
		m_active_on = addressof(*pair1.first);
	}
	
	{
		auto pair2 = st_all_offline.emplace(A.t_vars[2]);
		m_active_off = addressof(*pair2.first);
	}
	
	{
		auto pair8 = st_all_offline.emplace(A.t_vars[8]);
		m_guess_off = addressof(*pair8.first);
	}
	
	{
		auto pair5 = st_all_both.emplace(A.t_vars[5]);
		m_both = addressof(*pair5.first);
	}
	
	m_dim_diff = m_B->getEqsKnownKey().getNbSols(A.t_vars[0]);
	
	updateData();
}

int MitMAttack_Diff::timeOnline() const
{
	return st_all_time.at(m_guess_on);
}

void MitMAttack_Diff::setBlockCipher(BlockCipher const & B)
{
	m_B = addressof(B);
}


bool operator==(MitMAttack_Diff const & A1, MitMAttack_Diff const & A2)
{
	return (A1.m_active_on == A2.m_active_on && A1.m_active_off == A2.m_active_off && A1.m_both == A2.m_both);
}

bool operator<(MitMAttack_Diff const & A1, MitMAttack_Diff const & A2)
{
	if (A1.m_active_off == A2.m_active_off)
	{
		if (A1.m_active_on == A2.m_active_on) return (A1.m_both < A2.m_both);
		else return (A1.m_active_on < A2.m_active_on);
	}
	else return (A1.m_active_off < A2.m_active_off);
}

