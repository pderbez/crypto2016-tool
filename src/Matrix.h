/*
 * Matrix.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#ifndef DEF_MATRIX
#define DEF_MATRIX

#include <iostream>

template <typename T>
class Matrix
{
	public:
		Matrix();
		Matrix(unsigned int nb_lines, unsigned int nb_cols);
		Matrix(unsigned int nb_lines, unsigned int nb_cols, T value);
		Matrix(Matrix<T> const &);
		Matrix(Matrix<T> &&);
		
		Matrix<T>& operator=(Matrix<T> const &);
		Matrix<T>& operator=(Matrix<T> && mat);
		
		~Matrix();
		
		T const & operator()(const unsigned int line, const unsigned int column) const { return m_matrix[line][column]; };
		T & operator()(const unsigned int line, const unsigned int column) { return m_matrix[line][column]; };
		
		void swapLines(unsigned int, unsigned int);
		void swapColumns(unsigned int, unsigned int);
		
		unsigned int getNbLines() const { return m_nb_lines; };
		unsigned int getNbColumns() const { return m_nb_cols; };
		
		unsigned int rank() const;
		template <typename Iter> unsigned int rankColumns(Iter it, Iter end) const;
		template <typename Iter> unsigned int rankLines(Iter it, Iter end) const;
		
		unsigned int rank();
		template <typename Iter> unsigned int rankColumns(Iter it, Iter end);
		template <typename Iter> unsigned int rankLines(Iter it, Iter end);
		unsigned int rankColumns(unsigned int nb_cols); //rank of the first "nb_cols" columns
		
		bool rank_smaller_than(unsigned int r);
		
		unsigned int Gauss();
		unsigned int GaussJordan();
		
		void resize(unsigned int nb_lines, unsigned int nb_cols);
		
		template <typename T1> friend void swap(Matrix<T1> &, Matrix<T1> &);
		
		template <typename T1> friend std::ostream& operator<<( std::ostream &flux, Matrix<T1> const& mat);
	
	private:
		unsigned int m_nb_lines;
		unsigned int m_nb_cols;	
		T * m_matrix_space;
		T ** m_matrix;
	
};

#include "Matrix.tpp"

#endif
