/*
 * MitMAttack_eq.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "MitMAttack_eqs.h"
#include "MitMAttack_core.h"
#include "BlockCipher.h"

std::set<SubsetOfPacks> MitMAttack_Eqs::st_all_online;
std::set<SubsetOfPacks> MitMAttack_Eqs::st_all_offline;
std::map<SubsetOfPacks const *,int> MitMAttack_Eqs::st_all_time;

using namespace std;

void MitMAttack_Eqs::updateNbEqs()
{
	SubSystem const & E = m_B->getEqsKnownKey();
	const int s_P = E.getNbSols(activeOff());
	const int s = E.getNbSols(activeOn() + activeOff() + m_B->getPlaintextVars() + m_B->getCiphertextVars());
	m_nb_eqs = s_P + m_B->getStatesize() - s;
}

MitMAttack_Eqs::MitMAttack_Eqs(MitMAttack_Eqs const & A) : m_active_on(A.m_active_on), m_active_off(A.m_active_off), m_guess_on(A.m_guess_on), m_guess_off(A.m_guess_off),
m_null(A.m_null), m_nb_eqs(A.m_nb_eqs), m_B(A.m_B)
{
}


MitMAttack_Eqs::MitMAttack_Eqs(MitMAttack_Eqs && A) : m_active_on(A.m_active_on), m_active_off(A.m_active_off), m_guess_on(A.m_guess_on), m_guess_off(A.m_guess_off),
m_null(move(A.m_null)), m_nb_eqs(A.m_nb_eqs), m_B(A.m_B)
{
}
		
MitMAttack_Eqs & MitMAttack_Eqs::operator=(MitMAttack_Eqs const & A)
{
	m_active_on = A.m_active_on;
	m_active_off = A.m_active_off;
	m_guess_on = A.m_guess_on;
	m_guess_off = A.m_guess_off;
	m_null = A.m_null;
	m_nb_eqs = A.m_nb_eqs;
	m_B = A.m_B;
	return *this;
}


MitMAttack_Eqs & MitMAttack_Eqs::operator=(MitMAttack_Eqs && A)
{
	m_active_on = A.m_active_on;
	m_active_off = A.m_active_off;
	m_guess_on = A.m_guess_on;
	m_guess_off = A.m_guess_off;
	m_null = move(A.m_null);
	m_nb_eqs = A.m_nb_eqs;
	m_B = A.m_B;
	return *this;
}
		
MitMAttack_Eqs::~MitMAttack_Eqs()
{
	
}
		
MitMAttack_Eqs::MitMAttack_Eqs(MitMAttack_Core const & A) : m_null(A.t_vars[0]), m_nb_eqs(A.nbEqs()), m_B(A.m_B)
{
	{
		auto pair7 = st_all_online.emplace(A.t_vars[7]);
		m_guess_on = addressof(*pair7.first);
		auto it = st_all_time.find(m_guess_on);
		if (it == st_all_time.end()) // online time is unknown
		{
			st_all_time[m_guess_on] = m_B->getEqsNonLinKey().getNbSols(A.t_vars[7], A.t_vars[1] + m_B->getNonLinKeyVars()) - m_B->getStatesize();
		}
	}
	
	{
		auto pair1 = st_all_online.emplace(A.t_vars[1]);
		m_active_on = addressof(*pair1.first);
	}
	
	{
		auto pair2 = st_all_offline.emplace(A.t_vars[2]);
		m_active_off = addressof(*pair2.first);
	}
	
	{
		auto pair8 = st_all_offline.emplace(A.t_vars[8]);
		m_guess_off = addressof(*pair8.first);
	}
	
	/*cout << "new eqs: " << endl;
	cout << " - guess on (" << st_all_time[m_guess_on] << "): " << *m_guess_on << endl;
	cout << " - active on: " << *m_active_on << endl;
	getchar();*/
}


int MitMAttack_Eqs::timeOnline() const
{
	return st_all_time.at(m_guess_on);
}

void MitMAttack_Eqs::setBlockCipher(BlockCipher const & B)
{
	m_B = addressof(B);
}


bool operator==(MitMAttack_Eqs const & A1, MitMAttack_Eqs const & A2)
{
	return (A1.m_active_on == A2.m_active_on && A1.m_active_off == A2.m_active_off);
}

bool operator<(MitMAttack_Eqs const & A1, MitMAttack_Eqs const & A2)
{
	if (A1.m_active_off == A2.m_active_off) return (A1.m_active_on < A2.m_active_on);
	else return (A1.m_active_off < A2.m_active_off);
}
