/*
 * Sbox.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <iostream>
#include <sstream>
#include "Sbox.h"


using namespace std;

Sbox::Sbox(const unsigned int nb_var) : m_nb_var(nb_var)
{
	static unsigned long int cpt = 0;
    ostringstream oss;
    oss << "S_tmp_" << cpt;
    ++cpt;
    m_name = oss.str();
}

Sbox::Sbox(string const& name, const unsigned int nb_var) : m_name(name), m_nb_var(nb_var)
{
}

Sbox::Sbox(Sbox const& sb) : m_name(sb.m_name), m_nb_var(sb.m_nb_var)
{
}

Sbox& Sbox::operator=(Sbox const& sb)
{
	m_name = sb.m_name;
	m_nb_var = sb.m_nb_var;
	return *this;
}

Sbox::Sbox(Sbox && sb) : m_name(move(sb.m_name)), m_nb_var(sb.m_nb_var)
{
}

Sbox& Sbox::operator=(Sbox && sb)
{
	m_name = move(sb.m_name);
	m_nb_var = sb.m_nb_var;
	return *this;
}

ostream& operator<<( ostream &flux, Sbox const& sb)
{
    sb.print(flux);
    return flux;
}



