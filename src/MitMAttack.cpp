/*
 * MitMAttack.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "MitMAttack.h"
#include "BlockCipher.h"

using namespace std;

std::set<MitMAttack_Diff> MitMAttack::st_all_diff;
std::set<MitMAttack_Eqs> MitMAttack::st_all_eqs;
		
std::set<SubsetOfPacks> MitMAttack::st_all_online;
std::set<SubsetOfPacks> MitMAttack::st_all_offline;
		
std::map<SubsetOfPacks const *, int> MitMAttack::st_all_time_online;
std::map<SubsetOfPacks const *, int> MitMAttack::st_all_time_offline;



MitMAttack::MitMAttack(MitMAttack const & A) : m_diff(A.m_diff), m_eqs(A.m_eqs),
m_active_on(A.m_active_on), m_active_off(A.m_active_off), m_guess_on(A.m_guess_on), m_guess_off(A.m_guess_off),
m_B(A.m_B), m_cor_mem_flag(A.m_cor_mem_flag), m_cor_mem(A.m_cor_mem)
{
}

MitMAttack::MitMAttack(MitMAttack && A) : m_diff(A.m_diff), m_eqs(A.m_eqs),
m_active_on(A.m_active_on), m_active_off(A.m_active_off), m_guess_on(A.m_guess_on), m_guess_off(A.m_guess_off),
m_B(A.m_B), m_cor_mem_flag(A.m_cor_mem_flag), m_cor_mem(A.m_cor_mem)
{
}
		
MitMAttack const & MitMAttack::operator=(MitMAttack const & A)
{
	m_diff = A.m_diff;
	m_eqs = A.m_eqs;
	m_active_on = A.m_active_on;
	m_active_off = A.m_active_off;
	m_guess_on = A.m_guess_on;
	m_guess_off = A.m_guess_off;
	m_B = A.m_B;
	m_cor_mem_flag = A.m_cor_mem_flag;
	m_cor_mem = A.m_cor_mem;
	return *this;
}

MitMAttack const & MitMAttack::operator=(MitMAttack && A)
{
	m_diff = A.m_diff;
	m_eqs = A.m_eqs;
	m_active_on = A.m_active_on;
	m_active_off = A.m_active_off;
	m_guess_on = A.m_guess_on;
	m_guess_off = A.m_guess_off;
	m_B = A.m_B;
	m_cor_mem_flag = A.m_cor_mem_flag;
	m_cor_mem = A.m_cor_mem;
	return *this;
}
		
MitMAttack::~MitMAttack()
{
}
		
MitMAttack::MitMAttack(MitMAttack_Diff const & A_d, MitMAttack_Eqs const & A_e) : m_B(addressof(A_d.getBlockCipher()))
{
	assert(A_d.m_B == A_e.m_B);
	
	{
		auto pair = st_all_diff.emplace(A_d);
		m_diff = addressof(*pair.first);
	}
	
	{
		auto pair = st_all_eqs.emplace(A_e);
		m_eqs = addressof(*pair.first);
	}
	
	{
		auto pair = st_all_online.emplace(A_d.activeOn() + A_e.activeOn());
		m_active_on = addressof(*pair.first);
	}
	
	{
		auto pair = st_all_online.emplace(A_d.guessOn() + A_e.guessOn());
		m_guess_on = addressof(*pair.first);
		auto it = st_all_time_online.find(m_guess_on);
		if (it == st_all_time_online.end()) // online time is unknown
		{
			if (m_B->isSPN()) st_all_time_online[m_guess_on] = m_B->getEqsNonLinKey().getNbSols(*m_guess_on, *m_active_on + m_B->getNonLinKeyVars()) - 2*m_B->getStatesize();
			else st_all_time_online[m_guess_on] = m_B->getEqsNonLinKey().getNbSols(*m_guess_on, *m_active_on + A_d.null() + m_B->getNonLinKeyVars()) - 2*m_B->getStatesize();
		}
	}
	
	{
		auto pair = st_all_offline.emplace(A_d.activeOff() & A_e.activeOff());
		m_active_off = addressof(*pair.first);
	}
	
	{
		auto pair = st_all_offline.emplace(A_d.guessOff() & A_e.guessOff());
		m_guess_off = addressof(*pair.first);
		auto it = st_all_time_offline.find(m_guess_off);
		if (it == st_all_time_offline.end()) // offline time is unknown
		{
			if (m_B->isSPN()) st_all_time_offline[m_guess_off] = m_B->getEqsNonLinKey().getNbSols(*m_guess_off, *m_active_off + m_B->getNonLinKeyVars());
			else st_all_time_offline[m_guess_off] = m_B->getEqsNonLinKey().getNbSols(*m_guess_off, *m_active_off + A_d.null() + m_B->getNonLinKeyVars());
		}
	}
	
	m_cor_mem_flag = false;
	
	/*cout << "new att: " << endl;
	cout << " - guess on (" << st_all_time_online[m_guess_on] << "): " << *m_guess_on << endl;
	cout << " - active on: " << *m_active_on << endl;
	cout << endl;
	cout << " - guess on (" << A_d.timeOnline() << "): " << A_d.guessOn() << endl;
	cout << " - active on: " << A_d.activeOn() << endl;
	getchar();*/
}

int MitMAttack::timeOnline() const
{
	return st_all_time_online.at(m_guess_on);
}

int MitMAttack::timeOffline() const
{
	return st_all_time_offline.at(m_guess_off);
}

int MitMAttack::correctedMemory() const
{
	if (!m_cor_mem_flag)
	{
		m_cor_mem_flag = true;
		SubSystem const & E = m_B->getEqs();
		m_cor_mem = timeOffline() - (E.getNbSols(guessOn()) + E.getNbSols(guessOff()) - E.getNbSols(guessOn() + guessOff()));
	}
	return m_cor_mem;
}


SubsetOfPacks nullFromP(SubsetOfPacks const & s, BlockCipher const & B)
{
	SubsetOfPacks tmp = s;
	auto vec = s.get();
	for (auto const & p : vec) tmp += B.varsFromPtoV(p);
	return tmp - s;
}

SubsetOfPacks nullFromC(SubsetOfPacks const & s, BlockCipher const & B)
{
	SubsetOfPacks tmp = s;
	auto vec = s.get();
	for (auto const & p : vec) tmp += B.varsFromCtoV(p);
	return tmp - s;
}

#if 0

list<MitMAttack> findMitMAttacks_fromP_tmp(set<MitMAttack_Diff> & S_d, set<MitMAttack_Eqs> & S_e, const int bound_data, const int bound_time, const int bound_memory, BlockCipher const & B, const int bound_dim, const int bound_eqs)
{	
	set<SubsetOfPacks> set_memory_ok;
	set<SubsetOfPacks> set_time_ok;
	const unsigned int real_bound_time = static_cast<unsigned int>(bound_time) + 2*B.getStatesize();
	
	bool redo = true;
	
	unsigned int cpt_mem = 0;
	unsigned int cpt_time = 0;
	
	while (redo)
	{
		redo = false;
		{
			cout << "Phase 3-1";
			map<unsigned int, map<SubsetOfPacks, set<SubsetOfPacks>>> set_memory;
			for (auto const & A_d : S_d)
			{
				SubsetOfPacks null_off = nullFromC(A_d.offline(),B);
				for (auto const & A_e : S_e)
				{
					SubsetOfPacks tmp = A_d.offline() & A_e.offline();
					if (set_memory_ok.count(tmp) == 0)
					{
						const unsigned int size_tmp = tmp.getVars().size();
						if (size_tmp < static_cast<unsigned int>(bound_memory)) set_memory_ok.emplace(move(tmp));
						else set_memory[size_tmp][null_off & A_e.offline()].emplace(move(tmp));
					}
				}
			}
			SubSystem const & E = B.getEqsNonLinKey();
			unsigned int nb_free_guess = 2;
			while (!set_memory.empty())
			{
				++cpt_mem;
				unsigned int middle = set_memory.begin()->first + nb_free_guess;
				if (middle >= set_memory.rbegin()->first) middle = (set_memory.rbegin()->first + set_memory.begin()->first)/2;
				auto it = set_memory.lower_bound(middle);
				auto it_map = it->second.begin();
				auto it_s = it_map->second.begin();
				SubsetOfPacks tmp = *it_s;
				SubsetOfPacks null_off = it_map->first;
				bool shift = true;
				it_map->second.erase(it_s);
				if (it_map->second.empty())
				{
					it_map = it->second.erase(it_map);
					if (it->second.empty())
					{
						it = set_memory.erase(it);
						shift = false;
					}
				}
				if (E.nb_sols_smaller_than(bound_memory,tmp,B.getNonLinKeyVars() + null_off))
				{
					++nb_free_guess;
					auto it2 = set_memory.begin();
					while (it2 != it)
					{
						auto & my_map = it2->second;
						auto it2_map = my_map.begin();
						while (it2_map != my_map.end())
						{
							auto & my_set = it2_map->second;
							auto it2_s = my_set.begin();
							while (it2_s != my_set.end())
							{
								if (tmp.includes(*it2_s))
								{
									set_memory_ok.emplace(*it2_s);
									it2_s = my_set.erase(it2_s);
								}
								else ++it2_s;
							}
							if (my_set.empty()) it2_map = my_map.erase(it2_map);
							else ++it2_map;
						}
						if (my_map.empty()) it2 = set_memory.erase(it2);
						else ++it2;
					}
					set_memory_ok.emplace(move(tmp));
				}
				else
				{
					if (nb_free_guess > 0) --nb_free_guess;
					if (shift) ++it;
					while (it != set_memory.end())
					{
						auto & my_map = it->second;
						it_map = my_map.begin();
						while (it_map != my_map.end())
						{
							auto & my_set = it_map->second;
							it_s = my_set.begin();
							while (it_s != my_set.end())
							{
								if (it_s->includes(tmp))
								{
									it_s = my_set.erase(it_s);
								}
								else ++it_s;
							}
							if (my_set.empty()) it_map = my_map.erase(it_map);
							else ++it_map;
						}
						if (my_map.empty()) it = set_memory.erase(it);
						else ++it;
					}
				}
			}
			cout << "(" << cpt_mem << ")" << ": " << set_memory_ok.size() << endl;
		}
		
		{
			cout << "Phase 3-2";
			map<unsigned int, map<SubsetOfPacks, set<SubsetOfPacks>>> set_time;
			for (auto const & A_d : S_d)
			{
				SubsetOfPacks null_on = nullFromP(A_d.online(),B);
				for (auto const & A_e : S_e)
				{
					if (set_memory_ok.count(A_d.offline() & A_e.offline()) != 0)
					{
						SubsetOfPacks tmp = A_d.online() + A_e.online();
						if (set_time_ok.count(tmp) == 0)
						{
							//const unsigned int size_tmp = tmp.getVars().size();
							//if (size_tmp < real_bound_time) set_time_ok.emplace(move(tmp));
							//else set_time[size_tmp][null_on].emplace(move(tmp));
							const int time_tmp = A_d.timeOnline() + A_e.timeOnline();
							if (time_tmp < bound_time) set_time_ok.emplace(move(tmp));
							else set_time[time_tmp][null_on].emplace(move(tmp));
						}
					}
				}
			}
			SubSystem const & E = B.getEqsNonLinKey();
			unsigned int nb_free_guess = 2;
			while (!set_time.empty())
			{
				++cpt_time; 
				unsigned int middle = set_time.begin()->first + nb_free_guess;
				if (middle >= set_time.rbegin()->first) middle = (set_time.rbegin()->first + set_time.begin()->first)/2;
				auto it = set_time.lower_bound(middle);
				auto it_map = it->second.begin();
				auto it_s = it_map->second.begin();
				SubsetOfPacks tmp = *it_s;
				SubsetOfPacks null_on = it_map->first;
				bool shift = true;
				it_map->second.erase(it_s);
				if (it_map->second.empty())
				{
					it_map = it->second.erase(it_map);
					if (it->second.empty())
					{
						it = set_time.erase(it);
						shift = false;
					}
				}
				if (E.nb_sols_smaller_than(real_bound_time,tmp,B.getNonLinKeyVars() + null_on))
				{
					++nb_free_guess;
					auto it2 = set_time.begin();
					while (it2 != it)
					{
						auto & my_map = it2->second;
						auto it2_map = my_map.begin();
						while (it2_map != my_map.end())
						{
							auto & my_set = it2_map->second;
							auto it2_s = my_set.begin();
							while (it2_s != my_set.end())
							{
								if (tmp.includes(*it2_s))
								{
									set_time_ok.emplace(*it2_s);
									it2_s = my_set.erase(it2_s);
								}
								else ++it2_s;
							}
							if (my_set.empty()) it2_map = my_map.erase(it2_map);
							else ++it2_map;
						}
						if (my_map.empty()) it2 = set_time.erase(it2);
						else ++it2;
					}
					set_time_ok.emplace(move(tmp));
				}
				else
				{
					if (nb_free_guess > 0) --nb_free_guess;
					if (shift) ++it;
					while (it != set_time.end())
					{
						auto & my_map = it->second;
						it_map = my_map.begin();
						while (it_map != my_map.end())
						{
							auto & my_set = it_map->second;
							it_s = my_set.begin();
							while (it_s != my_set.end())
							{
								if (it_s->includes(tmp))
								{
									it_s = my_set.erase(it_s);
								}
								else ++it_s;
							}
							if (my_set.empty()) it_map = my_map.erase(it_map);
							else ++it_map;
						}
						if (my_map.empty()) it = set_time.erase(it);
						else ++it;
					}
				}
			}
			cout << "(" << cpt_time << ")" << ": " << set_time_ok.size() << endl;
		}
		
		{
			cout << "Phase 3-3" << endl;
			auto it_e = S_e.begin();
			while (it_e != S_e.end())
			{
				auto it_d = S_d.begin();
				auto const it_d_end = S_d.end();
				while (it_d != it_d_end && (set_memory_ok.count(it_d->offline() & it_e->offline()) == 0 || set_time_ok.count(it_d->online() + it_e->online()) == 0)) ++it_d;
				if (it_d == it_d_end) it_e = S_e.erase(it_e);
				else ++it_e;
			}
		}
		
		{
			cout << "Phase 3-4" << endl;
			auto it_d = S_d.begin();
			while (it_d != S_d.end())
			{
				auto it_e = S_e.begin();
				auto const it_e_end = S_e.end();
				while (it_e != it_e_end && (set_memory_ok.count(it_d->offline() & it_e->offline()) == 0 || set_time_ok.count(it_d->online() + it_e->online()) == 0)) ++it_e;
				if (it_e == it_e_end) it_d = S_d.erase(it_d);
				else ++it_d;
			}
		}
		
		
		{
			cout << "Phase 3-5" << endl;
			set<MitMAttack_Diff> S_tmp;
			auto it1 = S_d.begin();
			while (it1 != S_d.end())
			{
				if (it1->dimDiff() <= bound_dim) ++it1;
				else
				{
					S_tmp.emplace(*it1);
					it1 = S_d.erase(it1);
				}
			}
			if (!S_tmp.empty()) redo = true;
			for (it1 = S_tmp.begin(); it1 != S_tmp.end(); ++it1)
			{
				auto it2 = it1;
				for (++it2; it2 != S_tmp.end(); ++it2)
				{
					if (validCombination(*it1,*it2))
					{
						MitMAttack_Diff A_tmp = *it1 & *it2;
						if (A_tmp.timeOnline() < bound_time && A_tmp.data() < bound_data) S_d.emplace(move(A_tmp));
					}
				}
			}
		}
		
		{
			cout << "Phase 3-6" << endl;
			set<MitMAttack_Eqs> S_tmp;
			auto it1 = S_e.begin();
			while (it1 != S_e.end())
			{
				if (it1->nbEqs() >= bound_eqs) ++it1;
				else
				{
					S_tmp.emplace(*it1);
					it1 = S_e.erase(it1);
				}
			}
			if (!S_tmp.empty()) redo = true;
			for (auto it1 = S_tmp.begin(); it1 != S_tmp.end(); ++it1)
			{
				auto it2 = it1;
				for (++it2; it2 != S_tmp.end(); ++it2)
				{
					if (validCombination(*it1,*it2))
					{
						MitMAttack_Eqs A_tmp = *it1 & *it2;
						if (A_tmp.timeOnline() < bound_time) S_e.emplace(move(A_tmp));
					}
				}
			}
		}
	}
	
	list<MitMAttack> L;
	
	cout << "Phase 3-7" << endl;
	for (auto const & A_d : S_d)
	{
		for (auto const & A_e : S_e)
		{
			SubsetOfPacks tmp_t = A_d.online() + A_e.online();
			SubsetOfPacks tmp_m = A_d.offline() & A_e.offline();
			if (set_memory_ok.count(tmp_m) != 0 && set_time_ok.count(tmp_t) != 0) L.emplace_back(A_d,A_e);
		}
	}
	
	return L;
}

list<MitMAttack> findMitMAttacks_fromP_tmp_SPN(set<MitMAttack_Diff> & S_d, set<MitMAttack_Eqs> & S_e, const int bound_data, const int bound_time, const int bound_memory, BlockCipher const & B, const int bound_dim, const int bound_eqs)
{	
	set<SubsetOfPacks> set_memory_ok;
	set<SubsetOfPacks> set_time_ok;
	const unsigned int real_bound_time = static_cast<unsigned int>(bound_time) + 2*B.getStatesize();
	
	bool redo = true;
	
	while (redo)
	{
		redo = false;
		{
			cout << "Phase 3-1" << endl;
			map<unsigned int, set<SubsetOfPacks>> set_memory;	
			for (auto const & A_d : S_d)
			{
				for (auto const & A_e : S_e)
				{
					SubsetOfPacks tmp = A_d.offline() & A_e.offline();
					if (set_memory_ok.count(tmp) == 0)
					{
						const unsigned int size_tmp = tmp.getVars().size();
						if (size_tmp < static_cast<unsigned int>(bound_memory)) set_memory_ok.emplace(move(tmp));
						else set_memory[size_tmp].emplace(move(tmp));
					}
				}
			}
			SubSystem const & E = B.getEqsNonLinKey();
			unsigned int nb_free_guess = 2;
			while (!set_memory.empty())
			{
				unsigned int middle = set_memory.begin()->first + nb_free_guess;
				if (middle >= set_memory.rbegin()->first) middle = (set_memory.rbegin()->first + set_memory.begin()->first)/2;
				auto it = set_memory.lower_bound(middle);
				auto it_s = it->second.begin();
				SubsetOfPacks tmp = *it_s;
				it->second.erase(it_s);
				if (it->second.empty()) it = set_memory.erase(it);
				if (E.nb_sols_smaller_than(bound_memory,tmp,B.getNonLinKeyVars()))
				{
					++nb_free_guess;
					auto it2 = set_memory.begin();
					while (it2 != it)
					{
						auto & s = it2->second;
						it_s = s.begin();
						while (it_s != s.end())
						{
							if (tmp.includes(*it_s))
							{
								set_memory_ok.emplace(*it_s);
								it_s = s.erase(it_s);
							}
							else ++it_s;
						}
						if (s.empty()) it2 = set_memory.erase(it2);
						else ++it2;
					}
					set_memory_ok.emplace(move(tmp));
				}
				else
				{
					if (nb_free_guess > 0) --nb_free_guess;
					while (it != set_memory.end())
					{
						auto & s = it->second;
						it_s = s.begin();
						while (it_s != s.end())
						{
							if (it_s->includes(tmp)) it_s = s.erase(it_s);
							else ++it_s;
						}
						if (s.empty()) it = set_memory.erase(it);
						else ++it;
					}
				}
			}
		}
		
		{
			cout << "Phase 3-2" << endl;
			map<unsigned int, set<SubsetOfPacks>> set_time;
			for (auto const & A_d : S_d)
			{
				for (auto const & A_e : S_e)
				{
					if (set_memory_ok.count(A_d.offline() & A_e.offline()) != 0)
					{
						SubsetOfPacks tmp = A_d.online() + A_e.online();
						if (set_time_ok.count(tmp) == 0)
						{
							//const unsigned int size_tmp = tmp.getVars().size();
							//if (size_tmp < real_bound_time) set_time_ok.emplace(move(tmp));
							//else set_time[size_tmp].emplace(move(tmp));
							const unsigned int time_tmp = A_d.timeOnline() + A_e.timeOnline();
							if (time_tmp < static_cast<unsigned int>(bound_time)) set_time_ok.emplace(move(tmp));
							else set_time[time_tmp].emplace(move(tmp));
						}
					}
				}
			}
			SubSystem const & E = B.getEqsNonLinKey();
			unsigned int nb_free_guess = 2;
			while (!set_time.empty())
			{
				unsigned int middle = set_time.begin()->first + nb_free_guess;
				if (middle >= set_time.rbegin()->first) middle = (set_time.rbegin()->first + set_time.begin()->first)/2;
				auto it = set_time.lower_bound(middle);
				auto it_s = it->second.begin();
				SubsetOfPacks tmp = *it_s;
				it->second.erase(it_s);
				if (it->second.empty()) it = set_time.erase(it);
				if (E.nb_sols_smaller_than(real_bound_time,tmp,B.getNonLinKeyVars()))
				{
					++nb_free_guess;
					auto it2 = set_time.begin();
					while (it2 != it)
					{
						auto & s = it2->second;
						it_s = s.begin();
						while (it_s != s.end())
						{
							if (tmp.includes(*it_s))
							{
								set_time_ok.emplace(*it_s);
								it_s = s.erase(it_s);
							}
							else ++it_s;
						}
						if (s.empty()) it2 = set_time.erase(it2);
						else ++it2;
					}
					set_time_ok.emplace(move(tmp));
				}
				else
				{
					if (nb_free_guess > 0) --nb_free_guess;
					while (it != set_time.end())
					{
						auto & s = it->second;
						it_s = s.begin();
						while (it_s != s.end())
						{
							if (it_s->includes(tmp)) it_s = s.erase(it_s);
							else ++it_s;
						}
						if (s.empty()) it = set_time.erase(it);
						else ++it;
					}
				}
			}
			
			
		}
		
		
		{
			cout << "Phase 3-3" << endl;
			auto it_e = S_e.begin();
			while (it_e != S_e.end())
			{
				auto it_d = S_d.begin();
				auto const it_d_end = S_d.end();
				while (it_d != it_d_end && (set_memory_ok.count(it_d->offline() & it_e->offline()) == 0 || set_time_ok.count(it_d->online() + it_e->online()) == 0)) ++it_d;
				if (it_d == it_d_end) it_e = S_e.erase(it_e);
				else ++it_e;
			}
		}
		
		{
			cout << "Phase 3-4" << endl;
			auto it_d = S_d.begin();
			while (it_d != S_d.end())
			{
				auto it_e = S_e.begin();
				auto const it_e_end = S_e.end();
				while (it_e != it_e_end && (set_memory_ok.count(it_d->offline() & it_e->offline()) == 0 || set_time_ok.count(it_d->online() + it_e->online()) == 0)) ++it_e;
				if (it_e == it_e_end) it_d = S_d.erase(it_d);
				else ++it_d;
			}
		}
		
		
		{
			cout << "Phase 3-5" << endl;
			set<MitMAttack_Diff> S_tmp;
			auto it1 = S_d.begin();
			while (it1 != S_d.end())
			{
				if (it1->dimDiff() <= bound_dim) ++it1;
				else
				{
					S_tmp.emplace(*it1);
					it1 = S_d.erase(it1);
				}
			}
			if (!S_tmp.empty()) redo = true;
			for (it1 = S_tmp.begin(); it1 != S_tmp.end(); ++it1)
			{
				auto it2 = it1;
				for (++it2; it2 != S_tmp.end(); ++it2)
				{
					if (validCombination(*it1,*it2))
					{
						MitMAttack_Diff A_tmp = *it1 & *it2;
						if (A_tmp.timeOnline() < bound_time && A_tmp.data() < bound_data) S_d.emplace(move(A_tmp));
					}
				}
			}
		}
		
		{
			cout << "Phase 3-6" << endl;
			set<MitMAttack_Eqs> S_tmp;
			auto it1 = S_e.begin();
			while (it1 != S_e.end())
			{
				if (it1->nbEqs() >= bound_eqs) ++it1;
				else
				{
					S_tmp.emplace(*it1);
					it1 = S_e.erase(it1);
				}
			}
			if (!S_tmp.empty()) redo = true;
			for (auto it1 = S_tmp.begin(); it1 != S_tmp.end(); ++it1)
			{
				auto it2 = it1;
				for (++it2; it2 != S_tmp.end(); ++it2)
				{
					if (validCombination(*it1,*it2))
					{
						MitMAttack_Eqs A_tmp = *it1 & *it2;
						if (A_tmp.timeOnline() < bound_time) S_e.emplace(move(A_tmp));
					}
				}
			}
		}
	}
	
	list<MitMAttack> L;
	
	cout << "Phase 3-7" << endl;
	for (auto const & A_d : S_d)
	{
		for (auto const & A_e : S_e)
		{
			SubsetOfPacks tmp_t = A_d.online() + A_e.online();
			SubsetOfPacks tmp_m = A_d.offline() & A_e.offline();
			if (set_memory_ok.count(tmp_m) != 0 && set_time_ok.count(tmp_t) != 0) L.emplace_back(A_d,A_e);
		}
	}
	
	return L;	
}



list<MitMAttack> findMitMAttacks_fromP(const int bound_data, const int bound_time, const int bound_memory, BlockCipher const & B, const int bound_dim, const int bound_eqs)
{	
	list<MitMAttack_Eqs> L_e = findMitMAttacks_Eqs_fromC(bound_data,bound_time,bound_memory,bound_eqs,B);
	set<MitMAttack_Eqs> S_e (L_e.begin(), L_e.end());
	L_e.clear();
	
	list<MitMAttack_Diff> L_d = findMitMAttacks_Diff_fromP(bound_data,bound_time,bound_memory,bound_dim,B);
	set<MitMAttack_Diff> S_d (L_d.begin(), L_d.end());
	L_d.clear();
	
	list<MitMAttack> L;
	if (B.isSPN()) L = findMitMAttacks_fromP_tmp_SPN(S_d,S_e,bound_data, bound_time, bound_memory, B, bound_dim, bound_eqs);
	else L = findMitMAttacks_fromP_tmp(S_d,S_e,bound_data, bound_time, bound_memory, B, bound_dim, bound_eqs);
	
	list<MitMAttack> L_best;
	cout << "Phase 3-8" << endl;
	unsigned int cpt = 0;
	for (auto const & A : L)
	{
		if (A.timeOnline() == 66 && A.timeOffline() == 96 && A.data() == 32) ++cpt;
		bool best = true;
		auto it = L_best.begin();
		while (it != L_best.end())
		{
			if (it->timeOnline() <= A.timeOnline() && it->data() <= A.data() && (it->correctedMemory() < A.correctedMemory() || (it->correctedMemory() == A.correctedMemory() && it->timeOffline() <= A.timeOffline())))
			{
				best = false;
				break;
			}
			if (it->timeOnline() >= A.timeOnline() && it->data() >= A.data() && (it->correctedMemory() > A.correctedMemory() || (it->correctedMemory() == A.correctedMemory() && it->timeOffline() >= A.timeOffline()))) it = L.erase(it);
			else ++it;
		}
		if (best) L_best.emplace_back(A);
	}
	
	for (auto const & A : L_best)
	{
		cout << "Best Attack: " << endl;
		cout << " - data: " << A.data() << endl;
		cout << " - time: " << A.timeOnline() << endl;
		cout << " - memory: " << A.timeOffline() << " (corrected: " << A.correctedMemory() << ")" << endl;
		cout << " - dim: " << A.dimDiff() << endl;
		cout << " - nb eqs: " << A.nbEqs() << endl;
		cout << " - online: " << A.online() << endl;
		cout << " - offline: " << A.offline() << endl;
		cout << " - null: " << A.Diff().null() << endl;
		cout << endl;
	}
	cout << cpt;
	return L;
}

#else

list<MitMAttack> findMitMAttacks_fromP(const int bound_data, const int bound_time, const int bound_memory, BlockCipher const & B, const int bound_dim, const int bound_eqs)
{	
	list<MitMAttack_Eqs> L_e = findMitMAttacks_Eqs_fromC(bound_data,bound_time,bound_memory,bound_eqs,B);
	set<MitMAttack_Eqs> S_e (L_e.begin(), L_e.end());
	L_e.clear();
	
	list<MitMAttack_Diff> L_d = findMitMAttacks_Diff_fromP(bound_data,bound_time,bound_memory,bound_dim,B);
	set<MitMAttack_Diff> S_d (L_d.begin(), L_d.end());
	L_d.clear();
	
	list<MitMAttack> L;
	unsigned int cpt = 0;
	for (auto const & A_d : S_d)
	{
		cout << "\r" << "Merging: " << ++cpt << "/" << S_d.size() << flush;	
		for (auto const & A_e : S_e)
		{
			MitMAttack A (A_d,A_e);
			if (A.timeOnline() < bound_time && A.timeOffline() < bound_memory) L.emplace_back(move(A));
		}
	}
	cout << endl << endl;
	
	list<MitMAttack> L_best;
	for (auto const & A : L)
	{
		bool best = true;
		auto it = L_best.begin();
		while (it != L_best.end())
		{
			if (it->timeOnline() <= A.timeOnline() && it->data() <= A.data() && (it->correctedMemory() < A.correctedMemory() || (it->correctedMemory() == A.correctedMemory() && it->timeOffline() <= A.timeOffline())))
			{
				best = false;
				break;
			}
			if (it->timeOnline() >= A.timeOnline() && it->data() >= A.data() && (it->correctedMemory() > A.correctedMemory() || (it->correctedMemory() == A.correctedMemory() && it->timeOffline() >= A.timeOffline()))) it = L.erase(it);
			else ++it;
		}
		if (best) L_best.emplace_back(A);
	}
	
	for (auto const & A : L_best)
	{
		cout << "Best Attack: " << endl;
		cout << " - data: " << A.data() << endl;
		cout << " - time: " << A.timeOnline() << endl;
		cout << " - memory: " << A.timeOffline() << " (corrected: " << A.correctedMemory() << ")" << endl;
		cout << " - dim: " << A.dimDiff() << endl;
		cout << " - nb eqs: " << A.nbEqs() << endl;
		cout << " - online: " << A.activeOn() << endl;
		cout << " - offline: " << A.activeOff() << endl;
		cout << " - null: " << A.Diff().null() << endl;
		cout << endl;
	}
	cout << cpt;
	return L;
}

#endif

list<MitMAttack> findMitMAttacks_fromC(const int bound_data, const int bound_time, const int bound_memory, BlockCipher const & B, const int bound_dim, const int bound_eqs)
{
	BlockCipher B_reverse = B.reverse(); //switch plaintext and ciphertext
	list<MitMAttack> L = findMitMAttacks_fromP(bound_data, bound_time, bound_memory, B_reverse, bound_dim, bound_eqs);
	for (auto & A : L) A.setBlockCipher(B);
	return L;
}

void MitMAttack::setBlockCipher(BlockCipher const & B)
{
	m_B = addressof(B);
}




