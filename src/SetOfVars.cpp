/*
 * SetOfVars.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "SetOfVars.h"

using namespace std;

/*

SubsetOfVars_s::SubsetOfVars_s() : SubsetOfVars(), m_size(0)
{
}

SubsetOfVars_s::SubsetOfVars_s(SubsetOfVars_s const & s) : SubsetOfVars(s), m_size(s.m_size)
{
}

SubsetOfVars_s::SubsetOfVars_s(SubsetOfVars_s && s) : SubsetOfVars(move(s)), m_size(s.m_size)
{
}
		
SubsetOfVars_s & SubsetOfVars_s::operator=(SubsetOfVars_s const & s)
{
	m_size = s.m_size;
	SubsetOfVars::operator=(s);
	return *this;
}

SubsetOfVars_s & SubsetOfVars_s::operator=(SubsetOfVars_s && s)
{
	m_size = s.m_size;
	SubsetOfVars::operator=(move(s));
	return *this;
}
		
SubsetOfVars_s::SubsetOfVars_s(SubsetOfVars const & s) : SubsetOfVars(s), m_size(s.size())
{
}

SubsetOfVars_s::SubsetOfVars_s(SubsetOfVars && s) : SubsetOfVars(move(s)), m_size(SubsetOfVars::size())
{
}
	
SubsetOfVars_s & SubsetOfVars_s::operator=(SubsetOfVars const & s)
{
	m_size = s.size();
	SubsetOfVars::operator=(s);
	return *this;
}

SubsetOfVars_s & SubsetOfVars_s::operator=(SubsetOfVars && s)
{
	m_size = s.size();
	SubsetOfVars::operator=(move(s));
	return *this;
}
		
SubsetOfVars_s::~SubsetOfVars_s()
{
}

SubsetOfVars_s & SubsetOfVars_s::operator+=(SubsetOfVars const & s) // union
{
	SubsetOfVars::operator+=(s);
	m_size = SubsetOfVars::size();
	return *this;
}

SubsetOfVars_s & SubsetOfVars_s::operator-=(SubsetOfVars const & s) // difference
{
	SubsetOfVars::operator-=(s);
	m_size = SubsetOfVars::size();
	return *this;
}

SubsetOfVars_s & SubsetOfVars_s::operator&=(SubsetOfVars const & s) // intersection
{
	SubsetOfVars::operator&=(s);
	m_size = SubsetOfVars::size();
	return *this;
}

		
SubsetOfVars_s & SubsetOfVars_s::operator+=(Variable const & v)
{
	SubsetOfVars::operator+=(v);
	m_size = SubsetOfVars::size();
	return *this;
}

SubsetOfVars_s & SubsetOfVars_s::operator-=(Variable const & v)
{
	SubsetOfVars::operator-=(v);
	m_size = SubsetOfVars::size();
	return *this;
}

SubsetOfVars_s & SubsetOfVars_s::operator&=(Variable const & v)
{
	SubsetOfVars::operator&=(v);
	m_size = SubsetOfVars::size();
	return *this;
}

		
void SubsetOfVars_s::clear()
{
	SubsetOfVars::clear();
	m_size = 0;
}

*/
