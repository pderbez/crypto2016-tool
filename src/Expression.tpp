/*
 * Expression.tpp
 * 
 * Copyright 2014 pderbez <pderbez@pderbez-VirtualBox>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "SofExpression.h"

template <typename Iter> 
typename std::enable_if<std::is_same<Expression, typename Iter::value_type>::value,std::list<Expression>>::type normalize(Iter begin, const Iter end)
{
	std::list<Expression> L;
	
	{
		auto it = begin;
		while (it != end)
		{
			L.push_back(*it);
			++it;
		}
	}
	
	auto itL = L.begin();
	while (itL != L.end())
	{
		auto it_map = itL->m_nonlin.begin();
		while (it_map != itL->m_nonlin.end())
		{
			SofExpression const &tmp_cst = it_map->first;
			if (!tmp_cst.isMonomial())
			{
				GFElement coef = it_map->second;
				SofExpression tmp = tmp_cst;
				for (auto it_expr = tmp.begin(); it_expr != tmp.end(); ++it_expr)
				{
					if (!it_expr->isVariable())
					{
						Variable v;
						Expression new_expr = v;
						std::swap(new_expr,*it_expr);
						new_expr += v;
						L.push_back(std::move(new_expr));
					}
				}
				itL->m_nonlin.erase(it_map);
				itL->m_nonlin[tmp] = coef;
				it_map = itL->m_nonlin.begin();
			}
			else ++it_map;
		}
		++itL;
	}
	
	return L;
}

