/*
 * Variable.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <sstream>
#include <cassert>
#include <utility>

#include "Variable.h"

using namespace std;

vector<Variable::MyPair> Variable::variables_name; //names associated to variables
vector<unsigned int> Variable::empty_case; //free cases of variables_name;


Variable::Variable()
{
	static unsigned long int cpt = 0;
    ostringstream oss;
    oss << "_tmp_" << cpt;
    ++cpt;
    string name = oss.str();
	const unsigned int nb_empty_cases = Variable::empty_case.size();
	if (nb_empty_cases != 0) // if there is a free case
	{
		m_num = Variable::empty_case[nb_empty_cases-1];
		Variable::empty_case.pop_back();
		Variable::variables_name[m_num].first = name;
		Variable::variables_name[m_num].second = 1;
	}
	else
	{
		m_num = Variable::variables_name.size();
		Variable::variables_name.push_back(make_pair(name,1));
	}
}

Variable::Variable(string const& name)
{
	if (name[0] == '_')
	{
		cout << "Error in Variable construction: a variable name can not begin by '_'" << endl;
		exit(EXIT_FAILURE);
	}
	
	const unsigned int bound = Variable::variables_name.size();
	for (unsigned int i = 0; i < bound; ++i)
	{
		if (Variable::variables_name[i].second > 0 && name == Variable::variables_name[i].first)
		{
			m_num = i;
			++Variable::variables_name[i].second;
			goto end_function;
		}
	}
	
	{
		const unsigned int nb_empty_cases = Variable::empty_case.size();
		if (nb_empty_cases != 0) // if there is a free case
		{
			m_num = Variable::empty_case[nb_empty_cases-1];
			Variable::empty_case.pop_back();
			Variable::variables_name[m_num].first = name;
			Variable::variables_name[m_num].second = 1;
		}
		else
		{
			m_num = Variable::variables_name.size();
			Variable::variables_name.push_back(make_pair(name,1));
		}
	}
	
end_function:
	{
	}
}

Variable::Variable(Variable const& var)
{
	m_num = var.m_num;
	++Variable::variables_name[m_num].second;
}

Variable::~Variable()
{
	assert(Variable::variables_name[m_num].second > 0);
	--variables_name[m_num].second;
	if (Variable::variables_name[m_num].second == 0)
	{
		Variable::empty_case.push_back(m_num);
	}
}

Variable& Variable::operator=(Variable const& var)
{
	++Variable::variables_name[var.m_num].second;
	--Variable::variables_name[m_num].second;
	if (Variable::variables_name[m_num].second == 0) Variable::empty_case.push_back(m_num);
	m_num = var.m_num;
	return *this;
}

Variable & Variable::operator=(Variable && var)
{
	swap(m_num,var.m_num);
	return *this;
}

Variable& Variable::operator=(string const& name)
{
	*this = Variable(name);
	return *this;
}

string const& Variable::getName() const
{
	return Variable::variables_name[m_num].first;
}

void Variable::print(ostream &flux) const
{
	flux << Variable::variables_name[m_num].first;
}

ostream& operator<<( ostream &flux, Variable const& var)
{
    var.print(flux);
    return flux;
}

int Variable::priority() const
{
	string const& name = this->getName();
	switch (name[0])
	{
		case '_':
			return -1;
		case 'K':
			return 1;
		case 'P':
			return 2;
		case 'C':
			return 3;
		default:
			return 0;
	}
}

void Variable::printAllRegisteredVars()
{
	cout << "Variables: ";
	for (auto const & x : variables_name)
	{
		if (x.second > 0) cout << "(" << x.first << "," << x.second << ")" << ' ';
	}
	cout << endl;
}
