/*
 * SystemOfEquations.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "SystemOfEquations.h"
#include <algorithm>
#include <cassert>

using namespace std;

SystemOfEquations::~SystemOfEquations()
{
}

bool SystemOfEquations::simplify_tmp()
{	
	vector<set<Variable>> vars_by_col;
	vector<vector<Variable>> all_perm;
	
	unsigned int nb_linear_vars = 0;
	
	// Looking at involved variables in each terms
	for (auto it = m_expr_by_col.begin();  it != m_expr_by_col.end() ; ++it)
	{
		vector<Variable> v;
		if (it->isVariable())
		{
			Variable const & my_var = it->lin_cbegin()->first;
			if (!this->isLinear(my_var)) v.emplace_back(my_var);
			else
			{
				const unsigned int c = vars_by_col.size();
				vars_by_col.emplace_back(v.begin(),v.end());
				swap(vars_by_col[nb_linear_vars], *vars_by_col.rbegin());
				this->swapColumns(nb_linear_vars,c);
				++nb_linear_vars;
				continue; // don't add this variable to "all_perm"
			}
		}
		else
		{
			if (!it->isConstant())
			{
				SofExpression const& sexpr = it->nonlin_cbegin()->first;
				for (auto it_expr = sexpr.begin(); it_expr != sexpr.end(); ++it_expr) v.emplace_back(it_expr->lin_cbegin()->first);
			}
		}
		
		vars_by_col.emplace_back(v.begin(),v.end());
		
		auto it_all_perm = all_perm.begin();
		const auto it_end = all_perm.end();
		while (it_all_perm != it_end)
		{
			if (v == *it_all_perm) break;
			else ++it_all_perm;
		}
		if (it_all_perm == it_end) all_perm.emplace_back(move(v));
	}
	
	const unsigned int nb_cols_start = nb_linear_vars;
	const unsigned int nb_lines_start = this->m_matrix.rankColumns(nb_cols_start);
	
	// Allocating a temporary matrix big enough
	
	const unsigned int nb_cols = this->m_matrix.getNbColumns();
	const unsigned int nb_lines = this->m_matrix.getNbLines();	
	
	Matrix<GFElement> mat(nb_lines,nb_cols);
	
	
	unsigned int nb_cols_end;
	{
		unsigned int pivot_l = nb_lines_start;
		unsigned int pivot_c = nb_cols_start;
		while (pivot_l < nb_lines && pivot_c < nb_cols)
		{
			unsigned int pivot_c_tmp = pivot_c;
			while (pivot_c_tmp < nb_cols)
			{
				unsigned int pivot_l_tmp = pivot_l;
				while (pivot_l_tmp < nb_lines && m_matrix(pivot_l_tmp,pivot_c_tmp) == 0) ++pivot_l_tmp;
				if (pivot_l_tmp < nb_lines)
				{
					m_matrix.swapLines(pivot_l,pivot_l_tmp);
					swap(vars_by_col[pivot_c_tmp], vars_by_col[pivot_c]);
					this->swapColumns(pivot_c_tmp,pivot_c);
					break;
				}
				else ++pivot_c_tmp;
			}
			if (m_matrix(pivot_l,pivot_c) != 0)
			{
				const GFElement coef_inv = m_matrix(pivot_l,pivot_c).getInverse();
				for (unsigned int l = nb_lines_start; l < pivot_l; l++)
				{
					if (m_matrix(l,pivot_c) != 0)
					{
						const GFElement coef = coef_inv * m_matrix(l,pivot_c);
						for (unsigned int c = nb_cols_start; c < nb_cols; c++) m_matrix(l,c) += coef*m_matrix(pivot_l,c);
					}
				}
				for (unsigned int l = pivot_l+1; l < nb_lines; l++)
				{
					if (m_matrix(l,pivot_c) != 0)
					{
						const GFElement coef = coef_inv * m_matrix(l,pivot_c);
						for (unsigned int c = nb_cols_start; c < nb_cols; c++) m_matrix(l,c) += coef*m_matrix(pivot_l,c);
					}
				}
				++pivot_l;
				++pivot_c;
			}
			else break;
		}
		nb_cols_end = pivot_c;
	}
	
	
	vector<set<Variable>> all_vars_equal;
	
	map<Variable, set<unsigned int>> cols_by_var;
	for (unsigned int c = nb_cols_start; c < nb_cols; ++c)
	{
		unsigned int l = nb_lines_start;
		while (l < nb_lines && m_matrix(l,c) == 0) ++l;
		if (l < nb_lines)
		{
			auto const & s = vars_by_col[c];
			for (auto const & v : s) cols_by_var[v].insert(c);
		}
	}
	
	// Checking all possible equalities
	const unsigned int nb_all_perm = all_perm.size();
	for (unsigned int i = 0; i < nb_all_perm; ++i)
	{
		vector<Variable> const &v1 = all_perm[i];		
		
		cout << "\r" << "Looking for fake equations: " << i << '/' << nb_all_perm << " - " << flush;
		
		set<Variable> s1(v1.begin(),v1.end());
		for (unsigned int j = i+1; j < nb_all_perm; ++j)
		{
			vector<Variable> const &v2 = all_perm[j];
			if (v2.size() != v1.size()) continue;
			const unsigned int vec_size = v1.size(); // == v2.size()
			
			// v1 = [x1,y1,...] v2 = [x2,y2,..] -> check if x1 = x2 and y1 = y2 and ...
			
			set<Variable> s2(v2.begin(),v2.end());
			
			set<Variable> s12 = s2;
			s12.insert(s1.begin(),s1.end());
			
			set<unsigned int> involved_cols;
			for (auto const & v : s12)
			{
				auto const & s = cols_by_var[v];
				involved_cols.insert(s.begin(),s.end());
			}
			
			vector<set<Variable>> vars_equal; // [ {x1,x2}, {y1,y2} , ... ]
			
			for (unsigned int k = 0; k < vec_size; ++k) // for instance if x1 = y2 (they are the same variable) then "vars_equal" := [ {x1,x2,y1}, ... ]
			{
				unsigned int l = 0;
				for (; l < vars_equal.size(); ++l)
				{
					if (vars_equal[l].count(v1[k]) != 0)
					{
						vars_equal[l].insert(v2[k]);
						break;
					}
					else
					{
						if (vars_equal[l].count(v2[k]) != 0)
						{
							vars_equal[l].insert(v1[k]);
							break;
						}
					}
				}
				if (l != vars_equal.size()) continue;
				set<Variable> s_tmp;
				s_tmp.insert(v1[k]);
				s_tmp.insert(v2[k]);
				vars_equal.emplace_back(move(s_tmp));
			}
			
			// compute the minimal number of equations needed to have [x1,y1,...] = [x2,y2,..]
			unsigned int nb_eq = 0;
			for (unsigned int l = 0; l < vars_equal.size(); ++l) nb_eq += vars_equal[l].size() - 1;
			
			unsigned int max_eq = 0;
			auto it_cols = involved_cols.begin();
			while (it_cols != involved_cols.end() && *it_cols < nb_cols_end)
			{
				++max_eq;
				++it_cols;
			}
			
			if (max_eq >= nb_eq)
			{
				Matrix<GFElement> mat_final(max_eq,max_eq + nb_cols - nb_cols_end);
				unsigned int pos = 0;
				for (auto it_c = involved_cols.begin(); it_c != it_cols ; ++it_c)
				{
					for (unsigned int c = nb_cols_end; c < nb_cols; ++c)
					{
						mat_final(pos,max_eq + c - nb_cols_end) = m_matrix(nb_lines_start + *it_c - nb_cols_start, c);
					}
					for (unsigned int c = 0; c < max_eq; ++c) mat_final(pos,c) = 0;
					mat_final(pos,pos) = 1;
					++pos;
				}
				const unsigned int nb_cols_mat_final = mat_final.getNbColumns();
				unsigned int pivot = 0;
				for (unsigned int c = max_eq; c < nb_cols_mat_final; ++c)
				{
					if (involved_cols.count(c + nb_cols_end - max_eq) != 0) continue;
					unsigned int pivot_tmp = pivot;
					while (pivot_tmp < max_eq)
					{
						const GFElement coef_pivot = mat_final(pivot_tmp,c);
						if (coef_pivot != 0)
						{
							if (pivot + nb_eq >= max_eq) goto not_equal;
							const GFElement coef_inv = coef_pivot.getInverse();
							for (unsigned int l = pivot_tmp+1; l < max_eq; ++l)
							{
								const GFElement coef_line = mat_final(l,c);
								if (coef_line != 0)
								{
									const GFElement coef = coef_line * coef_inv;
									for (unsigned int k = 0; k < nb_cols_mat_final; ++k) mat_final(l,k) += coef * mat_final(pivot_tmp,k);
								}
							}
							mat_final.swapLines(pivot,pivot_tmp);
							++pivot;
							break;
						}
						else ++pivot_tmp;
					}
				}
				
				{
					vector<Expression> expr_by_col;
					unsigned int new_nb_cols = 0;
					const unsigned int new_nb_lines = max_eq - pivot;
					pos = 0;
					for (auto const c : involved_cols) 
					{
						Expression expr = m_expr_by_col[c];
						for (unsigned int k = 0; k < vars_equal.size(); ++k)
						{
							auto it_x = vars_equal[k].begin();
							const Variable x = *it_x;
							for (++it_x; it_x != vars_equal[k].end(); ++it_x) expr.replace(*it_x,x);
						}

						unsigned int k = 0;
						for (; k < new_nb_cols; ++k)
						{
							if (expr == expr_by_col[k])
							{
								if (c < nb_cols_end)
								{
									for (unsigned int l = 0; l < new_nb_lines; ++l) mat(l,k) += mat_final(l+pivot,pos);
								}
								else
								{
									for (unsigned int l = 0; l < new_nb_lines; ++l) mat(l,k) += mat_final(l+pivot,c - nb_cols_end + max_eq);
								}
								break;
							}
						}
						if (k == new_nb_cols)
						{
							if (c < nb_cols_end)
							{
								for (unsigned int l = 0; l < new_nb_lines; ++l) mat(l,k) = mat_final(l+pivot,pos);
							}
							else
							{
								for (unsigned int l = 0; l < new_nb_lines; ++l) mat(l,k) = mat_final(l+pivot,c - nb_cols_end + max_eq);
							}
							++new_nb_cols;
							expr_by_col.emplace_back(move(expr));
						}
						++pos;
					}
					
					const unsigned int final_nb_cols = new_nb_cols;
					pivot = 0;
					for (unsigned int c = 0; c < final_nb_cols; ++c)
					{
						unsigned int pivot_tmp = pivot;
						while (pivot_tmp < new_nb_lines)
						{
							const GFElement coef_pivot = mat(pivot_tmp,c);
							if (coef_pivot != 0)
							{
								if (pivot + nb_eq >= new_nb_lines) goto not_equal;
								const GFElement coef_inv = coef_pivot.getInverse();
								for (unsigned int l = pivot_tmp+1; l < new_nb_lines; ++l)
								{
									const GFElement coef_line = mat(l,c);
									if (coef_line != 0)
									{
										const GFElement coef = coef_line * coef_inv;
										for (unsigned int k = c+1; k < final_nb_cols; ++k) mat(l,k) += coef * mat(pivot_tmp,k);
									}
								}
								mat.swapLines(pivot,pivot_tmp);
								++pivot;
								break;
							}
							else ++pivot_tmp;
						}
					}
					for (unsigned int l = 0; l < vars_equal.size(); ++l) all_vars_equal.emplace_back(move(vars_equal[l]));	
				}
not_equal:
				{
				}
			}
		}
	}
	
	{
		unsigned int i = 0;
		while (i < all_vars_equal.size())
		{
			set<Variable> & s1 = all_vars_equal[i];
			const unsigned int loop_bound = all_vars_equal.size();
			for (unsigned int j = i+1; j < loop_bound; ++j)
			{
				set<Variable> & s2 = all_vars_equal[j];
				set<Variable> s_tmp;
				set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), inserter(s_tmp,s_tmp.begin()));
				if (s_tmp.size() < s1.size() + s2.size())
				{
					s1 = move(s_tmp);
					s2 = move(all_vars_equal[loop_bound - 1]);
					all_vars_equal.pop_back();
					--i;
					break;
				}
			}
			++i;
		}
	}
	
	
	const unsigned int bound = all_vars_equal.size();
	if (bound != 0) // Some variables are equals
	{		
		//cout << bound << " variables are equals" << endl;
		unsigned int new_nb_cols = 0;
		map<Expression,unsigned int> col_by_expr;
		vector<Expression> expr_by_col;
		
		// equal variables are substituted in each Expression
		for (unsigned int i = 0; i < m_expr_by_col.size(); ++i)
		{
			Expression expr = m_expr_by_col[i];
			for (unsigned int j = 0; j < bound; ++j)
			{
				auto it_x = all_vars_equal[j].begin();
				Variable x = *it_x;
				++it_x;
				for ( ; it_x != all_vars_equal[j].end(); ++it_x)
				{
					if (it_x->priority() <= x.priority()) expr.replace(*it_x,x); // according to their "priority" (P,C > K > ... > temporary vars)
					else
					{
						expr.replace(x,*it_x);
						x = *it_x;
					}
				}
			}
			//cout << "Expr: " << expr;
			auto it = col_by_expr.find(expr);
			if (it == col_by_expr.end())
			{
				//cout << " added" << endl;
				col_by_expr[expr] = new_nb_cols;
				expr_by_col.push_back(move(expr));
				for (unsigned int l = 0; l < nb_lines; ++l) mat(l,new_nb_cols) = m_matrix(l,i);
				++new_nb_cols;
			}
			else
			{
				//cout << " already present" << endl;
				const unsigned int c = it->second;
				for (unsigned int l = 0; l < nb_lines; ++l) mat(l,c) += m_matrix(l,i);
			}
		}
		//null columns are removed
		{
			unsigned int i = 0;
			while (i < new_nb_cols)
			{
				for (unsigned int j = 0; j < nb_lines; ++j)
				{
					if (mat(j,i) != 0)
					{
						++i;
						goto loop_end;
					}
				}
				--new_nb_cols;
				for (unsigned int j = 0; j < nb_lines; ++j) mat(j,i) = mat(j,new_nb_cols);
				expr_by_col[i] = move(expr_by_col[new_nb_cols]);
				expr_by_col.pop_back();
loop_end:
				{
				}
			}
		}
		
		swap(m_expr_by_col,expr_by_col);
		
		mat.resize(nb_lines,new_nb_cols);
		const unsigned int rank = mat.Gauss();
		mat.resize(rank,new_nb_cols);
		
		m_matrix = move(mat);
		
		this->simplify_tmp();
		
		/* adding equations x = y to the system */
		{
			const unsigned int old_nb_lines = m_matrix.getNbLines();
			unsigned int nb_eq_to_add = 0;
			for (unsigned int i = 0; i < all_vars_equal.size(); ++i) nb_eq_to_add += all_vars_equal[i].size() - 1;
			
			const unsigned int old_nb_cols = m_expr_by_col.size();
			map<Variable,unsigned int> col_by_var;
			
			for (unsigned int i = 0; i < all_vars_equal.size(); ++i)
			{
				for (auto it_x = all_vars_equal[i].cbegin(); it_x != all_vars_equal[i].cend(); ++it_x)
				{
					for (unsigned int j = 0; j < old_nb_cols; ++j)
					{
						if (m_expr_by_col[j] == *it_x)
						{
							col_by_var[*it_x] = j;
							goto loop_end2;
						}
						
					}
					col_by_var[*it_x] = m_expr_by_col.size();
					m_expr_by_col.emplace_back(*it_x);
loop_end2:
					{
					}
				}
			}
			
			m_matrix.resize(old_nb_lines + nb_eq_to_add, m_expr_by_col.size());
			
					
			for (unsigned int i = 0; i < old_nb_lines; ++i)
			{
				for (unsigned int j = old_nb_cols; j < m_expr_by_col.size(); ++j) m_matrix(i,j) = 0;
			}
			for (unsigned int i = 0; i < nb_eq_to_add; ++i)
			{
				for (unsigned int j = 0; j < m_expr_by_col.size(); ++j) m_matrix(i+old_nb_lines,j) = 0;
			}
			
			nb_eq_to_add = 0;
			for (unsigned int i = 0; i < all_vars_equal.size(); ++i)
			{
				auto it_x = all_vars_equal[i].cbegin();
				const unsigned int col_x = col_by_var[*it_x];
				auto it_y = it_x;
				for (++it_y; it_y != all_vars_equal[i].cend(); ++it_y)
				{
					const unsigned int col_y = col_by_var[*it_y];
					m_matrix(old_nb_lines + nb_eq_to_add,col_x) = 1;
					m_matrix(old_nb_lines + nb_eq_to_add,col_y) = 1;
					++nb_eq_to_add;
				}
			}
		}
		
		return true;
	}
	else return false;
}

void SystemOfEquations::simplify()
{
	bool need_to_reinitialized = this->simplify_tmp();
	need_to_reinitialized |= this->makeKeyAndStateInd_ANDOR();
	need_to_reinitialized |= this->makeKeyAndStateInd();
	//need_to_reinitialized |= this->makePackOfVarsInd();
	if (need_to_reinitialized) this->initialize_SubSystem();
}


ostream& operator<<( ostream &flux, SystemOfEquations const& sys)
{
	const unsigned int nb_cols = sys.m_matrix.getNbColumns();
	const unsigned int nb_lines = sys.m_matrix.getNbLines();
	
	for (unsigned int l = 0; l < nb_lines; ++l)
	{
		bool plus_needed = false;
		for (unsigned int c = 0; c < nb_cols; ++c)
		{
			const GFElement coef = sys.m_matrix(l,c);
			if (coef != 0)
			{
				if (plus_needed) flux << "+ ";
				plus_needed = true;
				Expression const &expr = sys.m_expr_by_col[c];
				if (!expr.isConstant())
				{
					if (coef != 1) flux << coef << ".";
					flux << expr << ' ';
				}
				else flux << coef << ' ';
			}
		}
		flux << endl;
	}
	flux << endl;
	return flux;
}


SystemOfEquations::SystemOfEquations(SystemOfEquations const& sys) : m_known_vars(sys.m_known_vars), m_original_equations(sys.m_original_equations), m_matrix(sys.m_matrix),
m_expr_by_col(sys.m_expr_by_col), m_set_of_vars(sys.m_set_of_vars), m_E(sys.m_E)
{
}

SystemOfEquations& SystemOfEquations::operator=(SystemOfEquations const& sys)
{
	if (&sys != this)
	{
		m_known_vars = sys.m_known_vars;
		m_original_equations = sys.m_original_equations;
		
		m_expr_by_col = sys.m_expr_by_col;
		
		m_set_of_vars = sys.m_set_of_vars;
		
		m_E = sys.m_E;
		
		m_matrix = sys.m_matrix;
	}
	return *this;	
}


SystemOfEquations& SystemOfEquations::operator=(SystemOfEquations && sys)
{
	m_known_vars = move(sys.m_known_vars);
	m_original_equations = move(sys.m_original_equations);
	
	m_expr_by_col = move(sys.m_expr_by_col);
			
	m_set_of_vars = move(sys.m_set_of_vars);
	
	m_E = move(sys.m_E);
		
	m_matrix = move(sys.m_matrix);
	
	return *this;
}


SystemOfEquations::SystemOfEquations(SystemOfEquations && sys) : m_known_vars(move(sys.m_known_vars)), m_original_equations(move(sys.m_original_equations)), m_matrix(move(sys.m_matrix)),
m_expr_by_col(move(sys.m_expr_by_col)), m_set_of_vars(move(sys.m_set_of_vars)), m_E(move(sys.m_E))
{
}


void SystemOfEquations::initialize_SubSystem()
{	
	const unsigned int rank = m_matrix.GaussJordan();
	
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	Matrix<GFElement> mat(rank, nb_cols - rank);
	
	m_E.m_line.clear();
	m_E.m_line.reserve(rank);
	m_E.m_column.clear();
	m_E.m_column.reserve(nb_cols - rank);
	
	unsigned int l = 0;
	for (unsigned int c = 0; c < nb_cols; ++c)
	{
		set<Variable> s = m_expr_by_col[c].getVariables();
		SubsetOfVars v = m_set_of_vars.getSubset(s.begin(),s.end());
		if (l < rank && m_matrix(l,c) != 0)
		{
			m_E.m_line.emplace_back(make_pair(move(v),m_expr_by_col[c].getTypeSbox()));
			++l;
		}
		else
		{
			const unsigned int new_col = m_E.m_column.size();
			m_E.m_column.emplace_back(make_pair(move(v),m_expr_by_col[c].getTypeSbox()));
			for (unsigned int i = 0; i < rank; ++i) mat(i,new_col) = m_matrix(i,c);
		}
	}
	
	m_E.m_set_of_vars = m_set_of_vars;
	
	m_E.m_matrix = move(mat);
	
	delete[] m_E.m_index_col;
	delete[] m_E.m_index_line;
	
	m_E.m_index_col = new unsigned int [nb_cols - rank];
	m_E.m_index_line = new unsigned int [rank];
	
	m_E.setKnownVars(m_known_vars.begin(),m_known_vars.end());
}


bool SystemOfEquations::isLinear(Variable const & v) const
{
	return m_E.isLinear(v);
}

SystemOfEquations::SystemOfEquations(string const & filename)
{
	cout << "Parsing system of equations: " << flush;
	ifstream file(filename, ifstream::in);
	
	if (!file)
	{
		cerr << "Error: file " << filename << " impossible to open" << endl;
		exit(EXIT_FAILURE);
	}
	
	list<Expression> list_of_exprs;
	
	vector<Variable> known_vars;	
	
	int mode = 0;
	string line;
	while (std::getline(file,line))
	{
		if (line[0] != '#')
		{
			if (line[0] == '-')
			{
				if (line == "--system:") mode = 1;
				else if (line == "--known variables:") mode = 2;
			}
			else
			{
				switch (mode)
				{
					case 1:
						{
							list_of_exprs.emplace_back(line);
							Expression const & expr = *(list_of_exprs.rbegin());
							if (expr.isConstant())
							{
								if (expr.getConstant() == 0) list_of_exprs.pop_back();
								else
								{
									cerr << "The system has no solutions: " << expr << endl;
									exit(EXIT_FAILURE);
								}
							}
						}
						break;
					case 2:
						{
							string::size_type start_index = 0;
							while (start_index != string::npos)
							{
								while (start_index < line.length() && line[start_index] == ' ') ++start_index;
								if (start_index < line.length())
								{
									string::size_type end_index = line.find(' ', start_index);
									known_vars.emplace_back(line.substr(start_index,end_index - start_index));
									start_index = end_index;
								}
								else break;
							}
						}
						break;
					default:
						break;
				}
				
			}
		}
	}
	
	cout << "done (" << known_vars.size() << " known variables)" << endl;
	
	*this = SystemOfEquations(list_of_exprs.begin(),list_of_exprs.end());
	this->setKnownVars(known_vars.begin(),known_vars.end());
	
}


bool SystemOfEquations::isKnown(Variable const & v) const
{
	auto it = m_known_vars.find(v);
	return (it != m_known_vars.end());
}

void SystemOfEquations::swapColumns(const unsigned int c1, const unsigned int c2) const
{
	m_matrix.swapColumns(c1,c2);
	swap(m_expr_by_col[c1],m_expr_by_col[c2]);
}


bool SystemOfEquations::makeKeyAndStateInd()
{
	vector<Expression> new_expr;
	
	map<Variable,pair<vector<set<Variable>>,vector<Variable>>> map_kv;
	
	for (auto it = m_expr_by_col.begin(); it != m_expr_by_col.end(); ++it)
	{
		auto s_v = it->getVariables();
		if (s_v.size() > 1)
		{
			set<Variable> key_vars;
			set<Variable> state_vars;
			for (auto const & v : s_v)
			{
				if (v.priority() == 1) key_vars.emplace(v);
				else state_vars.emplace(v);
			}
			if (key_vars.empty() || state_vars.empty()) new_expr.push_back(*it);
			else
			{
				vector<Variable> new_state_vars;
				for (auto const & k : key_vars)
				{
					auto it_map = map_kv.find(k);
					if (it_map == map_kv.end())
					{
						vector<Variable> tmp_v;
						vector<set<Variable>> tmp_s;
						Variable v;
						tmp_v.emplace_back(v);
						tmp_s.emplace_back(state_vars);
						map_kv.emplace(make_pair(k,make_pair(tmp_s,tmp_v)));
						new_state_vars.emplace_back(v);
					}
					else
					{
						vector<set<Variable>> & tmp_s = (it_map->second).first;
						vector<Variable> & tmp_v = (it_map->second).second;
						unsigned int i = 0;
						while (i < tmp_s.size())
						{
							set<Variable> s;
							std::set_intersection( tmp_s[i].begin(), tmp_s[i].end(), state_vars.begin(), state_vars.end(), std::inserter( s, s.begin() ) );
							if (!s.empty())
							{
								tmp_s[i] = move(s);
								new_state_vars.emplace_back(tmp_v[i]);
								break;
							}
							++i;
						}
						if (i == tmp_s.size())
						{
							Variable v;
							tmp_v.emplace_back(v);
							tmp_s.emplace_back(state_vars);
							new_state_vars.emplace_back(v);
						}
					}
				}
				Sbox new_S (state_vars.size() + new_state_vars.size() );
				vector<Expression> exprs;
				for (auto const & v : state_vars) exprs.emplace_back(v);
				for (auto const & v : new_state_vars) exprs.emplace_back(v);
				new_expr.emplace_back(new_S,exprs);
			}
		}
		else new_expr.push_back(*it);
	}
	if (map_kv.empty()) return false;
	else
	{
		list<tuple<Variable,Variable,Variable>> L;
		for (auto const & p : map_kv)
		{
			auto const & tmp_s = (p.second).first;
			auto const & tmp_v = (p.second).second;
			for (unsigned int i = 0; i < tmp_v.size(); ++i) L.emplace_back(make_tuple(tmp_v[i],*(tmp_s[i].begin()),p.first));
		}
		map_kv.clear();
		Matrix<GFElement> mat = m_matrix;
		const unsigned int nb_lines = m_matrix.getNbLines();
		const unsigned int nb_cols = m_matrix.getNbColumns();
		mat.resize(nb_lines + L.size(), nb_cols + 3*L.size());
		for (unsigned int l = nb_lines; l < mat.getNbLines(); ++l)
		{
			for (unsigned int c = 0; c < mat.getNbColumns(); ++c) mat(l,c) = 0;
		}
		for (unsigned int l = 0; l < nb_lines; ++l)
		{
			for (unsigned int c = nb_cols; c < mat.getNbColumns(); ++c) mat(l,c) = 0;
		}
		unsigned int new_nb_cols = nb_cols;
		unsigned int new_nb_lines = nb_lines;
		for (auto const & t : L)
		{
			auto const & v0 = get<0>(t);
			unsigned int c = 0;
			while (c < new_nb_cols && new_expr[c] != v0) ++c;
			if (c == new_nb_cols)
			{
				new_expr.emplace_back(v0);
				++new_nb_cols;
			}
			mat(new_nb_lines,c) = 1;
			auto const & v1 = get<1>(t);
			c = 0;
			while (c < new_nb_cols && new_expr[c] != v1) ++c;
			if (c == new_nb_cols)
			{
				new_expr.emplace_back(v1);
				++new_nb_cols;
			}
			mat(new_nb_lines,c) = 1;
			auto const & v2 = get<2>(t);
			c = 0;
			while (c < new_nb_cols && new_expr[c] != v2) ++c;
			if (c == new_nb_cols)
			{
				new_expr.emplace_back(v2);
				++new_nb_cols;
			}
			mat(new_nb_lines,c) = 1;
			cout << v0 << " = " << v1 << " + " << v2 << endl;
			++new_nb_lines;
		}
		mat.resize(new_nb_lines,new_nb_cols);
		swap(m_expr_by_col,new_expr);
		swap(m_matrix,mat);
		set<Variable> all_vars;
		for (auto const & expr : m_expr_by_col) expr.getVariables(all_vars);
		m_set_of_vars = SetOfVars(all_vars.begin(),all_vars.end());
		return true;
	}
}


bool SystemOfEquations::makeKeyAndStateInd_ANDOR()
{
	vector<Expression> new_expr;
	new_expr.reserve(m_expr_by_col.size());
	
	list<pair<Variable,Variable>> list_kv;
	
	for (auto it = m_expr_by_col.begin(); it != m_expr_by_col.end(); ++it)
	{
		auto s_v = it->getVariables();
		if (s_v.size() > 1 && it->getTypeSbox() == -1)
		{
			set<Variable> key_vars;
			set<Variable> state_vars;
			for (auto const & v : s_v)
			{
				if (v.priority() == 1) key_vars.emplace(v);
				else state_vars.emplace(v);
			}
			if (key_vars.empty() || state_vars.empty()) new_expr.emplace_back(*it);
			else
			{
				auto e = *it;
				for (auto const & k : key_vars)
				{
					Variable v;
					e.replace(k,v);
					list_kv.emplace_back(make_pair(k,move(v)));
				}
				new_expr.emplace_back(move(e));
			}
		}
		else new_expr.emplace_back(*it);
	}
	if (list_kv.empty()) return false;
	else
	{
		Matrix<GFElement> mat = m_matrix;
		const unsigned int nb_lines = m_matrix.getNbLines();
		const unsigned int nb_cols = m_matrix.getNbColumns();
		mat.resize(nb_lines + list_kv.size(), nb_cols + 2*list_kv.size());
		unsigned int new_nb_cols = nb_cols;
		unsigned int new_nb_lines = nb_lines;
		for (auto const & p : list_kv)
		{
			auto const & k = p.first;
			unsigned int ck = 0;
			while (ck < nb_cols && new_expr[ck] != k) ++ck;
			if (ck == nb_cols)
			{
				new_expr.emplace_back(k);
				ck = new_nb_cols++;
				for (unsigned int l = 0; l < new_nb_lines; ++l) mat(l,ck) = 0;
			}
			
			auto const & v = p.second;
			unsigned int cv = 0;
			while (cv < nb_cols && new_expr[cv] != v) ++cv;
			if (cv == nb_cols)
			{
				new_expr.emplace_back(v);
				cv = new_nb_cols++;
				for (unsigned int l = 0; l < new_nb_lines; ++l) mat(l,cv) = 0;
			}
			//cout << k << " = " << v << endl;
			for (unsigned int c = 0; c < new_nb_cols; ++c) mat(new_nb_lines,c) = 0;
			mat(new_nb_lines,ck) = 1;
			mat(new_nb_lines,cv) = 1;
			++new_nb_lines;
		}
		mat.resize(new_nb_lines,new_nb_cols);
		swap(m_expr_by_col,new_expr);
		swap(m_matrix,mat);
		set<Variable> all_vars;
		for (auto const & expr : m_expr_by_col) expr.getVariables(all_vars);
		m_set_of_vars = SetOfVars(all_vars.begin(),all_vars.end());
		return true;		
	}
}

