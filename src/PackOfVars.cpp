/*
 * PackOfVars.cpp
 * 
 * Copyright 2014 pderbez <pderbez@pderbez-VirtualBox>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <utility>

#include "PackOfVars.h"

using namespace std;

SubsetOfPacks::SubsetOfPacks() : SubsetOf<PackOfVars>(), m_vars()
{
}

SubsetOfPacks::SubsetOfPacks(SubsetOfPacks const & s) : SubsetOf<PackOfVars>(s), m_vars(s.m_vars)
{
}

SubsetOfPacks::SubsetOfPacks(SubsetOfPacks && s) : SubsetOf<PackOfVars>(move(s)), m_vars(move(s.m_vars))
{
}

SubsetOfPacks & SubsetOfPacks::operator=(SubsetOfPacks const & s)
{
	SubsetOf<PackOfVars>::operator=(s);
	m_vars = s.m_vars;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator=(SubsetOfPacks && s)
{
	SubsetOf<PackOfVars>::operator=(move(s));
	m_vars = move(s.m_vars);
	return *this;
}

SubsetOfPacks::~SubsetOfPacks()
{
}
		
SubsetOfPacks::SubsetOfPacks(SubsetOf<PackOfVars> const & s) : SubsetOf<PackOfVars>(s)
{
	m_vars = this->getSetOf().begin()->first.empty_set();
	vector<PackOfVars> my_vec = this->get();
	for (auto const & x : my_vec) m_vars += x;
}

SubsetOfPacks::SubsetOfPacks(SubsetOf<PackOfVars> && s) : SubsetOf<PackOfVars>(move(s))
{
	m_vars = this->getSetOf().begin()->first.empty_set();
	vector<PackOfVars> my_vec = this->get();
	for (auto const & x : my_vec) m_vars += x;
}

SubsetOfPacks & SubsetOfPacks::operator=(SubsetOf<PackOfVars> const & s)
{
	SubsetOf<PackOfVars>::operator=(s);
	m_vars = this->getSetOf().begin()->first.empty_set();
	vector<PackOfVars> my_vec = this->get();
	for (auto const & x : my_vec) m_vars += x;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator=(SubsetOf<PackOfVars> && s)
{
	SubsetOf<SubsetOfVars>::operator=(move(s));
	m_vars = this->getSetOf().begin()->first.empty_set();
	vector<PackOfVars> my_vec = this->get();
	for (auto const & x : my_vec) m_vars += x;
	return *this;
}
		
SubsetOfPacks & SubsetOfPacks::operator+=(SubsetOfPacks const & s)
{
	SubsetOf<PackOfVars>::operator+=(s);
	m_vars += s.m_vars;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator-=(SubsetOfPacks const & s)
{
	SubsetOf<PackOfVars>::operator-=(s);
	m_vars -= s.m_vars;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator&=(SubsetOfPacks const & s)
{
	SubsetOf<PackOfVars>::operator&=(s);
	m_vars &= s.m_vars;
	return *this;
}
		
SubsetOfPacks & SubsetOfPacks::operator+=(SubsetOf<PackOfVars> const & s)
{
	SubsetOf<PackOfVars>::operator+=(s);
	vector<PackOfVars> my_vec = s.get();
	for (auto const & x : my_vec) m_vars += x;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator-=(SubsetOf<PackOfVars> const & s)
{
	SubsetOf<PackOfVars>::operator-=(s);
	vector<PackOfVars> my_vec = s.get();
	for (auto const & x : my_vec) m_vars -= x;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator&=(SubsetOf<PackOfVars> const & s)
{
	SubsetOf<PackOfVars>::operator&=(s);
	vector<PackOfVars> my_vec = s.get();
	for (auto const & x : my_vec) m_vars &= x;
	return *this;
}
		
SubsetOfPacks & SubsetOfPacks::operator+=(PackOfVars const & p)
{
	SubsetOf<PackOfVars>::operator+=(p);
	m_vars += p;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator-=(PackOfVars const & p)
{
	SubsetOf<PackOfVars>::operator-=(p);
	m_vars -= p;
	return *this;
}

SubsetOfPacks & SubsetOfPacks::operator&=(PackOfVars const & p)
{
	SubsetOf<PackOfVars>::operator&=(p);
	m_vars &= p;
	return *this;
}

SubsetOfPacks::SubsetOfPacks(SubsetOf<PackOfVars> && s, SubsetOfVars && vars) : SubsetOf<PackOfVars>(move(s)), m_vars(move(vars))
{
}
		
SubsetOfPacks operator+(SubsetOfPacks const & s1, SubsetOfPacks const & s2)
{
	SubsetOf<PackOfVars> const & s1_tmp = s1;
	SubsetOf<PackOfVars> const & s2_tmp = s2;
	return SubsetOfPacks(s1_tmp + s2_tmp, s1.m_vars + s2.m_vars);
}

SubsetOfPacks operator-(SubsetOfPacks const & s1, SubsetOfPacks const & s2)
{
	SubsetOf<PackOfVars> const & s1_tmp = s1;
	SubsetOf<PackOfVars> const & s2_tmp = s2;
	return SubsetOfPacks(s1_tmp - s2_tmp, s1.m_vars - s2.m_vars);
}

SubsetOfPacks operator&(SubsetOfPacks const & s1, SubsetOfPacks const & s2)
{
	SubsetOf<PackOfVars> const & s1_tmp = s1;
	SubsetOf<PackOfVars> const & s2_tmp = s2;
	return SubsetOfPacks(s1_tmp & s2_tmp, s1.m_vars & s2.m_vars);
}

		
SubsetOfPacks operator+(SubsetOfPacks const & sp, SubsetOf<PackOfVars> const & s)
{
	SubsetOfPacks res = sp;
	return res += s;
}

SubsetOfPacks operator-(SubsetOfPacks const & sp, SubsetOf<PackOfVars> const & s)
{
	SubsetOfPacks res = sp;
	return res -= s;
}

SubsetOfPacks operator&(SubsetOfPacks const & sp, SubsetOf<PackOfVars> const & s)
{
	SubsetOfPacks res = sp;
	return res &= s;
}
		
SubsetOfPacks operator-(SubsetOf<PackOfVars> const & s, SubsetOfPacks const & sp)
{
	SubsetOfPacks res = s;
	return res -= sp;
}

SubsetOfPacks operator+(SubsetOfPacks const & s, PackOfVars const & p)
{
	SubsetOfPacks res = s;
	return res += p;
}

SubsetOfPacks operator-(SubsetOfPacks const & s, PackOfVars const & p)
{
	SubsetOfPacks res = s;
	return res -= p;
}

SubsetOfPacks operator&(SubsetOfPacks const & s, PackOfVars const & p)
{
	SubsetOfPacks res = s;
	return res &= p;
}
		
SubsetOfPacks operator-(PackOfVars const & p, SubsetOfPacks const & s)
{
	return s.getSubset(p) - s;
}

void SubsetOfPacks::clear()
{
	m_vars.clear();
	SubsetOf<PackOfVars>::clear();
}

