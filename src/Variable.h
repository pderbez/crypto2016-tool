/*
 * Variable.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#ifndef DEF_VARIABLE
#define DEF_VARIABLE

#include <string>
#include <iostream>
#include <tuple>
#include <vector>

class Variable
{
	public:
		Variable();
		Variable(std::string const& name);
		Variable(Variable const&);
		~Variable();
		Variable& operator=(Variable const&);
		Variable& operator=(Variable &&);
		Variable& operator=(std::string const&);
		
		// Member functions
		std::string const& getName() const;
		
		int priority() const;
		
		static void printAllRegisteredVars();
		
		
	private:
		// Member variables
		unsigned int m_num;
		
		void print(std::ostream &flux) const;
		
		typedef std::pair<std::string, unsigned int> MyPair;
		static std::vector<MyPair> variables_name; //names associated to variables
		// first: name
		// second: counter
		static std::vector<unsigned int> empty_case; //free cases of variables_name;
		
		
		

		
		
		
	friend std::ostream& operator<<( std::ostream &flux, Variable const& var);
	
	// Relational Operators (for fast comparison, not related to the name)
	friend bool operator<(Variable const& var1, Variable const& var2) {return var1.m_num < var2.m_num;};
	friend bool operator<=(Variable const& var1, Variable const& var2) {return var1.m_num <= var2.m_num;};
	friend bool operator>(Variable const& var1, Variable const& var2) {return var1.m_num > var2.m_num;};
	friend bool operator>=(Variable const& var1, Variable const& var2) {return var1.m_num >= var2.m_num;};
	friend bool operator==(Variable const& var1, Variable const& var2) {return var1.m_num == var2.m_num;};
	friend bool operator!=(Variable const& var1, Variable const& var2) {return var1.m_num != var2.m_num;};
		
};

#endif
