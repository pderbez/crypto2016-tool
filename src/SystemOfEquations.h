/*
 * SystemOfEquations.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef DEF_SYSTEM_OF_EQUATIONS
#define DEF_SYSTEM_OF_EQUATIONS

#include <set>
#include <vector>
#include <list>
#include <map>
#include <string>
#include <fstream>

#include "GFElement.h"
#include "Matrix.h"
#include "SubSystem.h"
#include "Variable.h"
#include "SetOfVars.h"


class SystemOfEquations
{
	public:
		
		
		SystemOfEquations(std::string const & filename);
		SystemOfEquations(SystemOfEquations const&);
		SystemOfEquations(SystemOfEquations &&);
		
		/* Iter has to be an iterator over Expressions */
		template <typename Iter> SystemOfEquations(Iter begin, Iter end, bool simplify = true);
		
		SystemOfEquations& operator=(SystemOfEquations const&);
		SystemOfEquations& operator=(SystemOfEquations &&);
		
		/* Iter has to be an iterator over Variables */
		template <typename Iter> void setKnownVars(Iter it, const Iter end);
		
		~SystemOfEquations();
		
		friend std::ostream& operator<<( std::ostream &flux, SystemOfEquations const& var);
		
		/* function for devs */
		void simplify(); // take care of the fact that x = y <=> S(x) = S(y)
		SubSystem const & getSubSystem() const {return m_E;};  
		SetOfVars const & getSetOfVars() const {return m_set_of_vars;};
		
		bool isLinear(Variable const &) const;
		bool isKnown(Variable const &) const;
	
	private:
		
		bool simplify_tmp(); // take care of the fact that x = y <=> S(x) = S(y)
		void initialize_SubSystem(); //initialize m_E
		bool makeKeyAndStateInd();
		bool makePackOfVarsInd();
		bool makeKeyAndStateInd_ANDOR();
		
		/* Members */
		
		std::set<Variable> m_known_vars; // the set of known variables
		std::list<Expression> m_original_equations; // the original system of equations
		
		mutable Matrix<GFElement> m_matrix; // the matrix representing the system
		
		mutable std::vector<Expression> m_expr_by_col; // the Expressions (Variable x or Monomial S(x_1,...,x_n)) corresponding to the columns.
		
		SetOfVars m_set_of_vars; // the set of all involved variables

		SubSystem m_E; // the corresponding System for fast computation.
		
		
		void swapColumns(unsigned int, unsigned int) const;
	
};

#include "SystemOfEquations.tpp"

#endif
