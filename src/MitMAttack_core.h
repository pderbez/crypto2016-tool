/*
 * MitMAttack_core.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_MITM_ATTACK_CORE
#define DEF_MITM_ATTACK_CORE

#include <tuple>
#include <set>
#include <list>

class BlockCipher;
class SubsetOfPacks;
class SubSystem;
class MitMAttack_Diff;
class MitMAttack_Eqs;

#include "PackOfVars.h"

struct MitMAttack_Core
{
	public:
		MitMAttack_Core(BlockCipher const &);
		
		MitMAttack_Core(MitMAttack_Core const &);
		MitMAttack_Core(MitMAttack_Core &&);
		
		MitMAttack_Core & operator=(MitMAttack_Core const &);
		MitMAttack_Core & operator=(MitMAttack_Core &&);
		
		~MitMAttack_Core();
		
		
	private:
		typedef std::pair<bool,bool> up_req_diff;
		bool updateAsDiff_tmp(SubsetOfPacks const & s, const int type);
		
		bool isValid() const {return m_valid;};
		
		void updateAsDiff_fromP(PackOfVars const &, int, int, int);

		void updateAsEqs_fromC(PackOfVars const &, int, int);
		
		void propagateZeros(bool check = true);
		
		bool time_online_Diff_P_smaller_than(int) const;
		bool time_online_Eqs_C_smaller_than(int) const;
		
		void updateAsDiff_fromP_lead(PackOfVars const &);
		
		int data_fromP() const;
		int data_fromC() const;
		
		int nbEqs_fromC() const;
		int nbEqs() const;
		int nbEqsMax() const;
		bool updateAsEqs_tmp(SubsetOfPacks const &, int);
		bool allVarsRequired() const;

	private:
		
		static const int tt[7][7]; 	
		
		SubsetOfPacks * t_vars; // 0: unused/null - 1: online - 2: offline - 3: 0 or 1 - 4: 0 or 2 - 5: 1 or 2 - 6: 0, 1 or 2 -
		BlockCipher const * m_B; 
		bool m_valid;
	
		friend std::list<MitMAttack_Core> findMitMAttacks_Diff_fromP_lead(MitMAttack_Core, int, int, int);
		friend void findMitMAttacks_Diff_fromP_lead_tmp_full(MitMAttack_Core &, PackOfVars const &, std::set<SubsetOfPacks> &, int, int, SubSystem const &, SubsetOfPacks const & );
		friend void findMitMAttacks_Diff_fromP_lead_tmp_fast(MitMAttack_Core &, PackOfVars const &, std::map<std::pair<unsigned int, unsigned int>,std::set<SubsetOfPacks>> &,
																																								int, int, int);
		friend void findMitMAttacks_Diff_fromP_lead_tmp_fast_tmp(MitMAttack_Core &, PackOfVars const &, std::map<std::pair<unsigned int, unsigned int>,std::set<SubsetOfPacks>> &,
																															SubSystem &, SubsetOfPacks const &, int, int, int);
		
		friend std::list<MitMAttack_Core> findMitMAttacks_Diff_fromP_lead_SPN(MitMAttack_Core, int, int);
		friend void findMitMAttacks_Diff_fromP_lead_SPN_tmp(MitMAttack_Core &, unsigned int, unsigned int, unsigned int, unsigned int, std::vector<PackOfVars> &, std::vector<PackOfVars> &, std::set<SubsetOfPacks> &, int, int);
		
		friend void findMitMAttacks_Eqs_fromC_lead_tmp_fast(MitMAttack_Core &, PackOfVars const &, std::map<std::pair<unsigned int, unsigned int>, std::list<MitMAttack_Core>> &,
																																								int, int, int);
		friend void findMitMAttacks_Eqs_fromC_lead_tmp_fast_init(MitMAttack_Core &, PackOfVars const &,
										std::map<std::pair<unsigned int, unsigned int>, std::list<MitMAttack_Core>> &,	int, int, int, SubSystem const &, SubsetOfPacks const &);
		friend void findMitMAttacks_Eqs_fromC_lead_tmp_fast_tmp(MitMAttack_Core &, std::map<std::pair<unsigned int, unsigned int>, std::list<MitMAttack_Core>> &, int, int, int);
		
		
		friend void findMitMAttacks_Eqs_fromC_lead_tmp_full_tmp(MitMAttack_Core &, PackOfVars const &, std::list<MitMAttack_Core> &, int, std::vector<PackOfVars> const &, unsigned int, SubSystem const &, SubsetOfPacks const & sub);
		friend void findMitMAttacks_Eqs_fromC_lead_tmp_full(MitMAttack_Core &, PackOfVars const &, std::list<MitMAttack_Core> &, int, SubSystem const &, SubsetOfPacks const & sub);
		friend std::list<MitMAttack_Core> findMitMAttacks_Eqs_fromC_lead(MitMAttack_Core, int, int);
		
		friend void findMitMAttacks_Eqs_fromC_lead_SPN_tmp(MitMAttack_Core &, unsigned int, unsigned int, unsigned int, unsigned int, std::vector<PackOfVars> &, std::vector<PackOfVars> &, std::list<MitMAttack_Core> &, int);
		friend std::list<MitMAttack_Core> findMitMAttacks_Eqs_fromC_lead_SPN(MitMAttack_Core, int);
		
		friend std::list<MitMAttack_Diff> findMitMAttacks_Diff_fromP(int, int, int, int, BlockCipher const &);
		friend std::list<MitMAttack_Eqs> findMitMAttacks_Eqs_fromC(int, int, int, int, BlockCipher const &);
		
		friend std::list<MitMAttack_Eqs> findSimpleMitMAttacks_Eqs_fromC(int, int, BlockCipher const &);
		
		friend MitMAttack_Core origin_MitM_Eqs_fromC_tmp(BlockCipher const & B, const int bound_time);
		friend MitMAttack_Core origin_MitM_Diff_fromP_tmp(BlockCipher const & B, const int bound_data, const int bound_time);
		friend MitMAttack_Core origin_MitM_Diff_fromP(BlockCipher const & B, const int bound_data, const int bound_time, const int bound_memory);
		friend MitMAttack_Core origin_MitM_Eqs_fromC(BlockCipher const & B, const int bound_data, const int bound_time, const int bound_memory);
		friend MitMAttack_Core origin_SimpleMitM_Eqs_fromC(BlockCipher const & B, const int bound_time);
		
		friend MitMAttack_Diff;
		friend MitMAttack_Eqs;

};

std::list<MitMAttack_Diff> findMitMAttacks_Diff_fromP(int, int, int, int, BlockCipher const &);
std::list<MitMAttack_Eqs> findMitMAttacks_Eqs_fromC(int, int, int, int, BlockCipher const &);

std::list<MitMAttack_Diff> findMitMAttacks_Diff_fromC(int, int, int, int, BlockCipher const &);
std::list<MitMAttack_Eqs> findMitMAttacks_Eqs_fromP(int, int, int, int, BlockCipher const &);

std::list<MitMAttack_Eqs> findSimpleMitMAttacks_Eqs_fromC(int, int, BlockCipher const &);


#include "BlockCipher.h"

#endif

