/*
 * SimpleMitMAttack.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_SIMPLE_MITM_ATTACK
#define DEF_SIMPLE_MITM_ATTACK

#include <set>
#include <map>

class MitMAttack_Eqs;
class BlockCipher;

#include "PackOfVars.h"

class SimpleMitMAttack
{
	public:
		SimpleMitMAttack(SimpleMitMAttack const &);
		
		SimpleMitMAttack & operator=(SimpleMitMAttack const &);
		
		~SimpleMitMAttack();
		
		SimpleMitMAttack(MitMAttack_Eqs const &);
		
		friend bool operator==(SimpleMitMAttack const &, SimpleMitMAttack const &);
		friend bool operator!=(SimpleMitMAttack const & A1, SimpleMitMAttack const & A2) {return !(A1 == A2);};
		friend bool operator<(SimpleMitMAttack const &, SimpleMitMAttack const &);
		
		int nbEqs() const {return m_nb_eqs;};
		int timeOnline() const;
		int timeOffline() const;
		int correctedMemory() const;
		SubsetOfPacks const & activeOn() const {return (m_onlineP ? *m_active_fromP : *m_active_fromC);};
		SubsetOfPacks const & activeOff() const {return (m_onlineP ? *m_active_fromC : *m_active_fromP);};
		SubsetOfPacks const & guessOn() const {return (m_onlineP ? *m_guess_fromP : *m_guess_fromC);};
		SubsetOfPacks const & guessOff() const {return (m_onlineP ? *m_guess_fromC : *m_guess_fromP);};
		
		BlockCipher const & getBlockCipher() const {return *m_B;};
		
	private:
		static std::set<SubsetOfPacks> st_all_subset;
		static std::map<SubsetOfPacks const *,int> st_all_time;
		
		SubsetOfPacks const * m_active_fromP;
		SubsetOfPacks const * m_active_fromC;
		SubsetOfPacks const * m_guess_fromP;
		SubsetOfPacks const * m_guess_fromC;
		int m_nb_eqs;
		BlockCipher const * m_B;
		bool m_onlineP;
		int m_cor_mem;
		
		void updateNbEqs();
};


bool operator==(SimpleMitMAttack const &, SimpleMitMAttack const &);
bool operator<(SimpleMitMAttack const &, SimpleMitMAttack const &);

void findSimpleMitMAttacks(const int bound_time, const int bound_memory, BlockCipher const & B, const int bound_eqs);

#endif
