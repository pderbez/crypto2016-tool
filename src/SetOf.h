/*
 * SetOf.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_SET_OF
#define DEF_SET_OF

#include <vector>
#include <stack>
#include <map>
#include <ostream>

template <typename T>
class SetOf;

template <typename T>
class SubsetOf
{
	public:
		
		SubsetOf();
		SubsetOf(SubsetOf<T> const &);
		SubsetOf(SubsetOf<T> &&);
		
		SubsetOf<T> & operator=(SubsetOf<T> const &);
		SubsetOf<T> & operator=(SubsetOf<T> &&);
		
		~SubsetOf();
		
		SubsetOf<T> & operator+=(SubsetOf<T> const &); // union
		SubsetOf<T> & operator-=(SubsetOf<T> const &); // difference
		SubsetOf<T> & operator&=(SubsetOf<T> const &); // intersection
		SubsetOf<T> & operator|=(SubsetOf<T> const & s) {return *this += s;}; // union
		
		SubsetOf<T> & operator+=(T const &);
		SubsetOf<T> & operator-=(T const &);
		SubsetOf<T> & operator&=(T const &);
		SubsetOf<T> & operator|=(T const & v) {return *this += v;};
		
		bool shareElements(SubsetOf<T> const &) const;
		
		
		bool includes(SubsetOf<T> const &) const;
		bool includes(T const &) const;
		template <typename Iter> bool includes(Iter begin, Iter end) const;
		
		bool is_included_in(SubsetOf<T> const & s) const {return s.includes(*this);};
		
		void clear();
		
		std::vector<T> get() const;
		T getFirst() const;
		
		SubsetOf<T> convert(SetOf<T> const &) const;
		
		bool is_empty() const;
		
		unsigned int size() const;
		
		SetOf<T> getSetOf() const;
		SubsetOf<T> empty_set() const;
		
		template <typename T1> friend SubsetOf<T1> operator~(SubsetOf<T1> const &); // complementary
		
		template <typename T1> friend SubsetOf<T1> operator+(SubsetOf<T1> const &, SubsetOf<T1> const &);
		template <typename T1> friend SubsetOf<T1> operator-(SubsetOf<T1> const &, SubsetOf<T1> const &);
		template <typename T1> friend SubsetOf<T1> operator&(SubsetOf<T1> const &, SubsetOf<T1> const &);
		template <typename T1> friend SubsetOf<T1> operator|(SubsetOf<T1> const & s1, SubsetOf<T1> const & s2);
		
		
		template <typename T1> friend SubsetOf<T1> operator+(SubsetOf<T1> const &, T1 const &);
		template <typename T1> friend SubsetOf<T1> operator-(SubsetOf<T1> const &, T1 const &);
		template <typename T1> friend SubsetOf<T1> operator&(SubsetOf<T1> const &, T1 const &);
		template <typename T1> friend SubsetOf<T1> operator|(SubsetOf<T1> const & s, T1 const & v);
		
		template <typename T1> friend SubsetOf<T1> operator+(T1 const & v, SubsetOf<T1> const & s);
		template <typename T1> friend SubsetOf<T1> operator-(T1 const &, SubsetOf<T1> const &);
		template <typename T1> friend SubsetOf<T1> operator&(T1 const & v, SubsetOf<T1> const & s);
		template <typename T1> friend SubsetOf<T1> operator|(T1 const & v, SubsetOf<T1> const & s);
		
		
		
		template <typename T1> friend bool operator!=(SubsetOf<T1> const & , SubsetOf<T1> const & );
		template <typename T1> friend bool operator==(SubsetOf<T1> const & s1, SubsetOf<T1> const & s2);
		
		template <typename T1> friend bool operator<(SubsetOf<T1> const & s1, SubsetOf<T1> const & s2); // for map, set, ...
		
		template <typename T1> friend std::ostream& operator<<( std::ostream &flux, SubsetOf<T1> const & s);
		
		
		friend class SetOf<T>;
	
	private:
		
		static const unsigned int block_size = 32;
		typedef uint32_t block_type;
		
		block_type * m_storage;
		unsigned int m_nb_blocks;
		unsigned int m_set_index;
		
		SubsetOf(block_type *, unsigned int, unsigned int);
		
	protected:
		
		SubsetOf<T> const & getSubset(T const &) const; 
		std::ostream& print(std::ostream &flux) const;
};

template <typename T>
class SetOf
{
	public: 
		SetOf();
		SetOf(SetOf<T> const &);
		SetOf(SetOf<T> &&);
		/* Iter has to be an iterator over T */
		template <typename Iter> SetOf(Iter begin, Iter end);
		
		SetOf<T> & operator=(SetOf<T> const &);
		SetOf<T> & operator=(SetOf<T> &&);
		
		~SetOf();
		
		/* Iter has to be an iterator over T */
		template <typename Iter> SubsetOf<T> getSubset(Iter begin, Iter end) const;
		SubsetOf<T> const & getSubset(T const &) const;
		
		std::vector<T> get() const;
		
		bool includes(SetOf<T> const &) const;
		
		SubsetOf<T> empty_set() const;
		
		typedef typename std::map<T, SubsetOf<T>>::size_type size_type;
		size_type size() const;
		
		typedef typename std::map<T, SubsetOf<T>>::const_iterator const_iterator;
		
		const_iterator cbegin() const;
		const_iterator cend() const;
		
		const_iterator begin() const {return this->cbegin();};
		const_iterator end() const {return this->cend();};
		
		template <typename T1> friend bool operator==(SetOf<T1> const &, SetOf<T1> const &);
		template <typename T1> friend bool operator!=(SetOf<T1> const &, SetOf<T1> const &);
	
	private:
		
		static std::vector<std::map<T, SubsetOf<T> > > v_sets_of_T;
		static std::vector<unsigned int> v_nb_refs;
		static std::stack<unsigned int> sk_empty_cases;
		
		unsigned int m_set_index;
		
		SetOf(unsigned int);
		
		friend class SubsetOf<T>;
};

#include "SetOf.tpp"

#endif

