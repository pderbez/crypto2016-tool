/*
 * BlockCipher.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_BLOCKCIPHER
#define DEF_BLOCKCIPHER

#include <map>
#include <list>

#include "SystemOfEquations.h"
#include "SubSystem.h"
#include "Variable.h"
#include "SetOfVars.h"

class BlockCipher
{
	public:
		BlockCipher(SystemOfEquations const&);
		BlockCipher(BlockCipher const &);
		BlockCipher(BlockCipher &&);
		
		BlockCipher & operator=(BlockCipher const &);
		BlockCipher & operator=(BlockCipher &&);
		
		~BlockCipher();
		
		SubsetOfPacks const & getStateVars() const {return m_state_vars;};
		SubsetOfPacks const & getKeyVars() const {return m_key_vars;};
		SubsetOfPacks const & getPlaintextVars() const {return m_plaintext_vars;};
		SubsetOfPacks const & getCiphertextVars() const {return m_ciphertext_vars;};
		SubsetOfPacks const & getNonLinKeyVars() const {return m_non_lin_key_vars;};
		SubsetOfPacks getAllVars() const { return m_state_vars + m_key_vars + m_plaintext_vars + m_ciphertext_vars;};
		
		SetOfVars const & getSetOfVars() const {return m_set_of_vars;};
		SetOf<PackOfVars> const & getSetOfPacks() const {return m_set_of_pack;};
		
		int getKeysize() const {return m_keysize;};
		int getStatesize() const {return m_statesize;};
		
		SubSystem const & getKeyschedule() const {return m_E_keyschedule;};
		SubSystem const & getEqs() const {return m_E_eq_key_unknown_pc_unknown;};
		SubSystem const & getEqsKnownPC() const {return m_E_eq_key_unknown_pc_known;};
		SubSystem const & getEqsKnownKey() const {return m_E_eq_key_known_pc_unknown;};
		SubSystem const & getEqsNonLinKey() const {return m_E_eq_nonlin_key_unknown_pc_unknown;};
		
		SubsetOfPacks const & varsFromVtoP(PackOfVars const & v) const {return m_from_v_to_p.at(v);};
		SubsetOfPacks const & varsFromVtoC(PackOfVars const & v) const {return m_from_v_to_c.at(v);};
		SubsetOfPacks const & varsFromPtoV(PackOfVars const & v) const {return m_from_p_to_v.at(v);};
		SubsetOfPacks const & varsFromCtoV(PackOfVars const & v) const {return m_from_c_to_v.at(v);};
		
		SubsetOfPacks const & guessFromP(PackOfVars const & v) const {return m_guess_from_p.at(v);};
		SubsetOfPacks const & guessFromC(PackOfVars const & v) const {return m_guess_from_c.at(v);};
		SubsetOfPacks const & guessToP(PackOfVars const & v) const {return m_guess_to_p.at(v);};
		SubsetOfPacks const & guessToC(PackOfVars const & v) const {return m_guess_to_c.at(v);};
		
		SubsetOfPacks const & varsRelatedFromP(PackOfVars const & v) const {return m_min_related_from_p.at(v);};
		SubsetOfPacks const & varsRelatedFromC(PackOfVars const & v) const {return m_min_related_from_c.at(v);};
		
		
		void sortVectorFromPtoV(std::vector<PackOfVars> & my_vec) const;
		void sortVectorFromCtoV(std::vector<PackOfVars> & my_vec) const;
		void sortVectorFromVtoC(std::vector<PackOfVars> & my_vec) const;
		void sortVectorFromVtoP(std::vector<PackOfVars> & my_vec) const;
		
		bool isSPN() const {return m_spn;};
		bool isMDS() const {return m_mds;};
		
		void forceNonMDS() {m_mds = false;};
		BlockCipher reverse() const;
		
		unsigned int maxSizePack() const {return m_biggest_pack;};
		
	private:
		
		SetOfVars m_set_of_vars;
		SetOf<PackOfVars> m_set_of_pack;
		
		SubSystem m_E_keyschedule;
		SubSystem m_E_eq_key_known_pc_unknown;
		SubSystem m_E_eq_key_unknown_pc_known;
		SubSystem m_E_eq_key_unknown_pc_unknown;
		SubSystem m_E_eq_nonlin_key_unknown_pc_unknown;
		
		SubsetOfPacks m_state_vars;
		SubsetOfPacks m_key_vars;
		SubsetOfPacks m_non_lin_key_vars;
		SubsetOfPacks m_plaintext_vars;
		SubsetOfPacks m_ciphertext_vars;
		
		int m_keysize;
		int m_statesize;
		
		std::map<PackOfVars, SubsetOfPacks> m_from_p_to_v;
		std::map<PackOfVars, SubsetOfPacks> m_from_c_to_v;
		
		std::map<PackOfVars, SubsetOfPacks> m_from_v_to_p;
		std::map<PackOfVars, SubsetOfPacks> m_from_v_to_c;
		
		std::map<PackOfVars, SubsetOfPacks> m_min_related_from_p;
		std::map<PackOfVars, SubsetOfPacks> m_min_related_from_c;
		
		std::map<PackOfVars, SubsetOfPacks> m_guess_from_p;
		std::map<PackOfVars, SubsetOfPacks> m_guess_from_c;
		
		std::map<PackOfVars, SubsetOfPacks> m_guess_to_p;
		std::map<PackOfVars, SubsetOfPacks> m_guess_to_c;
		
		bool m_spn;
		bool m_mds;
		
		unsigned int m_biggest_pack;
		
		
		/* Initialization functions */
		
		void initialize_PacksOfVars();
		
		SubsetOfPacks initialize_m_from_p_or_c_to_v(PackOfVars const &, SubsetOfVars const &);
		void initialize_m_from_p_to_v(PackOfVars const &, std::vector<SubsetOfPacks> const &);
		void initialize_m_from_c_to_v(PackOfVars const &, std::vector<SubsetOfPacks> const &);
		
		void initialize_m_from_v_to_p(PackOfVars const &);
		void initialize_m_from_v_to_c(PackOfVars const &);
		
		void initialize_m_guess_from_p(PackOfVars const &);
		void initialize_m_guess_from_c(PackOfVars const &);
		void initialize_m_guess_to_p(PackOfVars const &);
		void initialize_m_guess_to_c(PackOfVars const &);
		
		void initialize_m_min_related_from_p(PackOfVars const &);
		void initialize_m_min_related_from_c(PackOfVars const &);
		
		void initialize_m_from_p_to_v();
		void initialize_m_from_c_to_v();
		
		void initialize_isSPN();
		
		/* Private functions */

};

#endif
