/*
 * SimpleMitMAttack.cpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "SimpleMitMAttack.h"
#include "MitMAttack_eqs.h"
#include "BlockCipher.h"

std::set<SubsetOfPacks> SimpleMitMAttack::st_all_subset;
std::map<SubsetOfPacks const *,int> SimpleMitMAttack::st_all_time;

using namespace std;

void SimpleMitMAttack::updateNbEqs()
{
	SubSystem const & E = m_B->getEqsKnownKey();
	const int s = E.getNbSols(activeOn() + activeOff() + m_B->getPlaintextVars() + m_B->getCiphertextVars());
	m_nb_eqs = 2*m_B->getStatesize() - s;
}

SimpleMitMAttack::SimpleMitMAttack(SimpleMitMAttack const & A) : m_active_fromP(A.m_active_fromP), m_active_fromC(A.m_active_fromC),
m_guess_fromP(A.m_guess_fromP), m_guess_fromC(A.m_guess_fromC),
m_nb_eqs(A.m_nb_eqs), m_B(A.m_B), m_onlineP(A.m_onlineP), m_cor_mem(A.m_cor_mem)
{
}


		
SimpleMitMAttack & SimpleMitMAttack::operator=(SimpleMitMAttack const & A)
{
	m_active_fromP = A.m_active_fromP;
	m_active_fromC = A.m_active_fromC;
    m_guess_fromP = A.m_guess_fromP;
    m_guess_fromC = A.m_guess_fromC;
	m_nb_eqs = A.m_nb_eqs;
	m_B = A.m_B;
	m_onlineP = A.m_onlineP;
	m_cor_mem = A.m_cor_mem;
	return *this;
}


		
SimpleMitMAttack::~SimpleMitMAttack()
{	
}
		
SimpleMitMAttack::SimpleMitMAttack(MitMAttack_Eqs const & A) : m_B(addressof(A.getBlockCipher()))
{
	SubSystem const & E = m_B->getEqs();
	if (A.activeOn().shareElements(m_B->getPlaintextVars()))
	{
		{
			auto pair1 = st_all_subset.emplace(A.activeOn());
			m_active_fromP = addressof(*pair1.first);
		}
		
		{
			auto pair1 = st_all_subset.emplace(A.guessOn());
			m_guess_fromP = addressof(*pair1.first);
			st_all_time[m_guess_fromP] = A.timeOnline();
		}
		
		{
			auto pair2 = st_all_subset.emplace(A.activeOff() + m_B->getCiphertextVars());
			m_active_fromC = addressof(*pair2.first);
		}
		
		{
			auto pair2 = st_all_subset.emplace(A.guessOff() + m_B->getCiphertextVars());
			m_guess_fromC = addressof(*pair2.first);
			auto it = st_all_time.find(m_guess_fromC);
			if (it == st_all_time.end()) // offline time is unknown
			{
				st_all_time[m_guess_fromC] = E.getNbSols(*m_guess_fromC, *m_active_fromC + m_B->getNonLinKeyVars()) - m_B->getStatesize();
			}
		}
	}
	else
	{
		{
			auto pair1 = st_all_subset.emplace(A.activeOn());
			m_active_fromC = addressof(*pair1.first);
		}
		
		{
			auto pair1 = st_all_subset.emplace(A.guessOn());
			m_guess_fromC = addressof(*pair1.first);
			st_all_time[m_guess_fromC] = A.timeOnline();
		}
		
		{
			auto pair2 = st_all_subset.emplace(A.activeOff() + m_B->getPlaintextVars());
			m_active_fromP = addressof(*pair2.first);
		}
		
		{
			auto pair2 = st_all_subset.emplace(A.guessOff() + m_B->getPlaintextVars());
			m_guess_fromP = addressof(*pair2.first);
			auto it = st_all_time.find(m_guess_fromC);
			if (it == st_all_time.end()) // offline time is unknown
			{
				st_all_time[m_guess_fromP] = E.getNbSols(*m_guess_fromP, *m_active_fromP + m_B->getNonLinKeyVars()) - m_B->getStatesize();
			}
		}
	}
	
	m_onlineP = (st_all_time[m_guess_fromP] >= st_all_time[m_guess_fromC]);
	
	m_cor_mem = timeOffline() - (E.getNbSols(*m_guess_fromP) + E.getNbSols(*m_guess_fromC) - E.getNbSols(*m_guess_fromP + *m_guess_fromC));
	
	this->updateNbEqs();
}

int SimpleMitMAttack::timeOnline() const
{
	if (m_onlineP) return st_all_time.at(m_guess_fromP);
	else return st_all_time.at(m_guess_fromC);
}

int SimpleMitMAttack::timeOffline() const
{
	if (m_onlineP) return st_all_time.at(m_guess_fromC);
	else return st_all_time.at(m_guess_fromP);
}

int SimpleMitMAttack::correctedMemory() const
{
	return m_cor_mem;
}

bool operator==(SimpleMitMAttack const & A1, SimpleMitMAttack const & A2)
{
	return (A1.m_active_fromP == A2.m_active_fromP && A1.m_active_fromC == A2.m_active_fromC);
}

bool operator<(SimpleMitMAttack const & A1, SimpleMitMAttack const & A2)
{
	if (A1.m_active_fromP == A2.m_active_fromP) return (A1.m_active_fromC < A2.m_active_fromC);
	else return (A1.m_active_fromP < A2.m_active_fromP);
}

void findSimpleMitMAttacks(const int bound_time, const int bound_memory, BlockCipher const & B, const int bound_eqs)
{
	list<MitMAttack_Eqs> L = findSimpleMitMAttacks_Eqs_fromC(bound_time, bound_eqs, B);
	
	set<SimpleMitMAttack> S_final;
	
	for (auto const & A_e : L)
	{
		SimpleMitMAttack A (A_e);
		if (A.timeOnline() < bound_time && A.timeOffline() < bound_memory) S_final.emplace(move(A));
	}
	
	
	list<SimpleMitMAttack> L_best;
	
	for (auto const & A : S_final)
	{
		bool best = true;
		auto it = L_best.begin();
		while (it != L_best.end())
		{
			if (it->timeOnline() <= A.timeOnline() && (it->timeOffline() < A.timeOffline() || (it->timeOffline() == A.timeOffline() && it->correctedMemory() <= A.correctedMemory())))
			{
				best = false;
				break;
			}
			
			if (it->timeOnline() >= A.timeOnline() && (it->timeOffline() > A.timeOffline() || (it->timeOffline() == A.timeOffline() && it->correctedMemory() >= A.correctedMemory()))) it = L_best.erase(it);
			else ++it;
		}
		if (best) L_best.emplace_back(A);
	}
	
	for (auto const & A : L_best)
	{
		cout << "Best Attack: " << endl;
		cout << " - time: " << A.timeOnline() << endl;
		cout << " - memory: " << A.timeOffline() << " (corrected: " << A.correctedMemory() << ")" << endl;
		cout << " - nb eqs: " << A.nbEqs() << endl;
		cout << " - online: " << A.activeOn() << endl;
		cout << " - offline: " << A.activeOff() << endl;
		cout << endl;
	}
}
