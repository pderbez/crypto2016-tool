/*
 * SubSystem.cpp
 * 
 * Copyright 2014 pderbez <pderbez@pderbez-VirtualBox>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "SubSystem.h"
#include "GFElement.h"
#include "Variable.h"

#include <utility>
#include <cassert>
#include <list>

using namespace std;

SubSystem::SubSystem() : m_matrix(), m_index_col(nullptr), m_index_line(nullptr), m_set_of_vars(), m_line(), m_column()
{
}


SubSystem::SubSystem(SubSystem const& E) : m_matrix(E.m_matrix), m_set_of_vars(E.m_set_of_vars), m_line(E.m_line), m_column(E.m_column)
{
	const unsigned int nb_cols = m_matrix.getNbColumns();
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	m_index_col = new unsigned int [nb_cols];
	m_index_line = new unsigned int [nb_lines];
}

SubSystem::SubSystem(SubSystem && E) : m_matrix(move(E.m_matrix)), m_index_col(E.m_index_col), m_index_line(E.m_index_line),
m_set_of_vars(move(E.m_set_of_vars)), m_line(move(E.m_line)), m_column(move(E.m_column))
{
	E.m_index_col = nullptr;
	E.m_index_line = nullptr;
}



SubSystem& SubSystem::operator=(SubSystem const& E)
{
	const unsigned int nb_cols = E.m_matrix.getNbColumns();
	const unsigned int nb_lines = E.m_matrix.getNbLines();
	
	if (this->m_matrix.getNbColumns() < nb_cols)
	{
		delete[] m_index_col;
		m_index_col = new unsigned int [nb_cols];
	}
	
	if (this->m_matrix.getNbLines() < nb_lines)
	{
		delete[] m_index_line;
		m_index_line = new unsigned int [nb_lines];
	}
	
	m_line = E.m_line;
	m_column = E.m_column;
	
	m_matrix = E.m_matrix;
	
	m_set_of_vars = E.m_set_of_vars;
	
	return *this;
}


SubSystem& SubSystem::operator=(SubSystem && E)
{
	m_line = move(E.m_line);
	m_column = move(E.m_column);
	
	m_set_of_vars = move(E.m_set_of_vars);
	
	m_matrix = move(E.m_matrix);
	swap(m_index_col,E.m_index_col);
	swap(m_index_line,E.m_index_line);
	
	return *this;
}


SubSystem::~SubSystem()
{
	delete[] m_index_col;
	delete[] m_index_line;
}

SubSystem::SubSystem(Matrix<GFElement> && mat, SetOfVars const & set_of_vars, std::vector<MyPair> && v_line, std::vector<MyPair> && v_col) : 
m_matrix(mat), m_set_of_vars(set_of_vars), m_line(v_line), m_column(v_col)
{
	const unsigned int nb_cols = m_matrix.getNbColumns();
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	m_index_col = new unsigned int [nb_cols];
	m_index_line = new unsigned int [nb_lines];
}

SubSystem SubSystem::Extract(SubsetOfVars const& V) const
{
	const unsigned int nb_cols = m_matrix.getNbColumns();
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	
	unsigned int unwanted = 0;
	unsigned int last = nb_cols;
	
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (V.includes(m_column[i].first)) m_index_col[--last] = i;
		else m_index_col[unwanted++] = i;
	}
	
	unsigned int l = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (V.includes(m_line[i].first))
		{
			unsigned int c = 0;
			while ( c < unwanted )
			{
				const unsigned int real_c = m_index_col[c];
				if (m_matrix(i,real_c) != 0)
				{
					this->swapLineWithColumn(i,real_c);
					m_index_col[c] = m_index_col[--unwanted];
					m_index_col[unwanted] = real_c;
					goto loop_end;
				}
				else ++c;
			}
			m_index_line[l++] = i;
		}
loop_end:
		{	
		}
	}
	
	const unsigned int new_nb_lines = l;
	if (new_nb_lines > 0)
	{
		const unsigned int new_nb_cols = nb_cols - unwanted;
		
		vector<SubSystem::MyPair> line;
		line.reserve(new_nb_lines);
		for (unsigned int i = 0; i < new_nb_lines; ++i) line.emplace_back(m_line[m_index_line[i]]);
		
		vector<SubSystem::MyPair> column;
		column.reserve(new_nb_cols);
		for (unsigned int i = unwanted; i < nb_cols; ++i) column.emplace_back(m_column[m_index_col[i]]);
		
		Matrix<GFElement> matrix (new_nb_lines,new_nb_cols);
		for (unsigned int i = 0; i < new_nb_lines; ++i)
		{
			for (unsigned int j = 0; j < new_nb_cols; ++j) matrix(i,j) = m_matrix(m_index_line[i],m_index_col[j + unwanted]);
		}
		return SubSystem(move(matrix),m_set_of_vars,move(line),move(column));
	}
	else
	{
		SubSystem E;
		E.m_set_of_vars = m_set_of_vars;
		return E;
	}
}

SubSystem SubSystem::Extract(SubsetOfVars const & V1, SubsetOfVars const & V2) const
{
	const unsigned int nb_cols = m_matrix.getNbColumns();
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	
	unsigned int unwanted = 0;
	unsigned int last = nb_cols;
	
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (V1.includes(m_column[i].first) || V2.includes(m_column[i].first)) m_index_col[--last] = i;
		else m_index_col[unwanted++] = i;
	}
	
	unsigned int l = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (V1.includes(m_line[i].first) || V2.includes(m_line[i].first))
		{
			unsigned int c = 0;
			while ( c < unwanted )
			{
				const unsigned int real_c = m_index_col[c];
				if (m_matrix(i,real_c) != 0)
				{
					this->swapLineWithColumn(i,real_c);
					m_index_col[c] = m_index_col[--unwanted];
					m_index_col[unwanted] = real_c;
					goto loop_end;
				}
				else ++c;
			}
			m_index_line[l++] = i;
		}
loop_end:
		{	
		}
	}
	
	const unsigned int new_nb_lines = l;
	if (new_nb_lines > 0)
	{
		const unsigned int new_nb_cols = nb_cols - unwanted;
		
		vector<SubSystem::MyPair> line;
		line.reserve(new_nb_lines);
		for (unsigned int i = 0; i < new_nb_lines; ++i) line.emplace_back(m_line[m_index_line[i]]);
		
		vector<SubSystem::MyPair> column;
		column.reserve(new_nb_cols);
		for (unsigned int i = unwanted; i < nb_cols; ++i) column.emplace_back(m_column[m_index_col[i]]);
		
		Matrix<GFElement> matrix (new_nb_lines,new_nb_cols);
		for (unsigned int i = 0; i < new_nb_lines; ++i)
		{
			for (unsigned int j = 0; j < new_nb_cols; ++j) matrix(i,j) = m_matrix(m_index_line[i],m_index_col[j + unwanted]);
		}
		return SubSystem(move(matrix),m_set_of_vars,move(line),move(column));
	}
	else
	{
		SubSystem E;
		E.m_set_of_vars = m_set_of_vars;
		return E;
	}
}

void SubSystem::KeepOnly(SubsetOfVars const& V)
{
	const unsigned int nb_cols = m_matrix.getNbColumns();
	unsigned int nb_lines = m_matrix.getNbLines();
	
	
	list<unsigned int> unwanted;
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (!V.includes(m_column[i].first)) unwanted.emplace_back(i);
	}
	
	unsigned int l = 0;
	unsigned int i = 0;
	while (i < nb_lines)
	{
		if (V.includes(m_line[i].first))
		{
			const auto end = unwanted.end();
			auto it = unwanted.begin();
			while (it != end)
			{
				unsigned int const & real_c = *it;
				if (m_matrix(i,real_c) != 0)
				{
					this->swapLineWithColumn(i,real_c);
					unwanted.erase(it);
					goto loop_end;
				}
				else ++it;
			}
			++i;
		}
		else
		{
loop_end:
			{	
				--nb_lines;
				m_line[i] = move(m_line[nb_lines]);
				m_line.pop_back();
				m_matrix.swapLines(i,nb_lines);
			}
		}
	}
	
	if (!unwanted.empty())
	{
		const unsigned int new_nb_lines = nb_lines;
		const unsigned int new_nb_cols = nb_cols - unwanted.size();
		
		auto it_end = unwanted.crbegin();
		auto it_begin = unwanted.cbegin();
		unsigned int c = nb_cols;
		while (c > new_nb_cols && *it_begin < new_nb_cols)
		{
			--c;
			if (c != *it_end)
			{
				unsigned int const & new_c = *it_begin;
				for (unsigned int j = 0; j < new_nb_lines; ++j) m_matrix(j,new_c) = m_matrix(j,c);
				m_column[new_c] = move(m_column[c]);
				++it_begin;
			}
			else ++it_end;
		}
		m_column.resize(new_nb_cols);
		
		m_matrix.resize(new_nb_lines,new_nb_cols);
	}
	else m_matrix.resize(nb_lines,nb_cols);

	
}

void SubSystem::setKnownVars(SubsetOfVars const& V)
{	
	unsigned int nb_cols = m_matrix.getNbColumns();
	unsigned int nb_lines = m_matrix.getNbLines();
	
	unsigned int i = 0;
	
	while (i < nb_cols)
	{
		m_column[i].first-=V;
		if (m_column[i].first.is_empty())
		{
			--nb_cols;
			for (unsigned int l = 0; l < nb_lines; ++l) m_matrix(l,i) = m_matrix(l,nb_cols);
			m_column[i] = move(m_column[nb_cols]);
			m_column.pop_back();
		}
		else ++i;
	}
	
	i = 0;
	while (i < nb_lines)
	{
		m_line[i].first-=V;
		if (m_line[i].first.is_empty())
		{
			unsigned int c = 0;
			while (c < nb_cols)
			{
				const GFElement coef_pivot = m_matrix(i,c);
				if (coef_pivot != 0)
				{
					if (coef_pivot != 1)
					{
						for (unsigned int j = c; j < nb_cols; ++j) m_matrix(i,j) /= coef_pivot;
					}
					for (unsigned int l = 0; l < i; ++l)
					{
						const GFElement coef_line = m_matrix(l,c);
						if (coef_line != 0)
						{
							for (unsigned int j = 0; j < nb_cols; ++j) m_matrix(l,j)+= coef_line * m_matrix(i,j);
						}
					}
					for (unsigned int l = i+1; l < nb_lines; ++l)
					{
						const GFElement coef_line = m_matrix(l,c);
						if (coef_line != 0)
						{
							for (unsigned int j = 0; j < nb_cols; ++j) m_matrix(l,j)+= coef_line * m_matrix(i,j);
						}
					}
					m_line[i] = move(m_column[c]);
					--nb_cols;
					m_column[c] = move(m_column[nb_cols]);
					m_column.pop_back();
					for (unsigned int l = 0; l < nb_lines; ++l) m_matrix(l,c) = m_matrix(l,nb_cols);
					++i;
					goto loop_end;
				}
				else ++c;
			}
			--nb_lines;
			m_line[i] = move(m_line[nb_lines]);
			m_line.pop_back();
			m_matrix.swapLines(i,nb_lines);
		}
		else ++i;
loop_end:
		{
		}
	}
	
	m_matrix.resize(nb_lines,nb_cols);
}


#if 0 
int SubSystem::getNbSols(SubsetOfVars const& V) const
{
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	unsigned int l = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (V.includes(m_line[i].first)) m_index_line[l++] = i;
	}
	
	const unsigned int bound_l = l;
	
	if (bound_l == 0) return static_cast<int>(V.size());
	
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	unsigned int bound_c = 0;
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (!V.includes(m_column[i].first)) m_index_col[bound_c++] = i;
	}
	
	int nb_eqs = 0;
	
	for (unsigned int i = 0; i < bound_l; ++i)
	{
		const unsigned int real_l = m_index_line[i];
		for (unsigned int j = 0; j < bound_c; ++j) 
		{
			if (m_matrix(real_l,m_index_col[j]) != 0)
			{
				this->swapLineWithColumn(real_l,m_index_col[j]);
				m_index_col[j] = m_index_col[--bound_c];
				goto loop_end;
			}
		}
		++nb_eqs;
loop_end:
		{
		}
	}
	return static_cast<int>(V.size()) - nb_eqs;
	
}

#else

int SubSystem::getNbSols(SubsetOfVars const& V) const
{
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	unsigned int l = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (V.includes(m_line[i].first)) m_index_line[l++] = i;
	}
	
	const unsigned int bound_l = l;
	
	if (bound_l == 0) return static_cast<int>(V.size());
	
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	int nb_eqs = 0;
	
	for (unsigned int i = 0; i < bound_l; ++i)
	{
		const unsigned int real_l = m_index_line[i];
		for (unsigned int j = 0; j < nb_cols; ++j) 
		{
			if (m_matrix(real_l,j) != 0 && !V.includes(m_column[j].first))
			{
				this->swapLineWithColumn(real_l,j);
				goto loop_end;
			}
		}
		++nb_eqs;
loop_end:
		{
		}
	}
	return static_cast<int>(V.size()) - nb_eqs;
	
}

#endif

int SubSystem::getNbSolsNotBoth(SubsetOfVars const & V1, SubsetOfVars const & V2) const
{
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	unsigned int l = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (V1.includes(m_line[i].first) || V2.includes(m_line[i].first)) m_index_line[l++] = i;
	}
	
	const unsigned int bound_l = l;
	
	if (bound_l == 0) return static_cast<int>((V1+V2).size());
	
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	unsigned int bound_c = 0;
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (!V1.includes(m_column[i].first) && !V2.includes(m_column[i].first)) m_index_col[bound_c++] = i;
	}
	
	int nb_eqs = 0;
	
	for (unsigned int i = 0; i < bound_l; ++i)
	{
		const unsigned int real_l = m_index_line[i];
		for (unsigned int j = 0; j < bound_c; ++j) 
		{
			if (m_matrix(real_l,m_index_col[j]) != 0)
			{
				this->swapLineWithColumn(real_l,m_index_col[j]);
				m_index_col[j] = m_index_col[--bound_c];
				goto loop_end;
			}
		}
		++nb_eqs;
loop_end:
		{
		}
	}
	return static_cast<int>((V1 + V2).size()) - nb_eqs;
	
}

void SubSystem::setSetOfVars(SetOfVars const & s)
{
	assert(m_set_of_vars.includes(s));
		
	for (auto & x : m_line) x.first = x.first.convert(s);
	for (auto & x : m_column) x.first = x.first.convert(s);
	
	m_set_of_vars = s;
}


bool SubSystem::isLinear(Variable const & v) const
{
	unsigned int new_nb_lines = 0;
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (m_line[i].first.includes(v))
		{
			if (new_nb_lines == 1) return false;
			m_index_line[new_nb_lines++] = i;
		}
	}
	
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	if (new_nb_lines == 1)
	{
		const unsigned int index_line = m_index_line[0];
		for (unsigned int c = 0; c < nb_cols; ++c)
		{
			if (m_column[c].first.includes(v))
			{
				for (unsigned int l = 0; l < index_line; ++l)
				{
					if (m_matrix(l,c) != 0) return false;
				}
				for (unsigned int l = index_line+1; l < nb_lines; ++l)
				{
					if (m_matrix(l,c) != 0) return false;
				}
			}
		}
		return true;
	}
	else
	{
		unsigned int new_nb_cols = 0;
		for (unsigned int c = 0; c < nb_cols; ++c)
		{
			if (m_column[c].first.includes(v))
			{
				m_index_col[new_nb_cols++] = c;
			}
		}
		
		const unsigned int final_nb_cols = new_nb_cols;
		
		for (unsigned int i = 0; i < nb_lines; ++i)
		{
			for (unsigned int c = 0; c < final_nb_cols; ++c)
			{
				if (m_matrix(i,m_index_col[c]) != 0)
				{
					const unsigned int index_line = i;
					const unsigned int index_col = c;
					const GFElement coef_pivot = m_matrix(i,m_index_col[c]).getInverse();
					for (unsigned int l = index_line +1; l < nb_lines; ++l)
					{
						for (unsigned int k = 0; k < index_col; ++k)
						{
							if (m_matrix(l,m_index_col[k]) != 0) return false;
						}						
						const GFElement coef = coef_pivot * m_matrix(l,m_index_col[index_col]);
						for (unsigned int k = index_col+1; k < final_nb_cols; ++k)
						{
							if (m_matrix(l,m_index_col[k]) != coef*m_matrix(index_line,m_index_col[index_col])) return false;
						}
					}
					return true;
				}
			}
		}
		return true;
	}
}



bool SubSystem::isLinear(SubsetOfVars const & s) const
{
	const unsigned int s_size = s.size();
	unsigned int nb_eqs = 0;
	
	const unsigned int nb_lines = m_matrix.getNbLines();
	unsigned int bound_l_tmp = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (!m_line[i].first.shareElements(s)) m_index_line[bound_l_tmp++] = i;
		else
		{
			if (nb_eqs == s_size)
			{
				return false;
			}
			++nb_eqs;
		}
	}
	const unsigned int bound_l = bound_l_tmp;
	
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	if (nb_eqs == s_size)
	{
		for (unsigned int c = 0; c < nb_cols; ++c)
		{
			if (m_column[c].first.shareElements(s))
			{
				for (unsigned int l = 0; l < bound_l; ++l)
				{
					if (m_matrix(m_index_line[l],c) != 0)
					{
						return false;
					}
				}
			}
		}
		return true;
	}
	else
	{
		Matrix<GFElement> mat(bound_l, nb_cols);
		unsigned int real_c = 0;
		for (unsigned int c = 0; c < nb_cols; ++c)
		{
			if (m_column[c].first.shareElements(s))
			{
				for (unsigned int l = 0; l < bound_l; ++l) mat(l,real_c) = m_matrix(m_index_line[l],c);
				++real_c;
			}
		}
		mat.resize(bound_l,real_c);
		return mat.rank_smaller_than((s_size - nb_eqs) + 1);
	}
}


ostream& operator<<( ostream &flux, SubSystem const& sys)
{
	const unsigned int nb_lines = sys.m_matrix.getNbLines();
	const unsigned int nb_cols = sys.m_matrix.getNbColumns();
	flux << " The Matrix (" << nb_lines << "x" << nb_cols << "): " << endl;
	
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		for (unsigned int j = 0; j < i; ++j) flux << GFElement(0) << ' ';
		flux << GFElement(1) << ' ';
		for (unsigned int j = i+1; j < nb_lines; ++j) flux << GFElement(0) << ' ';
		for (unsigned int j = 0; j < nb_cols; ++j) flux << sys.m_matrix(i,j) << ' ';
		flux << endl;
	}
	flux << endl;
	
	flux << "The columns: " << endl;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		flux << " - column " << i << " : " << sys.m_line[i].first << endl;
	}
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		flux << " - column " << i + nb_lines << " : " << sys.m_column[i].first << endl;
	}
	flux << endl;
	
	
	return flux;
}

void SubSystem::swapLineWithColumn(const unsigned int l, const unsigned int c) const
{
	const unsigned int nb_lines = m_matrix.getNbLines();
	const unsigned int nb_cols = m_matrix.getNbColumns();
	assert(l < nb_lines && c < nb_cols);
	assert(m_matrix(l,c) != 0);
	
	const GFElement coef_inv = (m_matrix(l,c) == 1) ? 1 : m_matrix(l,c).getInverse();
	if (coef_inv != 1)
	{
		for (unsigned int i = 0; i < c; ++i) m_matrix(l,i) *= coef_inv;
		m_matrix(l,c) = coef_inv;
		for (unsigned int i = c+1; i < nb_cols; ++i) m_matrix(l,i) *= coef_inv;
	}
	
	for (unsigned int i = 0; i < l; ++i)
	{
		const GFElement coef_line = m_matrix(i,c);
		if (coef_line != 0)
		{
			for (unsigned int j = 0; j < c; ++j) m_matrix(i,j) -= coef_line * m_matrix(l,j); 
			m_matrix(i,c) = coef_line * coef_inv;
			for (unsigned int j = c+1; j < nb_cols; ++j) m_matrix(i,j) -= coef_line * m_matrix(l,j); 
		}
	}
	for (unsigned int i = l+1; i < nb_lines; ++i)
	{
		const GFElement coef_line = m_matrix(i,c);
		if (coef_line != 0)
		{
			for (unsigned int j = 0; j < c; ++j) m_matrix(i,j) -= coef_line * m_matrix(l,j); 
			m_matrix(i,c) = coef_line * coef_inv;
			for (unsigned int j = c+1; j < nb_cols; ++j) m_matrix(i,j) -= coef_line * m_matrix(l,j); 
		}
	}
	swap(m_line[l],m_column[c]);
}


vector<SubsetOfVars> SubSystem::getSubsetsOfVars() const
{
	vector<SubsetOfVars> v;
	v.reserve(m_line.size() + m_column.size());
	for (auto const & s : m_line) v.emplace_back(s.first);
	for (auto const & s : m_column) v.emplace_back(s.first);
	return v;
}

SubsetOfVars SubSystem::getVarsANDOR() const
{
	SubsetOfVars res = m_set_of_vars.empty_set();
	for (auto const & s : m_line)
	{
		if (s.second == -1) res += s.first;
	}
	for (auto const & s : m_column)
	{
		if (s.second == -1) res += s.first;
	}
	return res;
}

bool operator==(SubSystem const & E1, SubSystem const & E2)
{
	if (E1.m_line.size() != E2.m_line.size() || E1.m_column.size() != E2.m_column.size() || E1.m_set_of_vars != E2.m_set_of_vars) return false;
	//TODO	
	return true;
}

int SubSystem::getNbSols(Variable const & v) const
{
	Variable const * ptr_v = &v;
	return this->getNbSols(ptr_v, ptr_v+1);
}

void SubSystem::setKnownVars(Variable const& v)
{
	Variable const * ptr_v = &v;
	return this->setKnownVars(ptr_v, ptr_v+1);
}

void SubSystem::onlyBoth(SubsetOfVars const & s1, SubsetOfVars const & s2) const
{
	SubSystem E = this->Extract(s1 + s2);
	
	const unsigned int nb_cols = E.m_matrix.getNbColumns();
	const unsigned int nb_lines = E.m_matrix.getNbLines();
	
	unsigned int unwanted = 0;
	unsigned int last = nb_cols;
	
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (s1.includes(E.m_column[i].first)) E.m_index_col[--last] = i;
		else E.m_index_col[unwanted++] = i;
	}
	
	vector<unsigned int> final_lines;
	
	unsigned int l = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (s1.includes(E.m_line[i].first))
		{
			unsigned int c = 0;
			while ( c < unwanted )
			{
				const unsigned int real_c = E.m_index_col[c];
				if (E.m_matrix(i,real_c) != 0)
				{
					E.swapLineWithColumn(i,real_c);
					E.m_index_col[c] = E.m_index_col[--unwanted];
					E.m_index_col[unwanted] = real_c;
					E.m_index_line[l++] = i;
					goto loop_end;
				}
				else ++c;
			}
		}
		else E.m_index_line[l++] = i;
loop_end:
		{	
		}
	}
	
	const unsigned int new_nb_lines = l;
	
	unwanted = 0;
	last = nb_cols;
	
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (s2.includes(E.m_column[i].first)) E.m_index_col[--last] = i;
		else E.m_index_col[unwanted++] = i;
	}
	
	for (unsigned int l = 0; l < new_nb_lines; ++l)
	{
		const unsigned i = E.m_index_line[l];
		if (s2.includes(E.m_line[i].first))
		{
			unsigned int c = 0;
			while ( c < unwanted )
			{
				const unsigned int real_c = E.m_index_col[c];
				if (E.m_matrix(i,real_c) != 0)
				{
					E.swapLineWithColumn(i,real_c);
					E.m_index_col[c] = E.m_index_col[--unwanted];
					E.m_index_col[unwanted] = real_c;
					final_lines.push_back(i);
					goto loop_end2;
				}
				else ++c;
			}
		}
		else final_lines.push_back(i);
loop_end2:
		{	
		}
	}
	
	cout << "nb lines: " << final_lines.size() << endl;
	for (auto i : final_lines)
	{
		cout << "{" << E.m_line[i].first << "}";
		for (unsigned int c = 0; c < nb_cols; ++c)
		{
			if (E.m_matrix(i,c) != 0)
			{
				cout << " + " << E.m_matrix(i,c) << ".{" << E.m_column[c].first << "}";
			}
		}
		cout << endl;
	}
	
	
}

SubSystem SubSystem::extractRelated(PackOfVars const & lead)
{
	const unsigned int nb_cols = m_matrix.getNbColumns();
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	unsigned int lines_lead = 0;
	
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (m_line[i].first.shareElements(lead))
		{
			swapLines(lines_lead,i);
			++lines_lead;
		}
	}
	
	for (unsigned int i = 0; i < nb_cols; ++i)
	{
		if (m_column[i].first.shareElements(lead))
		{
			unsigned int j = lines_lead;
			while (j < nb_lines)
			{
				if (m_matrix(j,i) == 0) ++j;
				else
				{
					swapLineWithColumn(j,i);
					swapLines(lines_lead,j);
					++lines_lead;
					break;
				}
			}
		}
	}
	
	unsigned int cols_lead = 0;
	
	for (unsigned int c = 0; c < nb_cols; ++c)
	{
		unsigned int l = 0;
		while (l < lines_lead)
		{
			if (m_matrix(l,c) == 0) ++l;
			else
			{
				swapColumns(cols_lead,c);
				++cols_lead;
				break;
			}
		}
	}
	
	for (unsigned int c = 0; c < cols_lead; ++c)
	{
		unsigned int l = lines_lead;
		while (l < nb_lines)
		{
			if (m_matrix(l,c) != 0)
			{
				for (unsigned int cc = cols_lead; cc < nb_cols; ++cc)
				{
					if (m_matrix(l,cc) != 0)
					{
						swapColumns(cols_lead,cc);
						++cols_lead;
					}
				}
				swapLines(lines_lead,l);
				++lines_lead;
			}
			++l;
		}
	}
	
	Matrix<GFElement> new_matrix (lines_lead,cols_lead);
	for (unsigned int i = 0; i < lines_lead; ++i)
	{
		for (unsigned int j = 0; j < cols_lead; ++j) new_matrix(i,j) = m_matrix(i,j);
	}
	SetOfVars new_set_of_vars = m_set_of_vars;
	vector<SubSystem::MyPair> new_line;
	for (unsigned int i = 0; i < lines_lead; ++i) new_line.emplace_back(m_line[i]);
	vector<SubSystem::MyPair> new_column;
	for (unsigned int i = 0; i < cols_lead; ++i) new_column.emplace_back(m_column[i]);
	
	/*cout << lines_lead << 'x' << (lines_lead+cols_lead) << endl;
	cout << "m_line: ";
	for (unsigned int i = 0; i < lines_lead; ++i) cout << m_line[i] << ' ';
	cout << endl;
	cout << "m_column: ";
	for (unsigned int i = 0; i < cols_lead; ++i) cout << m_column[i] << ' ';
	cout << endl;
	cout << "mat: " << endl;
	cout << new_matrix;
	cout << endl;*/
	return SubSystem(move(new_matrix), move(new_set_of_vars), move(new_line), move(new_column));
}

/*void SubSystem::testRelated_tmp(unsigned int nb_lines, unsigned int nb_cols, list<PackOfVars> & my_list, PackOfVars start)
{
	unsigned int l = 0;
	while (l < nb_lines)
	{
		if (m_matrix(l,0) != 0)
		{
			unsigned int c = 1;
			while (c < nb_cols && m_matrix(l,c) == 0) ++c;
			if (c < nb_cols)
			{
				swapLineWithColumn(l,c);
				swapColumns(c,--nb_cols);
				{
					SubSystem E = *this;
					E.testRelated_tmp(nb_lines,nb_cols,my_list,start);
				}
				start += m_column[nb_cols];
				for (unsigned int i = 0; i < nb_lines; ++i) m_matrix(i,0) += m_matrix(i,nb_cols);
				++l;
			}
			else
			{
				swapLines(l,--nb_lines);
				start += m_line[nb_lines];
			}			
		}
		else ++l;
	}
	my_list.emplace_back(move(start));
}

list<PackOfVars> SubSystem::testRelated(void)
{
	const unsigned int nb_cols = m_matrix.getNbColumns();
	const unsigned int nb_lines = m_matrix.getNbLines();
	
	list<PackOfVars> my_list;
	
	return my_list;
	
	if (nb_lines == 0 || nb_cols == 0) return my_list;
	
	{
		unsigned int c = 0;
		while (m_matrix(0,c) == 0) ++c;
		swapLineWithColumn(0,c);
		swapColumns(0,c);
	}
	
	SubSystem E = *this;
	E.testRelated_tmp(nb_lines,nb_cols,my_list,m_column[0].empty_set());
	auto it = my_list.begin();
	while (it != my_list.end())
	{
		auto it2 = it;
		++it2;
		while (it2 != my_list.end())
		{
			if (it2->includes(*it)) it2 = my_list.erase(it2);
			else
			{
				if (it->includes(*it2))
				{
					it = my_list.erase(it);
					break;
				}
				else ++it2;
			}
		}
		if (it2 == my_list.end()) ++it;
	}
	cout << "my_list: " << my_list.size() << endl;
	return my_list;
}*/

bool SubSystem::involves(PackOfVars const & pack)
{
	for (auto const & p : m_column)
	{
		if (pack.shareElements(p.first)) return true;
	}
	for (auto const & p : m_line)
	{
		if (pack.shareElements(p.first)) return true;
	}
	
	return false;
}

bool SubSystem::propagateIfKnown(SubsetOfVars const & V)
{
	unsigned int nb_lines = m_matrix.getNbLines();
	
	unsigned int l = 0;
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		if (V.includes(m_line[i].first)) m_index_line[l++] = i;
	}
	
	const unsigned int size_V = V.size();
	
	if (size_V > l) return false;
	
	unsigned int bound_l = l;
	
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	while (bound_l > 0)
	{
		--bound_l;
		const unsigned int real_l = m_index_line[bound_l];
		for (unsigned int j = 0; j < nb_cols; ++j) 
		{
			if (m_matrix(real_l,j) != 0 && !V.includes(m_column[j].first))
			{
				this->swapLineWithColumn(real_l,j);
				--l;
				if (size_V > l) return false;
				goto loop_end;
			}
		}
		swapLines(real_l,--nb_lines);
loop_end:
		{
		}
	}
	
	m_matrix.resize(nb_lines,nb_cols);
	
	this->setKnownVars(V);
	
	return true;	
}

bool SubSystem::testRelatedPacks_tmp(PackOfVars const & lead, SubsetOfPacks const & packs, unsigned int size_lead) const
{
	unsigned int nb_lines_lead = 0;
	
	SubsetOfVars V = packs.getVars();
	
	const unsigned int nb_lines = m_matrix.getNbLines();
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	unsigned int nb_lines_V = 0;
	
	{
		unsigned int l = 0;
		for (unsigned int i = 0; i < nb_lines; ++i)
		{
			if (V.includes(m_line[i].first)) m_index_line[l++] = i;
		}
		
		const unsigned int bound_l = l;
		
		for (unsigned int i = 0; i < bound_l; ++i)
		{
			const unsigned int real_l = m_index_line[i];
			for (unsigned int j = 0; j < nb_cols; ++j) 
			{
				if (m_matrix(real_l,j) != 0 && !V.includes(m_column[j].first))
				{
					this->swapLineWithColumn(real_l,j);
					goto loop_end1;
				}
			}
			swapLines(nb_lines_V,real_l);
			++nb_lines_V;
loop_end1:
			{
			}
		}
	}
	
	V += lead;
	{
		unsigned int l = 0;
		unsigned int i = nb_lines_V;
		while (i < nb_lines && nb_lines + l >= i + size_lead)
		{
			if (V.includes(m_line[i].first)) m_index_line[l++] = i;
			++i;
		}
		
		if (nb_lines + l >= i + size_lead)
		{
			const unsigned int bound_l = l;
		
			for (i = 0; i < bound_l; ++i)
			{
				const unsigned int real_l = m_index_line[i];
				for (unsigned int j = 0; j < nb_cols; ++j) 
				{
					if (m_matrix(real_l,j) != 0 && !V.includes(m_column[j].first))
					{
						this->swapLineWithColumn(real_l,j);
						if (size_lead + i == bound_l) return false;
						goto loop_end2;
					}
				}
				if (size_lead == 1) return true;
				--size_lead;
loop_end2:
				{
				}
			}
		}
	}
	
	return false;
}


bool SubSystem::testAllKnown(SubsetOfPacks const & vars)
{
	auto vec = vars.get();
	if (vec.empty()) return true;
	unsigned int nb_lines = m_matrix.getNbLines();
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	unsigned int i = 0;
	unsigned int size_vec = vec.size();
	auto known = vec[0].empty_set();
	while (i < size_vec)
	{
		list<unsigned int> L;
		auto tmp = known + vec[i];
		for (unsigned int j = 0; j < nb_lines; ++j)
		{
			if (tmp.includes(m_line[j].first)) L.emplace_back(j);
		}
		const unsigned int size_i = vec[i].size();
		if (L.size() >= size_i)
		{
			auto it = L.begin();
			auto const it_end = L.end();
			while (it != it_end)
			{
				unsigned int c = 0;
				while (c < nb_cols && (m_matrix(*it,c) == 0 || tmp.includes(m_column[c].first))) ++c;
				if (c < nb_cols)
				{
					if (L.size() > size_i)
					{
						swapLineWithColumn(*it,c);
						it = L.erase(it);
					}
					else break;
				}
				else ++it;
			}
			if (it == L.end())
			{
				auto itr = L.crbegin();
				auto const itr_end = L.crend();
				while (itr != itr_end)
				{
					--nb_lines;
					swapLines(nb_lines,*itr);
					++itr;
				}
				known = move(tmp);
				vec[i] = move(vec[--size_vec]);
				i = 0;
			}
			else ++i;
		}
		else ++i;
	}
	
	return (size_vec == 0);
}


SubsetOfVars SubSystem::involvedFull(SubsetOfPacks const & myset) const
{
	auto vars = myset.getVars();
	auto tmp = vars.empty_set();
	
	const unsigned int nb_lines = m_matrix.getNbLines();
	const unsigned int nb_cols = m_matrix.getNbColumns();
	
	for (unsigned int l = 0; l < nb_lines; ++l)
	{
		if (m_line[l].second != 0 && m_line[l].first.shareElements(vars))
		{
			if (m_line[l].second == -1 && (m_line[l].first & vars).size() == 1) tmp += (m_line[l].first - vars);
			else tmp += m_line[l].first;
		}
	}
	
	for (unsigned int c = 0; c < nb_cols; ++c)
	{
		if (m_column[c].second != 0 && m_column[c].first.shareElements(vars))
		{
			unsigned int l = 0;
			while (l < nb_lines && m_matrix(l,c) == 0) ++l;
			if (l < nb_lines)
			{
				if (m_column[c].second == -1 && (m_column[c].first & vars).size() == 1) tmp += (m_column[c].first - vars);
				else tmp += m_column[c].first;
			}
		}
	}
	
	return tmp;
}

void SubSystem::swapLines(const unsigned int l1, const unsigned int l2) const
{
	m_matrix.swapLines(l1,l2);
	swap(m_line[l1],m_line[l2]);
}

void SubSystem::swapColumns(const unsigned int c1, const unsigned int c2) const
{
	m_matrix.swapColumns(c1,c2);
	swap(m_column[c1],m_column[c2]);
}

