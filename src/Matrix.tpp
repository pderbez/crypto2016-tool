/*
 * Matrix.tpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include <cassert>
 
template <typename T>
Matrix<T>::Matrix() : m_nb_lines(0), m_nb_cols(0), m_matrix_space(nullptr), m_matrix(nullptr)
{
}

template <typename T>
Matrix<T>::Matrix(const unsigned int nb_lines, const unsigned int nb_cols) : m_nb_lines(nb_lines), m_nb_cols(nb_cols)
{
	m_matrix_space = new T [nb_lines * nb_cols];
	m_matrix = new T * [nb_lines];
	
	for (unsigned int i = 0; i < nb_lines; ++i) m_matrix[i] = m_matrix_space + i*nb_cols;
}

template <typename T>
Matrix<T>::Matrix(const unsigned int nb_lines, const unsigned int nb_cols, T const value) : m_nb_lines(nb_lines), m_nb_cols(nb_cols)
{
	m_matrix_space = new T [nb_lines * nb_cols];
	m_matrix = new T * [nb_lines];
	
	for (unsigned int i = 0; i < nb_lines; ++i) m_matrix[i] = m_matrix_space + i*nb_cols;
	for (unsigned int i = 0; i < nb_lines * nb_cols; ++i) m_matrix_space[i] = value;

}

template <typename T>
Matrix<T>::Matrix(Matrix<T> const & mat) : m_nb_lines(mat.m_nb_lines), m_nb_cols(mat.m_nb_cols)
{
	const unsigned int nb_lines = mat.m_nb_lines;
	const unsigned int nb_cols = mat.m_nb_cols;
	
	T * const matrix_space = new T [nb_lines * nb_cols];
	T ** const matrix = new T * [nb_lines];
	
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		T * const src = mat.m_matrix[i];
		T * const dst = matrix_space + i*nb_cols;
		for (unsigned int j = 0; j < nb_cols; ++j) dst[j] = src[j];
		matrix[i] = dst;
	}
	
	m_matrix = matrix;
	m_matrix_space = matrix_space;
}

template <typename T>
Matrix<T>::Matrix(Matrix<T> && mat) : m_nb_lines(mat.m_nb_lines), m_nb_cols(mat.m_nb_cols), m_matrix_space(mat.m_matrix_space), m_matrix(mat.m_matrix)
{
	mat.m_nb_lines = 0;
	mat.m_matrix_space = nullptr;
	mat.m_matrix = nullptr;
}

template <typename T>		
Matrix<T>& Matrix<T>::operator=(Matrix<T> const & mat)
{
	const unsigned int nb_lines = mat.m_nb_lines;
	const unsigned int nb_cols = mat.m_nb_cols;
	
	T * const matrix_space = new T [nb_lines * nb_cols];
	T ** const matrix = new T * [nb_lines];
	
	for (unsigned int i = 0; i < nb_lines; ++i)
	{
		T * const src = mat.m_matrix[i];
		T * const dst = matrix_space + i*nb_cols;
		for (unsigned int j = 0; j < nb_cols; ++j) dst[j] = src[j];
		matrix[i] = dst;

	}
	
	delete[] m_matrix_space;
	delete[] m_matrix;
	
	m_matrix = matrix;
	m_matrix_space = matrix_space;
	
	m_nb_lines = mat.m_nb_lines;
	m_nb_cols = mat.m_nb_cols;
	
	return *this;
}

template <typename T>
Matrix<T>& Matrix<T>::operator=(Matrix<T> && mat)
{
	std::swap(m_matrix_space, mat.m_matrix_space);
	std::swap(m_matrix, mat.m_matrix);
	m_nb_lines = mat.m_nb_lines;
	mat.m_nb_lines = 0;
	m_nb_cols = mat.m_nb_cols;
	
	return *this;
}

template <typename T>
void swap(Matrix<T> & mat1, Matrix<T> & mat2)
{
	std::swap(mat1.m_nb_lines, mat2.m_nb_lines);
	std::swap(mat1.m_nb_cols, mat2.m_nb_cols);
	std::swap(mat1.m_matrix_space, mat2.m_matrix_space);
	std::swap(mat1.m_matrix, mat2.m_matrix);
}


template <typename T>
Matrix<T>::~Matrix()
{
	delete[] m_matrix;
	delete[] m_matrix_space;
}

template <typename T>
void Matrix<T>::swapLines(const unsigned int l1, const unsigned int l2)
{
	assert(l1 < this->getNbLines() && l2 < this->getNbLines());
	std::swap(m_matrix[l1],m_matrix[l2]);
}

template <typename T>
void Matrix<T>::swapColumns(const unsigned int c1, const unsigned int c2)
{
	assert(c1 < this->getNbColumns() && c2 < this->getNbColumns());
	const unsigned int nb_lines = getNbLines();
	for (unsigned int i = 0; i < nb_lines; ++i) std::swap(m_matrix[i][c1],m_matrix[i][c2]);
}

template <typename T>
unsigned int Matrix<T>::Gauss()
{
	const unsigned int nb_lines = m_nb_lines;
	const unsigned int nb_cols = m_nb_cols;
	
	unsigned int pivot = 0;
	for (unsigned int c = 0; c < nb_cols; ++c)
	{
		unsigned int pivot_tmp = pivot;
		while (pivot_tmp < nb_lines)
		{
			T const coef_pivot = m_matrix[pivot_tmp][c];
			if (coef_pivot != 0)
			{
				T * const __restrict__ src = m_matrix[pivot_tmp];
				for (unsigned int l = pivot_tmp+1; l < nb_lines; ++l)
				{
					T const coef_line = m_matrix[l][c];
					if (coef_line != 0)
					{
						T const coef = coef_line / coef_pivot;
						T * const __restrict__ dst = m_matrix[l];
						for (unsigned int i = c; i < nb_cols; ++i) dst[i] += coef * src[i];
					}
				}
				this->swapLines(pivot_tmp, pivot);
				++pivot;
				break;
			}
			else ++pivot_tmp;
		}
	}
	return pivot;
}

template <typename T>
unsigned int Matrix<T>::GaussJordan()
{
	const unsigned int nb_lines = m_nb_lines;
	const unsigned int nb_cols = m_nb_cols;
	
	unsigned int pivot = 0;
	for (unsigned int c = 0; c < nb_cols; ++c)
	{
		unsigned int pivot_tmp = pivot;
		while (pivot_tmp < nb_lines)
		{
			T const coef_pivot = m_matrix[pivot_tmp][c];
			if (coef_pivot != 0)
			{
				T * const __restrict__ src = m_matrix[pivot_tmp];
				for (unsigned int l = 0; l < pivot; ++l)
				{
					T const coef_line = m_matrix[l][c];
					if (coef_line != 0)
					{
						T const coef = coef_line / coef_pivot;
						T * const __restrict__ dst = m_matrix[l];
						for (unsigned int i = c; i < nb_cols; ++i) dst[i] += coef * src[i];
					}
				}
				for (unsigned int l = pivot_tmp+1; l < nb_lines; ++l)
				{
					T const coef_line = m_matrix[l][c];
					if (coef_line != 0)
					{
						T const coef = coef_line / coef_pivot;
						T * const __restrict__ dst = m_matrix[l];
						for (unsigned int i = c; i < nb_cols; ++i) dst[i] += coef * src[i];
					}
				}
				this->swapLines(pivot_tmp, pivot);
				++pivot;
				break;
			}
			else ++pivot_tmp;
		}
	}
	return pivot;
}

template <typename T>
unsigned int Matrix<T>::rank() const
{
	Matrix<T> mat(*this);
	return mat.rank();
}

template <typename T>
unsigned int Matrix<T>::rank()
{
	return this->Gauss();
}

template <typename T>
template <typename Iter>
unsigned int Matrix<T>::rankColumns(Iter it, Iter const end)
{
	const unsigned int nb_lines = m_nb_lines;
	const unsigned int nb_cols = m_nb_cols;
	
	unsigned int pivot = 0;
	for (; it != end; ++it)
	{
		unsigned int pivot_tmp = pivot;
		while (pivot_tmp < nb_lines)
		{
			T const coef_pivot = m_matrix[pivot_tmp][*it];
			if (coef_pivot != 0)
			{
				T * const src = m_matrix[pivot_tmp];
				for (unsigned int l = pivot_tmp+1; l < nb_lines; ++l)
				{
					T const coef_line = m_matrix[l][*it];
					if (coef_line != 0)
					{
						T const coef = coef_line / coef_pivot;
						T * const dst = m_matrix[l];
						for (unsigned int i = 0; i < nb_cols; ++i) dst[i] += coef * src[i];
					}
				}
				this->swapLines(pivot_tmp, pivot);
				++pivot;
				break;
			}
			else ++pivot_tmp;
		}
	}
	return pivot;
}

template <typename T>
unsigned int Matrix<T>::rankColumns(const unsigned int cols)
{
	const unsigned int nb_lines = m_nb_lines;
	const unsigned int nb_cols = m_nb_cols;
	
	unsigned int pivot = 0;
	for (unsigned int c = 0; c < cols; ++c)
	{
		unsigned int pivot_tmp = pivot;
		while (pivot_tmp < nb_lines)
		{
			T const coef_pivot = m_matrix[pivot_tmp][c];
			if (coef_pivot != 0)
			{
				T * const __restrict__ src = m_matrix[pivot_tmp];
				for (unsigned int l = pivot_tmp+1; l < nb_lines; ++l)
				{
					T const coef_line = m_matrix[l][c];
					if (coef_line != 0)
					{
						T const coef = coef_line / coef_pivot;
						T * const __restrict__ dst = m_matrix[l];
						for (unsigned int i = c; i < nb_cols; ++i) dst[i] += coef * src[i];
					}
				}
				this->swapLines(pivot_tmp, pivot);
				++pivot;
				break;
			}
			else ++pivot_tmp;
		}
	}
	return pivot;
}

template <typename T>
template <typename Iter>
unsigned int Matrix<T>::rankLines(Iter it, Iter const end)
{
	const unsigned int nb_lines = m_nb_lines;
	const unsigned int nb_cols = m_nb_cols;
	
	unsigned int rank = 0;
	for (unsigned int c = 0; c < nb_cols; ++c)
	{
		auto it_tmp = it;
		while (it_tmp != end)
		{
			T const coef_pivot = m_matrix[*it_tmp][c];
			if (coef_pivot != 0)
			{
				T * const src = m_matrix[*it_tmp];
				auto it_l = it_tmp;
				for (++it_l; it_l != end; ++it_l)
				{
					T const coef_line = m_matrix[*it_l][c];
					if (coef_line != 0)
					{
						T const coef = coef_line / coef_pivot;
						T * const dst = m_matrix[*it_l];
						for (unsigned int i = c; i < nb_cols; ++i) dst[i] += coef * src[i];
					}
				}
				this->swapLines(*it_tmp, *it);
				++it;
				++rank;
				if (it == end) return rank;
			}
			else ++it_tmp;
		}
	}
	return rank;
}

template <typename T>
void Matrix<T>::resize(const unsigned int nb_lines, const unsigned int nb_cols)
{
	if (nb_lines > m_nb_lines || nb_cols > m_nb_cols)
	{
		T ** const matrix = new T * [nb_lines];
		T * const space = new T [nb_lines * nb_cols];
		for (unsigned int i = 0; i < nb_lines; ++i) matrix[i] = space + i*nb_cols;
		
		for (unsigned int i = 0; i < m_nb_lines; ++i)
		{
			for (unsigned int j = 0; j < m_nb_cols; ++j) matrix[i][j] = m_matrix[i][j];
		}
		
		delete[] m_matrix;
		delete[] m_matrix_space;
		
		m_matrix = matrix;
		m_matrix_space = space;
	}
	m_nb_lines = nb_lines;
	m_nb_cols = nb_cols;
}

template <typename T>
std::ostream& operator<<( std::ostream &flux, Matrix<T> const& mat)
{
	for (unsigned int i = 0; i < mat.getNbLines(); ++i)
	{
		for (unsigned int j = 0; j < mat.getNbColumns(); ++j) flux << mat(i,j) << ' ' ;
		flux << std::endl;
	}
	flux << std::endl;
	return flux;
}

template <typename T>
bool Matrix<T>::rank_smaller_than(const unsigned int r)
{
	const unsigned int nb_lines = m_nb_lines;
	const unsigned int nb_cols = m_nb_cols;
	
	unsigned int pivot = 0;
	for (unsigned int c = 0; c < nb_cols; ++c)
	{
		unsigned int pivot_tmp = pivot;
		while (pivot_tmp < nb_lines)
		{
			T const coef_pivot = m_matrix[pivot_tmp][c];
			if (coef_pivot != 0)
			{
				if (pivot == r - 1) return false;
				T * const __restrict__ src = m_matrix[pivot_tmp];
				for (unsigned int l = pivot_tmp+1; l < nb_lines; ++l)
				{
					T const coef_line = m_matrix[l][c];
					if (coef_line != 0)
					{
						T const coef = coef_line / coef_pivot;
						T * const __restrict__ dst = m_matrix[l];
						for (unsigned int i = c; i < nb_cols; ++i) dst[i] += coef * src[i];
					}
				}
				this->swapLines(pivot_tmp, pivot);
				++pivot;
				break;
			}
			else ++pivot_tmp;
		}
	}
	return true;
}

