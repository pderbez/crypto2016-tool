/*
 * SetOf.tpp
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include <utility>
#include <cassert>

template <typename T>
std::vector<std::map<T, SubsetOf<T>>> SetOf<T>::v_sets_of_T(1);

template <typename T>
std::vector<unsigned int> SetOf<T>::v_nb_refs(1,0);

template <typename T>
std::stack<unsigned int> SetOf<T>::sk_empty_cases;

template <typename T>
template <typename Iter>
SubsetOf<T> SetOf<T>::getSubset(Iter const begin, Iter const end) const
{
	SubsetOf<T> res = this->empty_set();
	for (auto it = begin; it != end ; ++it) res += this->getSubset(*it);
	return res;
}

template <typename T>
SetOf<T>::SetOf() : m_set_index(0)
{
}

template <typename T>
SetOf<T>::SetOf(SetOf<T> const & S) : m_set_index(S.m_set_index)
{
	++v_nb_refs[m_set_index];
}

template <typename T>
SetOf<T>::SetOf(SetOf<T> && S) : m_set_index(S.m_set_index)
{
	S.m_set_index = 0;
}

template <typename T>
SetOf<T> & SetOf<T>::operator=(SetOf<T> const & S)
{
	if (m_set_index != S.m_set_index)
	{
		if (m_set_index != 0)
		{
			--v_nb_refs[m_set_index];
			if (v_nb_refs[m_set_index] == 0)
			{
				v_sets_of_T[m_set_index].clear();
				sk_empty_cases.push(m_set_index);
			}
		}
		++v_nb_refs[S.m_set_index];
		m_set_index = S.m_set_index;
	}
	return *this;
}

template <typename T>
SetOf<T> & SetOf<T>::operator=(SetOf<T> && S)
{
	std::swap(m_set_index,S.m_set_index);
	return *this;
}

template <typename T>		
SetOf<T>::~SetOf()
{
	if (m_set_index != 0)
	{
		--v_nb_refs[m_set_index];
		if (v_nb_refs[m_set_index] == 0)
		{
			v_sets_of_T[m_set_index].clear();
			sk_empty_cases.push(m_set_index);
		}
	}
}

template <typename T>
typename SetOf<T>::size_type SetOf<T>::size() const
{
	return v_sets_of_T[m_set_index].size();
}

template <typename T>	
SubsetOf<T> const & SetOf<T>::getSubset(T const & v) const
{
	auto const & my_map = v_sets_of_T[m_set_index];
	auto it = my_map.find(v);
	assert( it != my_map.end());
	return it->second;
}

template <typename T>
SubsetOf<T> SetOf<T>::empty_set() const
{
	const unsigned int nb_elements = this->size();
	const unsigned int nb_blocks = (nb_elements % SubsetOf<T>::block_size == 0) ? (nb_elements / SubsetOf<T>::block_size) : (nb_elements / SubsetOf<T>::block_size) + 1;
	typename SubsetOf<T>::block_type * const storage = new typename SubsetOf<T>::block_type [nb_blocks];
	for (unsigned int i = 0; i < nb_blocks; ++i) storage[i] = 0;
	return SubsetOf<T>(storage,nb_blocks,m_set_index);
}

template <typename T>
bool operator==(SetOf<T> const & s1, SetOf<T> const & s2)
{
	if (s1.m_set_index == s2.m_set_index) return true;
	auto const & my_map1 = SetOf<T>::v_sets_of_T[s1.m_set_index];
	auto const & my_map2 = SetOf<T>::v_sets_of_T[s2.m_set_index];
	if (my_map1.size() == my_map2.size())
	{
		auto it1 = my_map1.cbegin();
		auto end = my_map1.cend();
		auto it2 = my_map2.cbegin();
		while (it1 != end)
		{
			if (it1->first == it2->first)
			{
				++it1;
				++it2;
			}
			else return false;
		}
		return true;
	}
	else return false;
}

template <typename T>
bool SetOf<T>::includes(SetOf<T> const & s) const
{
	auto const & my_map_this = v_sets_of_T[m_set_index];
	auto const & my_map_s = v_sets_of_T[s.m_set_index];
	
	if (my_map_s.size() > my_map_this.size()) return false;
	
	auto it_this = my_map_this.begin();
	auto end_this = my_map_this.end();
	auto it_s = my_map_s.begin();
	auto end_s = my_map_s.end();
	
	while (it_s != end_s)
	{
		while (it_this != end_this && it_this->first < it_s->first) ++it_this;
		if (it_this == end_this) return false;
		if (it_this->first != it_s->first) return false;
		++it_s;
		++it_this;
	}
	return true;
}

template <typename T>
std::vector<T> SetOf<T>::get() const
{
	auto const & my_map = v_sets_of_T[m_set_index];
	std::vector<T> my_vec;
	my_vec.reserve(my_map.size());
	for (auto & x : my_map) my_vec.push_back(x.first);
	return my_vec;
}

template <typename T>
SetOf<T>::SetOf(unsigned int const set_index) : m_set_index(set_index)
{
	assert(m_set_index < v_sets_of_T.size());
	assert(v_nb_refs[m_set_index] > 0);
	++v_nb_refs[m_set_index];
}

template <typename T>		
typename SetOf<T>::const_iterator SetOf<T>::cbegin() const
{
	return v_sets_of_T[m_set_index].cbegin();
}

template <typename T>
typename SetOf<T>::const_iterator SetOf<T>::cend() const
{
	return v_sets_of_T[m_set_index].cend();
}

template <typename T>
template <typename Iter> 
bool SubsetOf<T>::includes(Iter begin, Iter end) const
{
	return this->includes(SetOf<T>(m_set_index).getSubset(begin,end));
}

template <typename T>
SubsetOf<T>::SubsetOf() : m_storage(nullptr), m_nb_blocks(0), m_set_index(0)
{
}

template <typename T>
SubsetOf<T>::SubsetOf(block_type * const storage, const unsigned int nb_blocks, unsigned int const set_index) : m_storage(storage), m_nb_blocks(nb_blocks), m_set_index(set_index)
{
}

template <typename T>
SubsetOf<T>::SubsetOf(SubsetOf<T> const & s) : m_storage(new block_type [s.m_nb_blocks]), m_nb_blocks(s.m_nb_blocks), m_set_index(s.m_set_index)
{
	for (unsigned int i = 0; i < s.m_nb_blocks; ++i) m_storage[i] = s.m_storage[i];
}

template <typename T>
SubsetOf<T>::SubsetOf(SubsetOf<T> && s) : m_storage(s.m_storage), m_nb_blocks(s.m_nb_blocks), m_set_index(s.m_set_index)
{
	s.m_storage = nullptr;
	s.m_set_index = 0;
}

template <typename T>
SubsetOf<T> & SubsetOf<T>::operator=(SubsetOf<T> const & s)
{
	if (m_set_index == s.m_set_index)
	{
		for (unsigned int i = 0; i < s.m_nb_blocks; ++i) m_storage[i] = s.m_storage[i];
	}
	else
	{
		block_type * const storage = new block_type [s.m_nb_blocks];
		for (unsigned int i = 0; i < s.m_nb_blocks; ++i) storage[i] = s.m_storage[i];
		delete[] m_storage;
		m_storage = storage;
		m_nb_blocks = s.m_nb_blocks;
		m_set_index = s.m_set_index;
	}
	return *this;
}

template <typename T>
SubsetOf<T> & SubsetOf<T>::operator=(SubsetOf<T> && s)
{
	std::swap(m_storage, s.m_storage);
	m_nb_blocks = s.m_nb_blocks;
	m_set_index = s.m_set_index;
	s.m_set_index = 0;
	return *this;
}

template <typename T>	
SubsetOf<T>::~SubsetOf()
{
	delete[] m_storage;
}

template <typename T>	
SubsetOf<T> & SubsetOf<T>::operator+=(SubsetOf<T> const & s)
{
	assert( s.m_set_index == m_set_index && s.m_nb_blocks == m_nb_blocks);
	for (unsigned int i = 0; i < s.m_nb_blocks; ++i) m_storage[i] |= s.m_storage[i];
	return *this;
}

template <typename T>
SubsetOf<T> & SubsetOf<T>::operator-=(SubsetOf<T> const & s)
{
	assert( s.m_set_index == m_set_index && s.m_nb_blocks == m_nb_blocks);
	for (unsigned int i = 0; i < s.m_nb_blocks; ++i) m_storage[i] &= ~s.m_storage[i];
	return *this;
}

template <typename T>
SubsetOf<T> & SubsetOf<T>::operator&=(SubsetOf<T> const & s)
{
	assert( s.m_set_index == m_set_index && s.m_nb_blocks == m_nb_blocks);
	for (unsigned int i = 0; i < s.m_nb_blocks; ++i) m_storage[i] &= s.m_storage[i];
	return *this;
}

template <typename T>	
bool SubsetOf<T>::includes(T const & v) const
{
	return this->includes(this->getSubset(v));
}


template <typename T>
unsigned int SubsetOf<T>::size() const
{
	unsigned int sum = 0;
	for (unsigned int i = 0; i < m_nb_blocks; ++i)
	{
		auto x = m_storage[i];
		while (x != 0)
		{
			sum += x%2;
			x >>= 1;
		}
	}
	return sum;
}

template <typename T>	
SubsetOf<T> operator+(SubsetOf<T> const & s1, SubsetOf<T> const & s2)
{
	assert( s1.m_set_index == s2.m_set_index && s1.m_nb_blocks == s2.m_nb_blocks);
	typename SubsetOf<T>::block_type * const storage = new typename SubsetOf<T>::block_type [s1.m_nb_blocks];
	for (unsigned int i = 0; i < s1.m_nb_blocks; ++i) storage[i] = s1.m_storage[i] | s2.m_storage[i];
	return SubsetOf<T>(storage,s1.m_nb_blocks,s1.m_set_index);
}

template <typename T>
SubsetOf<T> operator-(SubsetOf<T> const & s1, SubsetOf<T> const & s2)
{
	assert( s1.m_set_index == s2.m_set_index && s1.m_nb_blocks == s2.m_nb_blocks);
	typename SubsetOf<T>::block_type * const storage = new typename SubsetOf<T>::block_type [s1.m_nb_blocks];
	for (unsigned int i = 0; i < s1.m_nb_blocks; ++i) storage[i] = s1.m_storage[i] & ~s2.m_storage[i];
	return SubsetOf<T>(storage,s1.m_nb_blocks,s1.m_set_index);
}

template <typename T>
SubsetOf<T> operator&(SubsetOf<T> const & s1, SubsetOf<T> const & s2)
{
	assert( s1.m_set_index == s2.m_set_index && s1.m_nb_blocks == s2.m_nb_blocks);
	typename SubsetOf<T>::block_type * const storage = new typename SubsetOf<T>::block_type [s1.m_nb_blocks];
	for (unsigned int i = 0; i < s1.m_nb_blocks; ++i) storage[i] = s1.m_storage[i] & s2.m_storage[i];
	return SubsetOf<T>(storage,s1.m_nb_blocks,s1.m_set_index);
}

template <typename T>
bool SubsetOf<T>::includes(SubsetOf<T> const & s) const
{
	assert( m_set_index == s.m_set_index && m_nb_blocks == s.m_nb_blocks);
	for (unsigned int i = 0; i < s.m_nb_blocks; ++i)
	{
		if ((m_storage[i] & s.m_storage[i]) != s.m_storage[i]) return false;
	}
	return true;
}


template <typename T>
bool SubsetOf<T>::is_empty() const
{
	for (unsigned int i = 0; i < m_nb_blocks; ++i)
	{
		if (m_storage[i] != 0) return false;
	}
	return true;
}

template <typename T>
bool operator!=(SubsetOf<T> const & s1, SubsetOf<T> const & s2)
{
	assert( s1.m_set_index == s2.m_set_index && s1.m_nb_blocks == s2.m_nb_blocks);
	for (unsigned int i = 0; i < s1.m_nb_blocks; ++i)
	{
		if (s1.m_storage[i] != s2.m_storage[i]) return true;
	}
	return false;
}

template <typename T>
bool operator<(SubsetOf<T> const & s1, SubsetOf<T> const & s2)
{
	assert(s1.m_set_index == s2.m_set_index);
	const unsigned int loop_bound = s1.m_nb_blocks;
	for (unsigned int i = 0; i < loop_bound; ++i)
	{
		if (s1.m_storage[i] != s2.m_storage[i]) return (s1.m_storage[i] < s2.m_storage[i]);
	}
	return false;
}

template <typename T>
SubsetOf<T> operator~(SubsetOf<T> const & s)
{
	typename SubsetOf<T>::block_type * const storage = new typename SubsetOf<T>::block_type [s.m_nb_blocks];
	for (unsigned int i = 0; i < s.m_nb_blocks; ++i) storage[i] = ~s.m_storage[i];
	return SubsetOf<T>(storage,s.m_nb_blocks,s.m_set_index);	
}

template <typename T>
std::vector<T> SubsetOf<T>::get() const
{
	std::vector<T> my_vec;
	auto it = SetOf<T>::v_sets_of_T[m_set_index].begin();
	for (unsigned int i = 0; i < m_nb_blocks; ++i)
	{
		auto x = m_storage[i];
		unsigned int j = block_size;
		while (x != 0)
		{
			if  (x%2 == 1) my_vec.emplace_back(it->first);
			x >>= 1;
			++it;
			--j;
		}
		std::advance(it,j);
	}
	return my_vec;
}

template <typename T>
SubsetOf<T> const & SubsetOf<T>::getSubset(T const & v) const
{
	auto const & my_map = SetOf<T>::v_sets_of_T[m_set_index];
	assert(my_map.find(v) != my_map.end());
	return my_map.at(v);
}

template <typename T>
SubsetOf<T> & SubsetOf<T>::operator+=(T const & v)
{
	return *this += this->getSubset(v);
}

template <typename T>
SubsetOf<T> & SubsetOf<T>::operator-=(T const & v)
{
	return *this -= this->getSubset(v);
}

template <typename T>
SubsetOf<T> & SubsetOf<T>::operator&=(T const & v)
{
	return *this &= this->getSubset(v);
}

template <typename T>
SubsetOf<T> SubsetOf<T>::convert(SetOf<T> const & s) const
{
	assert(SetOf<T>(m_set_index).includes(s));
	std::vector<T> my_vec = this->get();
	return s.getSubset(my_vec.begin(),my_vec.end());
}

template <typename T>
std::ostream& SubsetOf<T>::print(std::ostream &flux) const
{
	auto it = SetOf<T>::v_sets_of_T[m_set_index].begin();
	for (unsigned int i = 0; i < m_nb_blocks; ++i)
	{
		auto x = m_storage[i];
		unsigned int j = 0;
		for (; j < block_size; ++j)
		{
			if  (x%2 == 1) flux << it->first << ' ';
			x >>= 1;
			++it;
		}
	}
	return flux;
}

template <typename T>
std::ostream& operator<<( std::ostream &flux, SubsetOf<T> const & s)
{
	return s.print(flux);
}

template <typename T>
SubsetOf<T> operator+(SubsetOf<T> const & s, T const & v)
{
	return s + s.getSubset(v);
}

template <typename T>
SubsetOf<T> operator-(SubsetOf<T> const & s, T const & v)
{
	return s - s.getSubset(v);
}

template <typename T>
SubsetOf<T> operator&(SubsetOf<T> const & s, T const & v)
{
	return s & s.getSubset(v);
}

template <typename T>
SubsetOf<T> operator-(T const & v, SubsetOf<T> const & s)
{
	return s.getSubset(v) - s;
}


template <typename T>
template <typename Iter>
SetOf<T>::SetOf(Iter const begin, Iter const end)
{
	{
		std::map<T,SubsetOf<T>> my_map;
		for (auto it = begin; it != end ; ++it) my_map[*it];
		
		if (sk_empty_cases.empty())
		{
			m_set_index = v_sets_of_T.size();
			v_sets_of_T.emplace_back(move(my_map));
			v_nb_refs.push_back(1);
		}
		else
		{
			m_set_index = sk_empty_cases.top();
			sk_empty_cases.pop();
			v_sets_of_T[m_set_index] = move(my_map);
			v_nb_refs[m_set_index] = 1;
		}
	}
	
	auto & my_map = v_sets_of_T[m_set_index];
	
	const unsigned int nb_elements = my_map.size();
	const unsigned int nb_blocks = (nb_elements % SubsetOf<T>::block_size == 0) ? (nb_elements / SubsetOf<T>::block_size) : (nb_elements / SubsetOf<T>::block_size) + 1;
	unsigned int index_block = 0;
	typename SubsetOf<T>::block_type x = 1;
	
	for (auto it = my_map.begin(); it != my_map.end() ; ++it)
	{
		typename SubsetOf<T>::block_type * const storage = new typename SubsetOf<T>::block_type [nb_blocks];
		for (unsigned int i = 0; i < nb_blocks; ++i) storage[i] = 0;
		storage[index_block] = x;
		x <<= 1;
		if (x == 0)
		{
			x = 1;
			++index_block;
		}
		it->second = SubsetOf<T>(storage, nb_blocks, m_set_index);
	}
}

template <typename T> 
SubsetOf<T> operator|(SubsetOf<T> const & s1, SubsetOf<T> const & s2)
{
	return s1 + s2;
}

template <typename T> 
SubsetOf<T> operator|(SubsetOf<T> const & s, T const & v) 
{
	return s + v;
}

template <typename T> 
SubsetOf<T> operator+(T const & v, SubsetOf<T> const & s)
{
	return s + v;
}

template <typename T> 
SubsetOf<T> operator&(T const & v, SubsetOf<T> const & s) 
{
	return s & v;
}

template <typename T> 
SubsetOf<T> operator|(T const & v, SubsetOf<T> const & s)
{
	return s + v;
}

template <typename T> 		
bool operator==(SubsetOf<T> const & s1, SubsetOf<T> const & s2)
{
	return !(s1 != s2);
}

template <typename T>
bool SubsetOf<T>::shareElements(SubsetOf<T> const & s) const
{
	assert(m_set_index == s.m_set_index);
	for (unsigned int i = 0; i < m_nb_blocks; ++i)
	{
		if ((m_storage[i] & s.m_storage[i]) != 0) return true;
	}
	return false;
}

template <typename T>
SetOf<T> SubsetOf<T>::getSetOf() const
{
	return SetOf<T>(m_set_index);
}

template <typename T>
SubsetOf<T> SubsetOf<T>::empty_set() const
{
	typename SubsetOf<T>::block_type * const storage = new typename SubsetOf<T>::block_type [m_nb_blocks];
	for (unsigned int i = 0; i < m_nb_blocks; ++i) storage[i] = 0;
	return SubsetOf<T>(storage,m_nb_blocks,m_set_index);
}

template <typename T>
T SubsetOf<T>::getFirst() const
{
	auto it = SetOf<T>::v_sets_of_T[m_set_index].begin();
	for (unsigned int i = 0; i < m_nb_blocks; ++i)
	{
		auto x = m_storage[i];
		unsigned int j = block_size;
		while (x != 0)
		{
			if  (x%2 == 1) return it->first;
			x >>= 1;
			++it;
			--j;
		}
		std::advance(it,j);
	}
	exit(EXIT_FAILURE);
}

template <typename T>
void SubsetOf<T>::clear()
{
	for (unsigned int i = 0; i < m_nb_blocks; ++i) m_storage[i] = 0;
}

template <typename T> 
bool operator!=(SetOf<T> const & s1, SetOf<T> const & s2)
{
	return !(s1 == s2);
}
