/*
 * PackOfVars.h
 * 
 * Copyright 2014 pderbez <pderbez@pderbez-VirtualBox>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef DEF_PACK_OF_VARS
#define DEF_PACK_OF_VARS

#include <vector>
#include <ostream>

#include "SetOf.h"
#include "SetOfVars.h"

typedef SubsetOfVars PackOfVars;

class SubsetOfPacks : public SubsetOf<PackOfVars>
{
	public:
		SubsetOfPacks();
		SubsetOfPacks(SubsetOfPacks const &);
		SubsetOfPacks(SubsetOfPacks &&);
		SubsetOfPacks & operator=(SubsetOfPacks const &);
		SubsetOfPacks & operator=(SubsetOfPacks &&);
		~SubsetOfPacks();
		
		SubsetOfPacks(SubsetOf<PackOfVars> const &);
		SubsetOfPacks(SubsetOf<PackOfVars> &&);
		SubsetOfPacks & operator=(SubsetOf<PackOfVars> const &);
		SubsetOfPacks & operator=(SubsetOf<PackOfVars> &&);
		
		SubsetOfPacks & operator+=(SubsetOfPacks const &);
		SubsetOfPacks & operator-=(SubsetOfPacks const &);
		SubsetOfPacks & operator&=(SubsetOfPacks const &);
		SubsetOfPacks & operator|=(SubsetOfPacks const & s) {return *this+=s;};
		
		SubsetOfPacks & operator+=(SubsetOf<PackOfVars> const &);
		SubsetOfPacks & operator-=(SubsetOf<PackOfVars> const &);
		SubsetOfPacks & operator&=(SubsetOf<PackOfVars> const &);
		SubsetOfPacks & operator|=(SubsetOf<PackOfVars> const & s) {return *this+=s;};
		
		SubsetOfPacks & operator+=(PackOfVars const &);
		SubsetOfPacks & operator-=(PackOfVars const &);
		SubsetOfPacks & operator&=(PackOfVars const &);
		SubsetOfPacks & operator|=(PackOfVars const & s) {return *this+=s;};
		
		void clear();
		
		friend SubsetOfPacks operator+(SubsetOfPacks const &, SubsetOfPacks const &);
		friend SubsetOfPacks operator-(SubsetOfPacks const &, SubsetOfPacks const &);
		friend SubsetOfPacks operator&(SubsetOfPacks const &, SubsetOfPacks const &);
		friend SubsetOfPacks operator|(SubsetOfPacks const & s1, SubsetOfPacks const & s2) {return s1 + s2;};
		
		friend SubsetOfPacks operator+(SubsetOfPacks const &, SubsetOf<PackOfVars> const &);
		friend SubsetOfPacks operator-(SubsetOfPacks const &, SubsetOf<PackOfVars> const &);
		friend SubsetOfPacks operator&(SubsetOfPacks const &, SubsetOf<PackOfVars> const &);
		friend SubsetOfPacks operator|(SubsetOfPacks const & s1, SubsetOf<PackOfVars> const & s2) {return s1 + s2;};
		
		friend SubsetOfPacks operator+(SubsetOf<PackOfVars> const & s1, SubsetOfPacks const & s2) {return s2 + s1;};
		friend SubsetOfPacks operator-(SubsetOf<PackOfVars> const &, SubsetOfPacks const &);
		friend SubsetOfPacks operator&(SubsetOf<PackOfVars> const & s1, SubsetOfPacks const & s2) {return s2 & s1;};
		friend SubsetOfPacks operator|(SubsetOf<PackOfVars> const & s1, SubsetOfPacks const & s2) {return s2 + s1;};
		
		friend SubsetOfPacks operator+(SubsetOfPacks const &, PackOfVars const &);
		friend SubsetOfPacks operator-(SubsetOfPacks const &, PackOfVars const &);
		friend SubsetOfPacks operator&(SubsetOfPacks const &, PackOfVars const &);
		friend SubsetOfPacks operator|(SubsetOfPacks const & s, PackOfVars const & p) {return s + p;};
		
		friend SubsetOfPacks operator+(PackOfVars const & p, SubsetOfPacks const & s) {return s + p;};
		friend SubsetOfPacks operator-(PackOfVars const &, SubsetOfPacks const &);
		friend SubsetOfPacks operator&(PackOfVars const & p, SubsetOfPacks const & s) {return s & p;};
		friend SubsetOfPacks operator|(PackOfVars const & p, SubsetOfPacks const & s) {return s + p;};
		
		SubsetOfVars const & getVars() const { return m_vars;};
				
	private:
	
		SubsetOfVars m_vars;
		
		SubsetOfPacks(SubsetOf<PackOfVars> &&, SubsetOfVars &&);
};


#endif

