/*
 * MitMAttack.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef DEF_MITM_ATTACK
#define DEF_MITM_ATTACK

#include <list>

#include "MitMAttack_diff.h"
#include "MitMAttack_eqs.h"

class MitMAttack
{
	public:
		MitMAttack(MitMAttack const &);
		MitMAttack(MitMAttack &&);
		
		MitMAttack const & operator=(MitMAttack const &);
		MitMAttack const & operator=(MitMAttack &&);
		
		~MitMAttack();
		
		MitMAttack(MitMAttack_Diff const &, MitMAttack_Eqs const &);
	
		int nbEqs() const {return m_eqs->nbEqs();};
		int data() const {return m_diff->data();};
		int dimDiff() const {return m_diff->dimDiff();};
		int timeOnline() const;
		int timeOffline() const;
		
		MitMAttack_Diff const & Diff() const {return *m_diff;};
		MitMAttack_Eqs const & Eqs() const {return *m_eqs;};
		
		SubsetOfPacks const & activeOn() const {return *m_active_on;};
		SubsetOfPacks const & activeOff() const {return *m_active_off;};
		SubsetOfPacks const & guessOn() const {return *m_guess_on;};
		SubsetOfPacks const & guessOff() const {return *m_guess_off;};
		
		int correctedMemory() const;
	
	private:
		
		static std::set<MitMAttack_Diff> st_all_diff;
		static std::set<MitMAttack_Eqs> st_all_eqs;
		
		static std::set<SubsetOfPacks> st_all_online;
		static std::set<SubsetOfPacks> st_all_offline;
		
		static std::map<SubsetOfPacks const *, int> st_all_time_online;
		static std::map<SubsetOfPacks const *, int> st_all_time_offline;
		
		MitMAttack_Diff const * m_diff;
		MitMAttack_Eqs const * m_eqs;
		
		SubsetOfPacks const * m_active_on;
		SubsetOfPacks const * m_active_off;
		SubsetOfPacks const * m_guess_on;
		SubsetOfPacks const * m_guess_off;
		
		BlockCipher const * m_B;
		
		mutable bool m_cor_mem_flag;
		mutable int m_cor_mem;
		
		friend std::list<MitMAttack> findMitMAttacks_fromP(int, int, int, BlockCipher const &, int, int);
		friend std::list<MitMAttack> findMitMAttacks_fromC(int, int, int, BlockCipher const &, int, int);
		
		void setBlockCipher(BlockCipher const &);
};

std::list<MitMAttack> findMitMAttacks_fromP(int, int, int, BlockCipher const & B, int, int);
std::list<MitMAttack> findMitMAttacks_fromC(int, int, int, BlockCipher const &, int, int);

#include "MitMAttack_core.h"

#endif
