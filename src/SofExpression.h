/*
 * SofExpression.h
 * 
 * Copyright 2014 Patrick Derbez <patrick.derbez@uni.lu>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#ifndef DEF_S_OF_EXPRESSION
#define DEF_S_OF_EXPRESSION

#include "Sbox.h"
#include <initializer_list>
#include <set>

class Expression;

class SofExpression
{	
	public:
		SofExpression(SofExpression const&);
		SofExpression(Sbox const&, std::initializer_list<Expression const> const Exprs);
		SofExpression(Sbox const&, std::vector<Expression> const& Exprs);
		
		template <typename Iter> SofExpression(Sbox const&, Iter it, Iter end);
		
		~SofExpression();
		
		SofExpression& operator=(SofExpression const&);
		
		unsigned int getNbExpressions() const { return m_sb.getNbVariables(); };
		
		typedef Expression * iterator;
		typedef const Expression * const_iterator;
		iterator begin() { return m_exprs; };
		iterator end() { return m_exprs + getNbExpressions(); };
		
		const_iterator begin() const { return m_exprs; };
		const_iterator end() const { return m_exprs + getNbExpressions(); };
		
		const_iterator cbegin() const { return m_exprs; };
		const_iterator cend() const { return m_exprs + getNbExpressions(); };
		
		bool isMonomial() const;
		
		bool replace(Variable old, Variable x);
		
		std::set<Variable> getVariables() const;
		void getVariables(std::set<Variable> &) const;
		
	private:
		Sbox m_sb;
		Expression * m_exprs;
		
		SofExpression(Sbox const&, Expression *);
		
		void print(std::ostream &flux) const;
		
		
	friend std::ostream& operator<<( std::ostream &flux, SofExpression const& expr);
	
	friend bool operator<(SofExpression const& expr1, SofExpression const& expr2);
	friend bool operator!=(SofExpression const& expr1, SofExpression const& expr2);
	friend bool operator==(SofExpression const& expr1, SofExpression const& expr2) {return !(expr1 != expr2); };
	
	friend class Expression;
};

template <typename ...ArgsT>
SofExpression Sbox::operator() (ArgsT ...Exprs) const
{
	const SofExpression expr(*this, {Exprs...});
	return expr;
}


template <typename Iter>
SofExpression::SofExpression(Sbox const& sb, Iter it, Iter end)
{
	const unsigned int nb_var = sb.getNbVariables();
	m_exprs = new Expression [nb_var];
	unsigned int pos = 0;
	for ( ; it != end && pos != nb_var; ++it) m_exprs[pos++] = *it;
	if (pos != nb_var || it != end)
	{
		std::cout << "Error: the Sbox " << sb << " takes exactly " << nb_var << " inputs" << std::endl;
		exit(EXIT_FAILURE);
	}
}

#endif

